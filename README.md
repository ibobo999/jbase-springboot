#jbase-springboot，JavaEE管理系统基础框架-JBase的SpringBoot版本
当前版本:0.0.1<br/>
QQ交流群号1：492587379<br/>
<br/>

#为什么使用SpringBoot
* 项目结构更加合理，各模块的独立性增强，每个模块都可以做为一个独立项目直接运行
* 更方便部署，每个项目都内嵌Tomcat容器
* 更好的管理静态资源，将公共的js，css，图片等等静态资源统一放到common项目中，其他项目不用重复COPY这些资源<br/>
<br/>

#界面展示
* 后台管理界面<br/>
![IMG_0874](./docs/images/zz001.png)
![IMG_0874](./docs/images/zz002.png)
![IMG_0874](./docs/images/zz003.png)
![IMG_0874](./docs/images/zz004.png)
* CMS演示界面<br/>
![IMG_0874](./docs/images/zz005.png)
![IMG_0874](./docs/images/zz006.png)
<br/><br/>

