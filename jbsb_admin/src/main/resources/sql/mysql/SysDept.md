-- 组织机构部门  SysDept sys_dept

selectPager
===
* 分页查询
select #page()# from sys_dept where 1=1  
@if(!isEmpty(name)){
 and name like #text('\'%'+name+'%\'')# 
@}
order by createTime desc

selectByParentId
===
* 某机构的直接子机构列表
select * from sys_dept where parentId=#parentId# order by sort