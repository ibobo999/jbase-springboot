-- 管理员 AdminUser admin_user

selectPager
===
* 分页查询
SELECT
@pageTag(){
t.*
@}
FROM admin_user AS t 
WHERE 1=1  
@if(!isEmpty(name)){
 AND t.name LIKE #text('\'%'+name+'%\'')# 
@}
ORDER BY createTime DESC

listBySids
===
* 根据sid的集合查询
SELECT * FROM admin_user WHERE sid IN (
@for(id in sids){
#id#  #text(idLP.last?"":"," )#
@}
) 