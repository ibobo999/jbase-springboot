-- 系统菜单表 SysMenu sys_menu

selectPager
===
* 分页查询
select #page()# from sys_menu where 1=1  
@if(!isEmpty(name)){
 and name like #text('\'%'+name+'%\'')# 
@}
order by createTime desc

selectByLevel
===
* 某一级别菜单列表
select * from sys_menu where level=#level# order by sort

selectByParentId
===
* 某菜单的直接子菜单列表
select * from sys_menu where parentId=#parentId# order by sort


updateSort
===
* 批量更新排序值
update sys_menu set sort=#sort# where id = #id#

