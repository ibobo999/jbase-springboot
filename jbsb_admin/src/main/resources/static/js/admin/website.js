
//是否存在指定函数 
function isExitsFunction(funcName) {
    try {
        if (typeof(eval(funcName)) == "function") {
            return true;
        }
    } catch(e) {}
    return false;
}
//是否存在指定变量 
function isExitsVariable(variableName) {
    try {
        if (typeof(variableName) == "undefined") {
            return false;
        } else {
            return true;
        }
    } catch(e) {}
    return false;
}

/*上传图片*/
function uploadImage(packId,fileImageId,fileInfoId,imageId){
	var uploader = WebUploader.create({
		auto: true,
		swf: STATIC_PATH+'/lib/webuploader/0.1.5/Uploader.swf',
		server: BASE_PATH+'/attachment/uploadImage',
		pick: '#'+packId,
		resize: false,
		accept: {
			title: 'Images',
			extensions: 'gif,jpg,jpeg,png',
			mimeTypes: 'image/*'
		}
	});
	uploader.on( 'beforeFileQueued', function( file ) {
		uploader.reset();
	});
	uploader.on( 'fileQueued', function( file ) {
		uploader.makeThumb( file, function( error, src ) {
			if ( error ) {
				$("#"+fileImageId).replaceWith('<span>不能预览</span>');
				return;
			}
			$("#"+fileImageId).attr( 'src', src );
			$("#"+fileInfoId).html(file.name);
		});
	});
	uploader.on( 'uploadSuccess', function( file, response ) {
		if ( "SUCCESS" != response.state ) {
			$("#"+fileInfoId).html("<font color='red'>文件上传失败！</font><br>"+response.error_info);
			return false;
		}
		$("#"+imageId).val(response.url );
		uploader.reset();
	}); 
}
/*表格里的checkbox，全选功能*/
function table_checkbox_all(){
	/*全选*/
	$("table thead th input:checkbox").on("click" , function(){
		$(this).closest("table").find("tr > td:first-child input:checkbox").prop("checked",$("table thead th input:checkbox").prop("checked"));
    });
}

/*textarea 字数限制*/
function textarealength(obj,maxlength){
	var v = $(obj).val();
	var l = v.length;
	if( l > maxlength){
		v = v.substring(0,maxlength);
		$(obj).val(v);
	}
	$(obj).parent().find(".textarea-length").text(v.length);
}

/*关闭弹出框口*/
function layer_close(){
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
}

/*关闭弹出框口*/
function iframe_close(){
	var topWindow = $(window.parent.document);
	var iframe = topWindow.find('#iframe_box .show_iframe');
	var tab = topWindow.find(".acrossTab li");
	var showTab = topWindow.find(".acrossTab li.active");
	var showBox=topWindow.find('.show_iframe:visible');
	var i = showTab.index();
	tab.eq(i-1).addClass("active");
	iframe.eq(i-1).show();
	tab.eq(i).remove();
	iframe.eq(i).remove();
}

toastr.options.closeButton = true;
toastr.options.closeDuration = 300;

$(function(){
	$.ajaxSetup({ cache: false }); 
}); 