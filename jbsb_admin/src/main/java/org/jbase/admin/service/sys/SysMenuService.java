/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.service.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import org.jbase.admin.dao.sys.SysMenuDao;
import org.jbase.admin.entity.sys.SysMenu;
import org.jbase.common.service.EntityMapperService;
import org.jbase.common.utils.Pager;
import org.jbase.common.utils.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Project: yxt_hebim <br/>
 * File: SysMenuService.java <br/>
 * Class: org.jbase.hebim.sevice.SysMenuService <br/>
 * Description: 系统菜单表. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: 2016-08-11 02:11:36 <br/>
 * gen by Jbase Coder <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class SysMenuService extends EntityMapperService<SysMenu> {

	@Autowired
	private SysMenuDao SysMenuDao;

	@Override
	public BaseMapper<SysMenu> dao() {
		return SysMenuDao;
	}

	public Pager query(Map<String, String> map, Pager pager) {

		PageQuery pageQuery = new PageQuery(pager.getPageNumber(), map, -1, pager.getPageSize());
		SysMenuDao.selectPager(pageQuery);

		pager.setList(pageQuery.getList());
		pager.setRecordCount((int) pageQuery.getTotalRow());
		return pager;
	}

	public List<SysMenu> listTop() {
		List<SysMenu> list = listByParentId(0);
		for (SysMenu menu : list) {
			List<SysMenu> childs = listByParentId(menu.getId());
			if (childs != null && childs.size() > 0) {
				menu.set("children", childs);
				menu.set("isParent", true);
			} else {
				menu.set("isParent", false);
			}
		}
		return list;
	}

	public List<SysMenu> listByParentId(int parentId) {
		return SysMenuDao.selectByParentId(parentId);
	}

	public List<Map<String, Object>> listSubMenus(int parentId) {
		List<SysMenu> list = listByParentId(parentId);
		List<Map<String, Object>> lm = new ArrayList<Map<String, Object>>();
		if (list != null && list.size() > 0) {
			for (SysMenu menu : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("name", menu.getName());
				map.put("_href", menu.getHref());
				map.put("icon", menu.getIcon());
				map.put("level", menu.getLevel());
				map.put("id", menu.getId());
				map.put("parentId", menu.getParentId());
				List<Map<String, Object>> childs = listSubMenus(menu.getId());
				if (childs != null && childs.size() > 0) {
					map.put("children", childs);
					map.put("isParent", true);
				} else {
					map.put("isParent", false);
				}
				lm.add(map);
			}
			return lm;
		} else
			return null;
	}

	public List<SysMenu> listChilds(int parentId) {
		List<SysMenu> list = listByParentId(parentId);
		for (SysMenu menu : list) {
			List<SysMenu> childs = listByParentId(menu.getId());
			if (childs != null && childs.size() > 0) {
				menu.set("children", childs);
				menu.set("isParent", true);
			} else {
				menu.set("isParent", false);
			}
		}
		return list;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param ids
	 * @param sorts
	 * @return
	 */
	public Message<?> updateSort(String[] ids, String[] sorts) {

		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		int length = ids.length;
		for (int i = 0; i < length; i++) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", ids[i]);
			map.put("sort", sorts[i]);
			list.add(map);
		}
		SysMenuDao.updateSort(list);
		return Message.fireSuccess();
	}
}
