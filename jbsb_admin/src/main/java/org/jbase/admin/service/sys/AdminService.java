/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.service.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.admin.entity.sys.Admin;
import org.jbase.common.service.EntityService;
import org.jbase.common.utils.Encodes;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Project: fw_admin <br/>
 * File: AdminService.java <br/>
 * Class: com.yxt.admin.service.system.AdminService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月2日 上午9:44:38 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class AdminService extends EntityService<Admin> {

	@Override
	protected Class<Admin> clzz() {
		return Admin.class;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param loginName
	 * @return
	 */
	public Admin fetchByName(String loginName) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("loginName", loginName);
		return fetchByCol(map);
	}

	@Override
	public int insertHolderId(Map<String, Object> map) {
		map.put("password", Encodes.md5(map.get("password").toString()));
		return super.insertHolderId(map);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param params
	 * @param roleIds
	 */
	@Transactional
	public void saveWithRoleIds(Map<String, Object> params, String[] roleIds) {
		int adminId = insertHolderId(params);
		insertAdminRoleLinker(roleIds, adminId);

	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param roleIds
	 * @param adminId
	 */
	private void insertAdminRoleLinker(String[] roleIds, int adminId) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (String roleId : roleIds) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("adminId", adminId);
			map.put("roleId", roleId);
			list.add(map);
		}
		sql.updateBatch("sys_admin.insertAdminRoleLinker", list);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param params
	 * @param roleIds
	 */
	public void updateWithRoleIds(Map<String, Object> params, String[] roleIds) {
		update(params);
		sql.executeUpdate("delete from sys_admin_role_linker where adminId=#id#", params);
		int adminId = Integer.parseInt(params.get("id").toString());
		insertAdminRoleLinker(roleIds, adminId);
	}

	@Override
	public void deleteByIds(String ids) {
		if (ids.endsWith(","))
			ids = ids.substring(0, ids.length() - 1);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("table_name", sql.getNc().getTableName(clzz()));
		map.put("ids", ids);
		sql.update("common.deleteByIds", map);
		sql.executeUpdate("delete from sys_admin_role_linker where adminId in (#text(ids)#)", map);
	}

}
