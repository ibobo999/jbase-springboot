/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.service.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import org.jbase.admin.dao.sys.SysDeptDao;
import org.jbase.admin.entity.sys.SysDept;
import org.jbase.common.service.EntityMapperService;
import org.jbase.common.utils.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Project: jbase-springboot <br/>
 * File: SysDeptService.java <br/>
 * Class: org.jbase.admin.service.sys.SysDeptService <br/>
 * Description: . <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: 2016-09-19 11:15:18 <br/>
 * gen by Jbase Coder <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class SysDeptService extends EntityMapperService<SysDept> {

	@Autowired
	private SysDeptDao sysDeptDao;

	@Override
	public BaseMapper<SysDept> dao() {
		return sysDeptDao;
	}

	public Pager query(Map<String, String> map, Pager pager) {

		PageQuery pageQuery = new PageQuery(pager.getPageNumber(), map, -1, pager.getPageSize());
		sysDeptDao.selectPager(pageQuery);

		pager.setList(pageQuery.getList());
		pager.setRecordCount((int) pageQuery.getTotalRow());
		return pager;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param pid
	 * @return
	 */
	public List<SysDept> selectByParentId(int pid) {
		List<SysDept> list = sysDeptDao.selectByParentId(pid);
		return list;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param pid
	 * @return
	 */
	public List<Map<String, Object>> selectTreeByParentId(int pid) {

		List<SysDept> list = selectByParentId(pid);
		if (list != null && list.size() > 0) {
			List<Map<String, Object>> lm = new ArrayList<Map<String, Object>>();
			for (SysDept dept : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("name", dept.getName());
				map.put("level", dept.getLevel());
				map.put("id", dept.getId());
				map.put("parentId", dept.getParentId());
				map.put("address", dept.getAddress());
				map.put("zipCode", dept.getZipCode());
				map.put("master", dept.getMaster());
				map.put("phone", dept.getPhone());
				List<Map<String, Object>> childs = selectTreeByParentId(dept.getId());
				if (childs != null && childs.size() > 0) {
					map.put("children", childs);
					map.put("isParent", true);
				} else {
					map.put("isParent", false);
				}
				lm.add(map);
			}
			return lm;
		} else
			return null;
	}

}
