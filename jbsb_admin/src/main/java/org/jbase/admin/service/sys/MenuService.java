/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.service.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.admin.entity.sys.Menu;
import org.jbase.common.service.EntityService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Project: fw_admin <br/>
 * File: AdminService.java <br/>
 * Class: com.yxt.admin.service.system.AdminService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月2日 上午9:44:38 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class MenuService extends EntityService<Menu> {

	@Override
	protected Class<Menu> clzz() {
		return Menu.class;
	}

	@Cacheable("accountCache")
	public List<Menu> listTop() {
		List<Menu> list = listByParentId(0);
		for (Menu menu : list) {
			List<Menu> childs = listByParentId(menu.getId());
			if (childs != null && childs.size() > 0) {
				menu.set("children", childs);
				menu.set("isParent", true);
			} else {
				menu.set("isParent", false);
			}
		}
		return list;
	}

	@Cacheable("accountCache")
	public List<Map<String, Object>> listSubMenus(int parentId) {
		List<Menu> list = listByParentId(parentId);
		List<Map<String, Object>> lm = new ArrayList<Map<String, Object>>();
		if (list != null && list.size() > 0) {
			for (Menu menu : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("name", menu.getName());
				map.put("_href", menu.getHref());
				List<Map<String, Object>> childs = listSubMenus(menu.getId());
				if (childs != null && childs.size() > 0) {
					map.put("children", childs);
					map.put("isParent", true);
				} else {
					map.put("isParent", false);
				}
				lm.add(map);
			}
			return lm;
		} else
			return null;
	}

	public List<Menu> listTopMenu() {
		List<Menu> listByLevel = listByLevel(0);
		for (Menu menu : listByLevel) {
			if (menu.getIsShowTopSub() == 1) {
				menu.set("children", listByParentId(menu.getId()));
			}
		}
		return listByLevel;
	}

	public List<Menu> listSideMenu(int parentId) {
		List<Menu> list = listByParentId(parentId);
		for (Menu menu : list) {
			List<Menu> childs = listByParentId(menu.getId());
			if (childs != null && childs.size() > 0) {
				menu.set("children", childs);
				menu.set("isParent", true);
			} else {
				menu.set("isParent", false);
			}
		}
		return list;
	}

	public List<Menu> listChilds(int parentId) {
		List<Menu> list = listByParentId(parentId);
		for (Menu menu : list) {
			List<Menu> childs = listByParentId(menu.getId());
			if (childs != null && childs.size() > 0) {
				menu.set("children", childs);
				menu.set("isParent", true);
			} else {
				menu.set("isParent", false);
			}
		}
		return list;
	}

	public List<Menu> listByLevel(int level) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("level", level);
		List<Menu> select = sql.select("sys_menu.listByLevel", Menu.class, map);
		return select;
	}

	public List<Menu> listByParentId(int parentId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", parentId);
		List<Menu> select = sql.select("sys_menu.listByParentId", Menu.class, map);
		return select;
	}
}