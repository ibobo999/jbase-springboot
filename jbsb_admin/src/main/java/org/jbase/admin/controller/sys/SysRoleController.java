/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.controller.sys;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jbase.common.utils.message.Message;
import org.jbase.common.utils.message.MessageCode;
import org.jbase.common.utils.Pager;
import org.jbase.common.utils.Requester;
import org.jbase.admin.entity.sys.SysRole;
import org.jbase.admin.service.sys.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

/**
 * Project: jbase-springboot <br/>
 * File: SysRoleController.java <br/>
 * Class: org.jbase.admin.controller.sys.SysRoleController <br/>
 * Description: . <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: 2016-09-16 02:21:39 <br/>
 * gen by Jbase Coder <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("org.jbase.admin.controller.sys.SysRoleController")
@RequestMapping("/admin/sys/SysRole")
public class SysRoleController {

	@Autowired
	private SysRoleService sysRoleService;

	@RequestMapping("/index")
	public void index() {
	}

	@RequestMapping("/add")
	public void add() {
	}

	@RequestMapping("/edit")
	public void edit(HttpServletRequest request) {
		Message<?> mess = getUnique(request);
		request.setAttribute("obj", mess.getData());
	}

	@RequestMapping("/show")
	public void show(HttpServletRequest request) {
		Message<?> mess = getUnique(request);
		request.setAttribute("obj", mess.getData());
	}

	@ResponseBody
	@RequestMapping(value = "/list", produces = "text/plain;charset=UTF-8")
	public String list(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = warp.getPager();
		sysRoleService.query(warp.getSearchKeys(), pager);
		return JSON.toJSONString(pager);
	}

	@ResponseBody
	@RequestMapping(value = "/save", produces = "text/plain;charset=UTF-8")
	public String save(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Message<SysRole> message = sysRoleService.saveOrUpdate(warp.getParams());
		return JSON.toJSONString(message);
	}

	@ResponseBody
	@RequestMapping(value = "/fetch", produces = "text/plain;charset=UTF-8")
	public String fetch(HttpServletRequest request) {
		Message<?> mess = getUnique(request);
		return JSON.toJSONString(mess);
	}

	@ResponseBody
	@RequestMapping(value = "/delete", produces = "text/plain;charset=UTF-8")
	public String delete(HttpServletRequest request) {
		String ids = request.getParameter("ids");
		if (StringUtils.isBlank(ids)) {
			Message<?> mess = Message.fireFail().SetMessageCode(MessageCode.ENTITY_ID_IS_EMPTY);
			return JSON.toJSONString(mess);
		}
		String[] idArray = ids.split(",");
		if (idArray.length > 1)
			return JSON.toJSONString(sysRoleService.deleteByIds(ids));
		else
			return JSON.toJSONString(sysRoleService.deleteById(ids));
	}

	/**
	 * 描述 : 如果id为空直接返回，否则返回实体对象. <br/>
	 * <p>
	 * 
	 * @param request
	 */
	private Message<?> getUnique(HttpServletRequest request) {
		String id = request.getParameter("id");
		if (StringUtils.isBlank(id)) {
			return Message.fireFail().SetMessageCode(MessageCode.ENTITY_ID_IS_EMPTY);
		}
		Message<SysRole> unique =sysRoleService.unique(id);
		return unique;

	}
}
