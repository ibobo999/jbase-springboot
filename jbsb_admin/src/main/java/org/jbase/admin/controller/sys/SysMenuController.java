/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.controller.sys;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jbase.admin.entity.sys.SysMenu;
import org.jbase.admin.service.sys.SysMenuService;
import org.jbase.common.utils.Pager;
import org.jbase.common.utils.Requester;
import org.jbase.common.utils.message.Message;
import org.jbase.common.utils.message.MessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSON;

/**
 * Project: yxt_hebim <br/>
 * File: SysMenuController.java <br/>
 * Class: org.jbase.yxt.hebim.web.SysMenuController <br/>
 * Description: 系统菜单表. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: 2016-08-11 02:11:35 <br/>
 * gen by Jbase Coder <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("org.jbase.admin.controller.sys.SysMenuController")
@RequestMapping("/admin/sys/SysMenu")
public class SysMenuController {

	@Autowired
	private SysMenuService sysMenuService;

	@RequestMapping("/index")
	public void index(HttpServletRequest request) {
		List<SysMenu> menuList = new ArrayList<SysMenu>();
		menuList = sysMenuService.listTop();
		request.setAttribute("menuList", menuList);
	}

	@RequestMapping("/add")
	public void add(HttpServletRequest request) {
		SysMenu pMenu = null;
		String parentId = request.getParameter("parentId");
		if (StringUtils.isNoneBlank(parentId)) {
			pMenu = sysMenuService.unique(parentId).getData();
		} else {
			pMenu = new SysMenu();
			pMenu.setLevel(0);
			pMenu.setId(0);
			pMenu.setName("顶级菜单");
		}
		request.setAttribute("parentMenu", pMenu);
	}

	@RequestMapping("/edit")
	public void edit(HttpServletRequest request) {
		Message<?> mess = getUnique(request);
		request.setAttribute("obj", mess.getData());
	}

	@RequestMapping("/show")
	public void show(HttpServletRequest request) {
		Message<?> mess = getUnique(request);
		request.setAttribute("obj", mess.getData());
	}

	@ResponseBody
	@RequestMapping(value = "/list", produces = "text/plain;charset=UTF-8")
	public String list(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = warp.getPager();
		sysMenuService.query(warp.getSearchKeys(), pager);
		return JSON.toJSONString(pager);
	}

	@ResponseBody
	@RequestMapping(value = "/save", produces = "text/plain;charset=UTF-8")
	public String save(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Message<SysMenu> message = sysMenuService.saveOrUpdate(warp.getParams());
		return JSON.toJSONString(message);
	}

	@ResponseBody
	@RequestMapping(value = "/fetch", produces = "text/plain;charset=UTF-8")
	public String fetch(HttpServletRequest request) {
		Message<?> mess = getUnique(request);
		return JSON.toJSONString(mess);
	}

	@ResponseBody
	@RequestMapping(value = "/delete", produces = "text/plain;charset=UTF-8")
	public String delete(HttpServletRequest request) {
		String ids = request.getParameter("ids");
		if (StringUtils.isBlank(ids)) {
			Message<?> mess = Message.fireFail().SetMessageCode(MessageCode.ENTITY_ID_IS_EMPTY);
			return JSON.toJSONString(mess);
		}
		String[] idArray = ids.split(",");
		if (idArray.length > 1)
			return JSON.toJSONString(sysMenuService.deleteByIds(ids));
		else
			return JSON.toJSONString(sysMenuService.deleteById(ids));
	}

	@ResponseBody
	@RequestMapping(value = "/childTree", produces = "text/plain;charset=UTF-8")
	public String childTree(HttpServletRequest request) {
		String pid = request.getParameter("pId");
		List<SysMenu> menuList = sysMenuService.listChilds(Integer.parseInt(pid));
		System.out.println("OK");
		return JSON.toJSONString(menuList);
	}

	@ResponseBody
	@RequestMapping(value = "/selectTree", produces = "text/plain;charset=UTF-8")
	public String selectTree(HttpServletRequest request) {
		int pid = 0;
		String parentId = request.getParameter("parentId");
		if (StringUtils.isNoneBlank(parentId)) {
			pid = Integer.parseInt(parentId);
		}
		List<Map<String, Object>> list = sysMenuService.listSubMenus(pid);
		return JSON.toJSONString(list);
	}

	/**
	 * 描述 : 如果id为空直接返回，否则返回实体对象. <br/>
	 * <p>
	 * 
	 * @param request
	 */
	private Message<?> getUnique(HttpServletRequest request) {
		String id = request.getParameter("id");
		if (StringUtils.isBlank(id)) {
			return Message.fireFail().SetMessageCode(MessageCode.ENTITY_ID_IS_EMPTY);
		}
		Message<SysMenu> unique = sysMenuService.unique(id);
		return unique;

	}

	@ResponseBody
	@RequestMapping(value = "/updateSort", produces = "text/plain;charset=UTF-8")
	public String updateSort(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String[] ids = request.getParameterValues("ids");
		String[] sorts = request.getParameterValues("sorts");
		if (ids.length != sorts.length) {
			Message<?> fireFail = Message.fireFail();
			fireFail.setMsg("保存失败，保存的数据条数不一致！");
			return JSON.toJSONString(fireFail);
		}
		Message<?> message = sysMenuService.updateSort(ids, sorts);
		return JSON.toJSONString(message);
	}
}
