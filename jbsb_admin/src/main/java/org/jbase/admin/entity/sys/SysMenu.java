/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.entity.sys;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Project: jbase-springboot <br/>
 * File: SysMenu.java <br/>
 * Class: org.jbase.admin.entity.sys.SysMenu <br/>
 * Description: 系统菜单表. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: 2016-09-16 02:21:39 <br/>
 * gen by Jbase Coder <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "sys_menu")
public class SysMenu extends BaseEntity {

	private static final long serialVersionUID = 1L;

	// 父级编号
	private Integer parentId = 0;
	// parentName
	private String parentName;
	// 所有父级编号
	private String parentIds;
	// 名称
	private String name;
	// 排序
	private Integer sort = 0;
	// level
	private Integer level = 0;
	// 链接
	private String href;
	// 目标
	private String target;
	// 图标
	private String icon;
	// 是否在菜单中显示
	private Integer isShow = 1;
	// isShowTopSub
	private Integer isShowTopSub = 0;
	// 权限标识
	private String permission;
	// 0
	private Integer fortypeid = 0;

	/**
	 * @return 父级编号
	 */
	public Integer getParentId() {
		return parentId;
	}

	/**
	 * @param 父级编号
	 *            to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return parentName
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * @param parentName
	 *            to set
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	/**
	 * @return 所有父级编号
	 */
	public String getParentIds() {
		return parentIds;
	}

	/**
	 * @param 所有父级编号
	 *            to set
	 */
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	/**
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param 名称
	 *            to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return 排序
	 */
	public Integer getSort() {
		return sort;
	}

	/**
	 * @param 排序
	 *            to set
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}

	/**
	 * @return level
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 * @return 链接
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param 链接
	 *            to set
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * @return 目标
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @param 目标
	 *            to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * @return 图标
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param 图标
	 *            to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @return 是否在菜单中显示
	 */
	public Integer getIsShow() {
		return isShow;
	}

	/**
	 * @param 是否在菜单中显示
	 *            to set
	 */
	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	/**
	 * @return isShowTopSub
	 */
	public Integer getIsShowTopSub() {
		return isShowTopSub;
	}

	/**
	 * @param isShowTopSub
	 *            to set
	 */
	public void setIsShowTopSub(Integer isShowTopSub) {
		this.isShowTopSub = isShowTopSub;
	}

	/**
	 * @return 权限标识
	 */
	public String getPermission() {
		return permission;
	}

	/**
	 * @param 权限标识
	 *            to set
	 */
	public void setPermission(String permission) {
		this.permission = permission;
	}

	/**
	 * @return 0
	 */
	public Integer getFortypeid() {
		return fortypeid;
	}

	/**
	 * @param 0
	 *            to set
	 */
	public void setFortypeid(Integer fortypeid) {
		this.fortypeid = fortypeid;
	}

}
