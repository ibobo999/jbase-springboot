/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.entity.sys;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Project: fw_admin <br/>
 * File: Actions.java <br/>
 * Class: com.yxt.admin.entity.sys.Actions <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年1月18日 上午11:20:32 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "sys_actions")
public class Actions extends BaseEntity {

	private static final long serialVersionUID = -5882017389696078340L;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the enname
	 */
	public String getEnname() {
		return enname;
	}

	/**
	 * @param enname
	 *            the enname to set
	 */
	public void setEnname(String enname) {
		this.enname = enname;
	}

	/**
	 * @return the controllerClassName
	 */
	public String getControllerClassName() {
		return controllerClassName;
	}

	/**
	 * @param controllerClassName
	 *            the controllerClassName to set
	 */
	public void setControllerClassName(String controllerClassName) {
		this.controllerClassName = controllerClassName;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the requestMethod
	 */
	public String getRequestMethod() {
		return requestMethod;
	}

	/**
	 * @param requestMethod
	 *            the requestMethod to set
	 */
	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	/**
	 * @return the requestParameterTypes
	 */
	public String getRequestParameterTypes() {
		return requestParameterTypes;
	}

	/**
	 * @param requestParameterTypes
	 *            the requestParameterTypes to set
	 */
	public void setRequestParameterTypes(String requestParameterTypes) {
		this.requestParameterTypes = requestParameterTypes;
	}

	private String name;
	private String enname;
	private String controllerClassName;
	private String url;
	private String requestMethod;
	private String requestParameterTypes;

}
