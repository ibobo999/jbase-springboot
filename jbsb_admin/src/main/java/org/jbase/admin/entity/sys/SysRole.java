/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.entity.sys;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Project: jbase-springboot <br/>
 * File: SysRole.java <br/>
 * Class: org.jbase.admin.entity.sys.SysRole <br/>
 * Description: . <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: 2016-09-16 02:21:39 <br/>
 * gen by Jbase Coder <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "sys_role")
public class SysRole extends BaseEntity {

	private static final long serialVersionUID = 1L;

	// 归属机构
	private int deptId = 0;

	// 归属机构名称
	private String deptName;

	/**
	 * @return the deptName
	 */
	public String getDeptName() {
		return deptName;
	}

	/**
	 * @param deptName
	 *            the deptName to set
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	// 角色名称
	private String name;
	// 英文名称
	private String enname;
	// 角色类型
	private String roleType;
	// 数据范围
	private int dataScope = 0;
	// 是否系统数据
	private int isSys = 0;
	// 角色描述
	private String summary;

	/**
	 * @return 归属机构
	 */
	public int getDeptId() {
		return deptId;
	}

	/**
	 * @param 归属机构
	 *            to set
	 */
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	/**
	 * @return 角色名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param 角色名称
	 *            to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return 英文名称
	 */
	public String getEnname() {
		return enname;
	}

	/**
	 * @param 英文名称
	 *            to set
	 */
	public void setEnname(String enname) {
		this.enname = enname;
	}

	/**
	 * @return 角色类型
	 */
	public String getRoleType() {
		return roleType;
	}

	/**
	 * @param 角色类型
	 *            to set
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	/**
	 * @return 数据范围
	 */
	public int getDataScope() {
		return dataScope;
	}

	/**
	 * @param 数据范围
	 *            to set
	 */
	public void setDataScope(int dataScope) {
		this.dataScope = dataScope;
	}

	/**
	 * @return 是否系统数据
	 */
	public int getIsSys() {
		return isSys;
	}

	/**
	 * @param 是否系统数据
	 *            to set
	 */
	public void setIsSys(int isSys) {
		this.isSys = isSys;
	}

	/**
	 * @return 角色描述
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @param 角色描述
	 *            to set
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

}
