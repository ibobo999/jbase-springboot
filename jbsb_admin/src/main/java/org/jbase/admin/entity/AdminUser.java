/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.entity;

import java.sql.Timestamp;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Project: jbase-springboot <br/>
 * File: AdminUser.java <br/>
 * Class: org.jbase.admin.entity.AdminUser <br/>
 * Description: 管理员. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: 2016-12-25 01:03:08 <br/>
 * gen by Jbase Coder <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "admin_user")
public class AdminUser extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String name; // 登录名
	private String password; // 密码
	private String realName; // 真实姓名
	private Integer sex; // sex
	private String email; // 邮箱
	private String phone; // 电话
	private String mobile; // 手机
	private String userType; // 用户类型
	private String photo; // 用户头像
	private String loginIp; // 最后登陆IP
	private Timestamp loginDate; // 最后登陆时间
	private String loginFlag; // 是否可登录

	/**
	 * @return 登录名
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param 登录名
	 *            to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return 密码
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param 密码
	 *            to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return 真实姓名
	 */
	public String getRealName() {
		return realName;
	}

	/**
	 * @param 真实姓名
	 *            to set
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * @return sex
	 */
	public Integer getSex() {
		return sex;
	}

	/**
	 * @param sex
	 *            to set
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * @return 邮箱
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param 邮箱
	 *            to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return 电话
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param 电话
	 *            to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return 手机
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param 手机
	 *            to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return 用户类型
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param 用户类型
	 *            to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @return 用户头像
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * @param 用户头像
	 *            to set
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**
	 * @return 最后登陆IP
	 */
	public String getLoginIp() {
		return loginIp;
	}

	/**
	 * @param 最后登陆IP
	 *            to set
	 */
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	/**
	 * @return 最后登陆时间
	 */
	public Timestamp getLoginDate() {
		return loginDate;
	}

	/**
	 * @param 最后登陆时间
	 *            to set
	 */
	public void setLoginDate(Timestamp loginDate) {
		this.loginDate = loginDate;
	}

	/**
	 * @return 是否可登录
	 */
	public String getLoginFlag() {
		return loginFlag;
	}

	/**
	 * @param 是否可登录
	 *            to set
	 */
	public void setLoginFlag(String loginFlag) {
		this.loginFlag = loginFlag;
	}

}
