/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.entity.sys;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Project: jbase-springboot <br/>
 * File: SysDept.java <br/>
 * Class: org.jbase.admin.entity.sys.SysDept <br/>
 * Description: . <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: 2016-09-19 11:15:18 <br/>
 * gen by Jbase Coder <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "sys_dept")
public class SysDept extends BaseEntity {

	private static final long serialVersionUID = 1L;

	// 父级编号
	private Integer parentId = 0;
	// 所有父级编号
	private String parentIds;
	// 上级部门名称
	private String parentName;

	// 名称
	private String name;
	// 排序
	private Integer sort = 0;
	// level
	private Integer level = 0;
	// 归属区域
	private String areaId;
	// 区域编码
	private String code;
	// 机构类型
	private String type = "0";
	// 机构等级
	private String grade = "0";
	// 联系地址
	private String address;
	// 邮政编码
	private String zipCode;
	// 负责人
	private String master;
	// 电话
	private String phone;
	// 传真
	private String fax;
	// 邮箱
	private String email;

	/**
	 * @return 父级编号
	 */
	public Integer getParentId() {
		return parentId;
	}

	/**
	 * @param 父级编号
	 *            to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return 所有父级编号
	 */
	public String getParentIds() {
		return parentIds;
	}

	/**
	 * @param 所有父级编号
	 *            to set
	 */
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	/**
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param 名称
	 *            to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return 排序
	 */
	public Integer getSort() {
		return sort;
	}

	/**
	 * @param 排序
	 *            to set
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}

	/**
	 * @return level
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 * @return 归属区域
	 */
	public String getAreaId() {
		return areaId;
	}

	/**
	 * @param 归属区域
	 *            to set
	 */
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return 区域编码
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param 区域编码
	 *            to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return 机构类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param 机构类型
	 *            to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return 机构等级
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param 机构等级
	 *            to set
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * @return 联系地址
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param 联系地址
	 *            to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return 邮政编码
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param 邮政编码
	 *            to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return 负责人
	 */
	public String getMaster() {
		return master;
	}

	/**
	 * @param 负责人
	 *            to set
	 */
	public void setMaster(String master) {
		this.master = master;
	}

	/**
	 * @return 电话
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param 电话
	 *            to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return 传真
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param 传真
	 *            to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return 邮箱
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param 邮箱
	 *            to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the parentName
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * @param parentName
	 *            the parentName to set
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
}
