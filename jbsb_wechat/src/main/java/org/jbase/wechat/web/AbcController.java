/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.wechat.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jbase.wechat.domain.Hello;
import org.jbase.wechat.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Project: jbsb_wechat <br/>
 * File: AbcController.java <br/>
 * Class: org.jbase.wechat.web.AbcController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年10月13日 上午10:32:28 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("org.jbase.wechat.web.AbcController")
@RequestMapping("/abc")
public class AbcController {

	@Autowired
	private HelloService helloService;

	@RequestMapping("/aaa")
	public void aaa(HttpServletRequest request) {
		String name = request.getParameter("name");

		String hello = helloService.hello(name);

		request.setAttribute("name", hello);

	}

	@RequestMapping("/bbb")
	public void bbb(HttpServletRequest request) {

		String name = request.getParameter("name");
		String age = request.getParameter("age");

		// List<Hello> allHello = helloService.allHello(name);
		List<Hello> allHello = helloService.findByAge(Integer.parseInt(age));

		request.setAttribute("list", allHello);
	}

	@RequestMapping("/ccc")
	public String ccc() {
		return "redirect:bbb";

	}

	@RequestMapping("/ddd")
	public void ddd() {
	}

	@RequestMapping("/eee")
	@ResponseBody
	public String eee(HttpServletRequest request) {
		String age = request.getParameter("age");
		String name = request.getParameter("name");
		helloService.doInsert(Integer.parseInt(age), name);
		return "ok";
	}

}
