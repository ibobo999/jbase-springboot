/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.wechat.service;

import java.util.List;

import org.beetl.sql.core.db.KeyHolder;
import org.jbase.wechat.dao.HelloDao;
import org.jbase.wechat.domain.Hello;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Project: jbsb_wechat <br/>
 * File: HelloService.java <br/>
 * Class: org.jbase.wechat.service.HelloService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年10月13日 上午10:59:04 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class HelloService {

	@Autowired
	private HelloDao helloDao;

	public String hello(String yourname) {
		return "hello:" + yourname;
	}

	public void doInsert(int age, String name) {
		Hello hello = new Hello();
		hello.setAge(age);
		hello.setName(name);
		// helloDao.insert(hello);
		KeyHolder insertReturnKey = helloDao.insertReturnKey(hello);
		System.out.println("DDDDDDDDDD :    " + insertReturnKey.getInt());
	}
	
	public List<Hello> allHello(String name){
		return helloDao.queryByName(name);
	}
	public List<Hello> findByAge(int age){
		return helloDao.queryByAge(age);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @return
	 */
	public List<Hello> all() {
		// TODO Auto-generated method stub
		return helloDao.all();
	}

}










