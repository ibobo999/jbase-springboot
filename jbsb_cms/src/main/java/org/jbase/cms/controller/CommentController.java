package org.jbase.cms.controller;

import javax.servlet.http.HttpServletRequest;

import org.jbase.cms.entity.Comment;
import org.jbase.cms.service.CommentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("cms_comment_controller")
@RequestMapping("/comment")
public class CommentController {

	@Autowired
	private CommentService commentService;


	@RequestMapping("/commentDo")
	public Object docolumn(HttpServletRequest request) {
		Comment com = new Comment();
		com.setContent(request.getParameter("content"));
		com.setUserName(request.getParameter("name"));
		String articleId = request.getParameter("articleId");
		String columnId = request.getParameter("columnId");
		String childId = request.getParameter("childId");
		com.setArticleId(Integer.parseInt(articleId));
		commentService.insert(com);
		return "redirect:/article/"+articleId;
	}

}
