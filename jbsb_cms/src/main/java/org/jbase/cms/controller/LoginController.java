/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jbase.cms.entity.LoginTabla;
import org.jbase.cms.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Project: yxt_hbdd <br/>
 * File: LoginController.java <br/>
 * Class: org.jbase.website.controller.LoginController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: www.yxtsoft.com <br/>
 * Makedate: 2016年6月13日 下午6:41:01 <br/>
 * 
 * @author liyi
 * @version 1.0
 * @since 1.0
 */
@Controller
@RequestMapping("/longinNumber")
public class LoginController {
	@Autowired
	private LoginService loginService;
	@RequestMapping("/queryNumber")
	@ResponseBody
	public String page(HttpServletRequest request) {
		HttpSession session = request.getSession();
		int list=loginService.queryLoginNumber(session.getId());
//		if(list.size()>0){
//			return list.get(0).getSessionString()+","+list.get(0).getLoginNumbers();
//			
//		}
	   return list+"";
	}
	@RequestMapping("/saveNumber")
	public void saveNumber(HttpServletRequest request) {
		loginService.svaeLoginNumber(request.getParameter("loginsession"),request.getParameter("loginnumber"));
		
	}
}
