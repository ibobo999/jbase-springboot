/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.entity;

import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Project: fw_common <br/>
 * File: Column.java <br/>
 * Class: com.yxt.cms.entity.Column <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:05:59 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "cms_article")
public class Article extends BaseEntity {

	private static final long serialVersionUID = -3883590126771214153L;

	/**
	 * @return the columnId
	 */
	public int getColumnId() {
		return columnId;
	}

	/**
	 * @param columnId
	 *            the columnId to set
	 */
	public void setColumnId(int columnId) {
		this.columnId = columnId;
	}

	/**
	 * @return the columnSid
	 */
	public String getColumnSid() {
		return columnSid;
	}

	/**
	 * @param columnSid
	 *            the columnSid to set
	 */
	public void setColumnSid(String columnSid) {
		this.columnSid = columnSid;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords
	 *            the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 * @return the weight_date
	 */
	public Date getWeight_date() {
		return weight_date;
	}

	/**
	 * @param weight_date
	 *            the weight_date to set
	 */
	public void setWeight_date(Date weight_date) {
		this.weight_date = weight_date;
	}

	/**
	 * @return the hits
	 */
	public int getHits() {
		return hits;
	}

	/**
	 * @param hits
	 *            the hits to set
	 */
	public void setHits(int hits) {
		this.hits = hits;
	}

	/**
	 * @return the posid
	 */
	public String getPosid() {
		return posid;
	}

	/**
	 * @param posid
	 *            the posid to set
	 */
	public void setPosid(String posid) {
		this.posid = posid;
	}

	/**
	 * @return the customListView
	 */
	public String getCustomListView() {
		return customListView;
	}

	/**
	 * @param customListView
	 *            the customListView to set
	 */
	public void setCustomListView(String customListView) {
		this.customListView = customListView;
	}

	/**
	 * @return the customContentView
	 */
	public String getCustomContentView() {
		return customContentView;
	}

	/**
	 * @param customContentView
	 *            the customContentView to set
	 */
	public void setCustomContentView(String customContentView) {
		this.customContentView = customContentView;
	}

	/**
	 * @return the viewConfig
	 */
	public String getViewConfig() {
		return viewConfig;
	}

	/**
	 * @param viewConfig
	 *            the viewConfig to set
	 */
	public void setViewConfig(String viewConfig) {
		this.viewConfig = viewConfig;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the copyfrom
	 */
	public String getCopyfrom() {
		return copyfrom;
	}

	/**
	 * @param copyfrom
	 *            the copyfrom to set
	 */
	public void setCopyfrom(String copyfrom) {
		this.copyfrom = copyfrom;
	}

	/**
	 * @return the relation
	 */
	public String getRelation() {
		return relation;
	}

	/**
	 * @param relation
	 *            the relation to set
	 */
	public void setRelation(String relation) {
		this.relation = relation;
	}

	/**
	 * @return the allowComment
	 */
	public int getAllowComment() {
		return allowComment;
	}

	/**
	 * @param allowComment
	 *            the allowComment to set
	 */
	public void setAllowComment(int allowComment) {
		this.allowComment = allowComment;
	}

	/**
	 * @return the isAudit
	 */
	public int getIsAudit() {
		return isAudit;
	}

	/**
	 * @param isAudit
	 *            the isAudit to set
	 */
	public void setIsAudit(int isAudit) {
		this.isAudit = isAudit;
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @param target
	 *            the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * @return the sort
	 */
	public int getSort() {
		return sort;
	}

	/**
	 * @param sort
	 *            the sort to set
	 */
	public void setSort(int sort) {
		this.sort = sort;
	}

	/**
	 * @return the videoCode
	 */
	public String getVideoCode() {
		return videoCode;
	}

	/**
	 * @param videoCode
	 *            the videoCode to set
	 */
	public void setVideoCode(String videoCode) {
		this.videoCode = videoCode;
	}

	private int columnId;
	private String columnSid;
	private String title;
	private String link;
	private String color;
	private String image;
	private String keywords;
	private String description;
	// 权重，越大越靠前
	private String ImageUrl;

	public String getImageUrl() {
		return ImageUrl;
	}

	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}

	private int weight = 0;
	private Date weight_date;
	private int hits = 0;
	private String posid;
	private String customListView = "/tpl/column";
	private String customContentView = "/tpl/article";
	private String viewConfig;
	private String content;
	private String copyfrom;
	private String relation;
	private String authorName;
	private int allowComment = 1;
	private int isAudit = 0;
	private String target;
	private int sort = 0;
	private String videoCode;
	private int useColTpl = 0;

	/**
	 * @return the useColTpl
	 */
	public int getUseColTpl() {
		return useColTpl;
	}

	/**
	 * @param useColTpl
	 *            the useColTpl to set
	 */
	public void setUseColTpl(int useColTpl) {
		this.useColTpl = useColTpl;
	}

	/**
	 * @return the authorName
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * @param authorName
	 *            the authorName to set
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

}
