package org.jbase.cms.entity;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

@Table(name = "place_gymnasium")
public class PlaceGymnasium extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5595009908399868747L;
	private String name;
	private String areaIds;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAreaIds() {
		return areaIds;
	}

	public void setAreaIds(String areaIds) {
		this.areaIds = areaIds;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public int getLinkerId() {
		return linkerId;
	}

	public void setLinkerId(int linkerId) {
		this.linkerId = linkerId;
	}

	public String getLinkerName() {
		return linkerName;
	}

	public void setLinkerName(String linkerName) {
		this.linkerName = linkerName;
	}

	public String getLinkerPhone() {
		return linkerPhone;
	}

	public void setLinkerPhone(String linkerPhone) {
		this.linkerPhone = linkerPhone;
	}

	public int getCreateId() {
		return createId;
	}

	public void setCreateId(int createId) {
		this.createId = createId;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getTraffic() {
		return traffic;
	}

	public void setTraffic(String traffic) {
		this.traffic = traffic;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShopTime() {
		return shopTime;
	}

	public void setShopTime(String shopTime) {
		this.shopTime = shopTime;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getCharacteristic() {
		return characteristic;
	}

	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
	}

	public String getBooknote() {
		return booknote;
	}

	public void setBooknote(String booknote) {
		this.booknote = booknote;
	}

	public int getFieldcount() {
		return fieldcount;
	}

	public void setFieldcount(int fieldcount) {
		this.fieldcount = fieldcount;
	}

	public int getHasAudit() {
		return hasAudit;
	}

	public void setHasAudit(int hasAudit) {
		this.hasAudit = hasAudit;
	}

	public int getRecommend() {
		return recommend;
	}

	public void setRecommend(int recommend) {
		this.recommend = recommend;
	}

	public String getRegionID() {
		return regionID;
	}

	public void setRegionID(String regionID) {
		this.regionID = regionID;
	}

	public int getClubId() {
		return clubId;
	}

	public void setClubId(int clubId) {
		this.clubId = clubId;
	}

	public int getSales() {
		return sales;
	}

	public void setSales(int sales) {
		this.sales = sales;
	}

	private String areaName;
	private String summary;
	private String notice;
	private int linkerId;
	private String linkerName;
	private String linkerPhone;
	private int createId;
	private String logo;
	private String traffic;
	private String address;
	private String shopTime;
	private int number;
	private String characteristic;
	private String booknote;
	private int fieldcount;
	private int hasAudit;
	private int recommend;
	private String regionID;
	private int clubId;
	private int sales;
	private String dishasAuditreason;
	private String recommendedreason;

	public String getDishasAuditreason() {
		return dishasAuditreason;
	}

	public void setDishasAuditreason(String dishasAuditreason) {
		this.dishasAuditreason = dishasAuditreason;
	}

	public String getRecommendedreason() {
		return recommendedreason;
	}

	public void setRecommendedreason(String recommendedreason) {
		this.recommendedreason = recommendedreason;
	}
}
