/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.entity;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Project: yxt_hbdd <br/>
 * File: LoginTabla.java <br/>
 * Class: org.jbase.website.entity.LoginTabla <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: www.yxtsoft.com <br/>
 * Makedate: 2016年6月13日 下午6:31:43 <br/>
 * 
 * @author liyi
 * @version 1.0
 * @since 1.0
 */
@Table(name = "login_table")
public class LoginTabla extends BaseEntity {
	private static final long serialVersionUID = -2115924221341289653L;
	private int loginNumbers;
	private String sessionString;

	/**
	 * @return the loginNumbers
	 */
	public int getLoginNumbers() {
		return loginNumbers;
	}

	/**
	 * @param loginNumbers
	 *            the loginNumbers to set
	 */
	public void setLoginNumbers(int loginNumbers) {
		this.loginNumbers = loginNumbers;
	}

	/**
	 * @return the sessionString
	 */
	public String getSessionString() {
		return sessionString;
	}

	/**
	 * @param sessionString
	 *            the sessionString to set
	 */
	public void setSessionString(String sessionString) {
		this.sessionString = sessionString;
	}

}
