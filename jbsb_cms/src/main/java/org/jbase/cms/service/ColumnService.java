/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jbase.cms.entity.Column;
import org.jbase.cms.ext.beetl.CmsFunctionPackage;
import org.jbase.common.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Project: fw_common <br/>
 * File: ColumnService.java <br/>
 * Class: com.yxt.cms.service.ColumnService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:14:01 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class ColumnService extends EntityService<Column> {

	@Autowired
	private CmsFunctionPackage cmsFunctionPackage;

	@Override
	protected Class<Column> clzz() {
		return Column.class;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @return
	 */
	public List<Column> findParentList() {
		List<Column> list = sql.select("cms_column.findParentList", Column.class, null);
		return list;
	}

	public List<Column> findAllList() {
		List<Column> oneList = findParentList();
		for (Column column : oneList) {
			column.getTails().put("childList", findChildListByParentId(column.getId()));
		}
		return oneList;
	}

	/**
	 * 
	 * 描述 : 列出所有顶级栏目以及其子栏目. <br/>
	 * <p>
	 * 
	 * @return
	 */
	public List<Column> listNavWithChilds() {
		List<Column> oneList = listNavTop();
		for (Column column : oneList) {
			column.getTails().put("childList", findChildListByParentId(column.getId()));
		}
		return oneList;
	}

	public List<Column> listNavTop() {
		return sql.select("cms_column.listNavTop", Column.class, null);
	}

	public List<Column> listNavByParentId(int parentId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", parentId);
		return sql.select("cms_column.listNavByParentId", Column.class, map);
	}

	public List<Column> findChildListByParentId(int parentId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", parentId);
		List<Column> list = sql.select("cms_column.findChildList", Column.class, map);
		return list;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param articleId
	 */
	public Column fetchByArticleId(int articleId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("articleId", articleId);
		return sql.selectSingle("cms_column.fetchByArticleId", map, Column.class);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param childId
	 * @return
	 */
	public List<Column> fetchByChildId(List<Column> list, int childId) {
		if (list == null)
			list = new ArrayList<Column>();
		Column childCol = fetchById(childId);
		list.add(childCol);
		int parentId = childCol.getParentId();
		if (parentId == 0) {
			return list;
		} else {
			return fetchByChildId(list, parentId);
		}

	}

	@Override
	public int insertHolderId(Map<String, Object> map) {
		int re = super.insertHolderId(map);
		cmsFunctionPackage.remave(CmsFunctionPackage.COLUMN_WITH_CHILDS);
		return re;
	}

	@Override
	public int update(Map<String, Object> map) {
		int re = super.update(map);
		cmsFunctionPackage.remave(CmsFunctionPackage.COLUMN_WITH_CHILDS);
		return re;
	}

	@Override
	public int deleteById(int id) {
		int re = super.deleteById(id);
		cmsFunctionPackage.remave(CmsFunctionPackage.COLUMN_WITH_CHILDS);
		return re;
	}

	@Override
	public void deleteByIds(String ids) {
		super.deleteByIds(ids);
		cmsFunctionPackage.remave(CmsFunctionPackage.COLUMN_WITH_CHILDS);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param ids
	 * @param sorts
	 * @return
	 */
	public boolean updateSort(String[] ids, String[] sorts) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		int length = ids.length;
		for (int i = 0; i < length; i++) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", ids[i]);
			map.put("sort", sorts[i]);
			list.add(map);
		}
		int[] updateByIdBatch = sql.updateBatch("cms_column.updateSort", list);
		boolean result = updateByIdBatch.length == length ? true : false;
		if (result)
			cmsFunctionPackage.remave(CmsFunctionPackage.COLUMN_WITH_CHILDS);
		return result;
	}

	public Column getByUniquekey(String key) {
		Map<String, Object> paras = new HashMap<String, Object>();
		paras.put("key", key);
		Column re = sql.selectSingle("cms_column.getByUniquekey", paras, Column.class);
		return re;
	}

	public Column getByHref(HttpServletRequest request) {
		String href = request.getServletPath();

		Map<String, Object> paras = new HashMap<String, Object>();
		paras.put("key", href);
		Column re = sql.selectSingle("cms_column.getByHref", paras, Column.class);
		return re;
	}

}
