/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.service;

import java.util.Map;

import org.jbase.cms.entity.SlideCat;
import org.jbase.cms.ext.beetl.CmsFunctionPackage;
import org.jbase.common.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Project: fw_common <br/>
 * File: ColumnService.java <br/>
 * Class: com.yxt.cms.service.ColumnService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:14:01 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class SlideCatService extends EntityService<SlideCat> {

	@Autowired
	private CmsFunctionPackage cmsFunctionPackage;

	@Override
	protected Class<SlideCat> clzz() {
		return SlideCat.class;
	}

	@Override
	public int update(Map<String, Object> map) {
		int re = super.update(map);
		Object cid = map.get("id");
		cmsFunctionPackage.remave(CmsFunctionPackage.SLIDE_CAT_WITH_CHILDS + cid);
		return re;
	}

	@Override
	public int deleteById(int id) {
		int re = super.deleteById(id);
		cmsFunctionPackage.remave(CmsFunctionPackage.SLIDE_CAT_WITH_CHILDS + id);
		return re;
	}

	@Override
	public void deleteByIds(String ids) {
		super.deleteByIds(ids);
		String[] split = ids.split(",");
		for (String cid : split) {
			cmsFunctionPackage.remave(CmsFunctionPackage.SLIDE_CAT_WITH_CHILDS + cid);
		}
	}

}
