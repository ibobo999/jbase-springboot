/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.cms.entity.LoginTabla;
import org.jbase.common.service.EntityService;
import org.springframework.stereotype.Service;

/**
 * Project: yxt_hbdd <br/>
 * File: LoginService.java <br/>
 * Class: org.jbase.website.sevice.LoginService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: www.yxtsoft.com <br/>
 * Makedate: 2016年6月13日 下午6:42:01 <br/>
 * 
 * @author liyi
 * @version 1.0
 * @since 1.0
 */
@Service
public class LoginService extends EntityService<LoginTabla> {

	/* (non-Javadoc)
	 * @see org.jbase.common.service.EntityService#clzz()
	 */
	@Override
	protected Class<LoginTabla> clzz() {
		// TODO Auto-generated method stub
		return LoginTabla.class;
	}
	public int queryLoginNumber(String loginSession){
//		Map<String, Object> map = new HashMap<String, Object>();
//		List<LoginTabla> list =sql.select("loginnumber.findloginnumber", LoginTabla.class, map);
//		return list;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sessionString", loginSession);
		List<LoginTabla> list =sql.select("loginnumber.findloginnumber", LoginTabla.class, map);
//		for(int i=0;i<list.size();i++){
//			if(list.size()==0 || !list.get(i).getSessionString().equals(loginSession)){
//				LoginTabla log=new LoginTabla();
//				log.setSessionString(loginSession);
//				log.setLoginNumbers(1);
//				sql.insert(log);	
//			}
//		}
		if(list.size()==0){
			LoginTabla log=new LoginTabla();
			log.setSessionString(loginSession);
			log.setLoginNumbers(1);
			sql.insert(log);	
		}
		Map<String, Object> map1 = new HashMap<String, Object>();
		int list1 =sql.intValue("loginnumber.findCount", map1);
		return list1;
	}
	public void svaeLoginNumber(String loginSession,String loginnumber){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sessionString", loginSession);
		List<LoginTabla> list =sql.select("loginnumber.findloginnumber", LoginTabla.class, map);
//		if(list.size()!=0 && !list.get(0).getSessionString().equals(loginSession)){
//			
//			LoginTabla loginTabla=list.get(0);
//			loginTabla.setSessionString(loginSession);
//			loginTabla.setLoginNumbers(loginTabla.getLoginNumbers()+1);
//			sql.update("loginnumber.updatesession", loginTabla);
//		}else if(list.size()==0){
//			LoginTabla log=new LoginTabla();
//			log.setSessionString(loginSession);
//			log.setLoginNumbers(1);
//			sql.insert(log);
//		}
		for(int i=0;i<list.size();i++){
			if(list.size()==0 || !list.get(0).getSessionString().equals(loginSession)){
				LoginTabla log=new LoginTabla();
				log.setSessionString(loginSession);
				log.setLoginNumbers(1);
				sql.insert(log);	
			}
		}
	
	}

}
