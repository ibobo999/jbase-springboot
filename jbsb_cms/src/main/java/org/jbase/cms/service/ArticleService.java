/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.cms.entity.Article;
import org.jbase.cms.entity.Column;
import org.jbase.common.service.EntityService;
import org.jbase.common.utils.Pager;
import org.springframework.stereotype.Service;

/**
 * Project: fw_common <br/>
 * File: ColumnService.java <br/>
 * Class: com.yxt.cms.service.ColumnService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:14:01 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class ArticleService extends EntityService<Article> {

	@Override
	protected Class<Article> clzz() {
		return Article.class;
	}

	@Override
	protected Map<String, Object> listWithPageWhere(Map<String, String> map) {

		StringBuilder sb = new StringBuilder(" 1=1 ");
		for (String key : map.keySet()) {
			if ("columnId".equals(key)) {
				if (!"0".equals(map.get(key))) {
					sb.append(" and columnId=" + map.get(key));
				}
			} else {
				sb.append(" and " + key + " like '%" + map.get(key) + "%'");
			}
		}
		String tableName = sql.getNc().getTableName(clzz());
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("table_name", tableName);
		where.put("sql_where", sb.toString());
		return where;
	}

	@Override
	public Pager listWithPage(Map<String, String> map, Pager pager) {
		return listWithPageList("common.count", "cms_article.searchAndPager", map, pager);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param column
	 * @return
	 */
	public List<Article> listByColumn(Column column) {
		return listAllByColumnId(column.getId());
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param column
	 * @return
	 */
	public List<Article> listAllByColumnId(int columnId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("columnId", columnId);
		return sql.select("cms_article.listAllByColumnId", Article.class, map);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param childId
	 * @return
	 */
	public List<Article> listByColumnId(int columnId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("columnId", columnId);
		return sql.select("cms_article.listByColumnId", Article.class, map);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 */
	public void updateReaderNumber(int id, int hits) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("hits", hits);
		super.update(map);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param columnId
	 * @param start
	 * @param pageSize
	 * @param orderkey
	 * @return
	 */
	public Pager pageByColumnId(int columnId, int start, int size) {
		Pager pager = new Pager(start, size);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("columnId", columnId);
		Integer recordCount = sql.intValue("cms_article.pageByColumnId_count", map);
		pager.setRecordCount(recordCount);
		List<Article> list = sql.select("cms_article.pageByColumnId_page", Article.class, map, pager.getStart(),
				pager.getPageSize());
		pager.setList(list);
		return pager;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param skey
	 * @return
	 */
	public List<Article> findByKeyword(String skey) {
		String sqls = "select * from cms_article where title like '%" + skey + "%' order by createTime";
		return sql.execute(sqls, Article.class, null);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param ids
	 * @param sorts
	 * @return
	 */
	public boolean updateSort(String[] ids, String[] sorts) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		int length = ids.length;
		for (int i = 0; i < length; i++) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", ids[i]);
			map.put("sort", sorts[i]);
			list.add(map);
		}
		int[] updateByIdBatch = sql.updateBatch("cms_article.updateSort", list);
		return updateByIdBatch.length == length ? true : false;
	}
}
