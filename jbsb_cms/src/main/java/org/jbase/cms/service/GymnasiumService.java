package org.jbase.cms.service;

import java.util.Map;

import org.jbase.cms.entity.PlaceGymnasium;
import org.jbase.common.service.EntityService;
import org.jbase.common.utils.Pager;
import org.springframework.stereotype.Service;

@Service
public class GymnasiumService extends EntityService<PlaceGymnasium> {

	@Override
	protected Class<PlaceGymnasium> clzz() {
		return PlaceGymnasium.class;
	}

	/**
	 * 待审核球馆列表
	 * 
	 * @param map
	 *            参数集合
	 * @param pager
	 *            分页对象
	 * @return
	 */
	public Pager getGyListForSH(Map<String, String> map, Pager pager) {
		pager = listWithPageList("common.hasAudit6", "common.hasAudit6List", map, pager);
		return pager;
	}

	public Pager listWithPage2(Map<String, String> map, Pager pager) {
		pager = listWithPageList("common.count2", "common.searchAndPager2", map, pager);
		return pager;
	}

}
