package org.jbase.admin.cms.controller;

import javax.servlet.http.HttpServletRequest;

import org.jbase.cms.entity.PlaceGymnasium;
import org.jbase.cms.service.GymnasiumService;
import org.jbase.common.service.EntityService;
import org.jbase.common.utils.Pager;
import org.jbase.common.utils.Requester;
import org.jbase.common.web.EntityController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("admin_cms_gymnasium_controller")
@RequestMapping("/admin/cms/gymnasium")
public class GymnasiumController extends EntityController<PlaceGymnasium> {
	@Autowired
	private GymnasiumService gymnasiumservice;

	@Override
	protected EntityService<PlaceGymnasium> service() {
		// TODO Auto-generated method stub
		return gymnasiumservice;
	}

	@RequestMapping("/list")
	public void list(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = gymnasiumservice.getGyListForSH(warp.getSearchKeys(), warp.getPager());
		request.setAttribute("pager", pager);
	}

	@RequestMapping("/recommendlist")
	public void recommendlist(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = gymnasiumservice.listWithPage2(warp.getSearchKeys(), warp.getPager());
		request.setAttribute("pager", pager);
	}

	@RequestMapping("/recommendedit")
	public void recommendedit(@RequestParam int id, HttpServletRequest request) {
		PlaceGymnasium t = gymnasiumservice.fetchById(id);
		request.setAttribute("obj", t);
	}
}
