/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.cms.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jbase.cms.entity.Slide;
import org.jbase.cms.entity.SlideCat;
import org.jbase.cms.service.SlideCatService;
import org.jbase.cms.service.SlideService;
import org.jbase.common.service.EntityService;
import org.jbase.common.utils.Requester;
import org.jbase.common.web.EntityController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSON;

/**
 * Project: fw_admin <br/>
 * File: ColumnController.java <br/>
 * Class: com.yxt.admin.controller.cms.ColumnController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:15:46 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("admin_cms_slide_controller")
@RequestMapping("/admin/cms/slide")
public class SlideController extends EntityController<Slide> {
	@Autowired
	private SlideCatService slideCatService;
	@Autowired
	private SlideService slideService;

	@Override
	protected EntityService<Slide> service() {
		return slideService;
	}

	@Override
	public void list(HttpServletRequest request) {
		request.setAttribute("catList", slideCatService.list());
		super.list(request);
	}

	@Override
	public void add(HttpServletRequest request) {
		int catId = 0;
		String cid = request.getParameter("cid");
		if (!StringUtils.isEmpty(cid))
			catId = Integer.parseInt(cid);
		request.setAttribute("catList", slideCatService.list());
		request.setAttribute("catId", catId);
		super.add(request);
	}

	@Override
	public void edit(int id, HttpServletRequest request) {
		request.setAttribute("catList", slideCatService.list());
		super.edit(id, request);
	}

	@RequestMapping("/listCat")
	public void listCat(HttpServletRequest request) {
		request.setAttribute("list", slideCatService.list());
	}

	@RequestMapping("/addCat")
	public void addCat(HttpServletRequest request) {
		request.setAttribute("catList", slideCatService.list());
	}

	@ResponseBody
	@RequestMapping(value = "/addCatSave", produces = "text/plain;charset=UTF-8")
	public String addCatSave(HttpServletRequest request) {
		SlideCat slideCat = new SlideCat();
		slideCat.setName(request.getParameter("name"));
		slideCat.setIdname(request.getParameter("idname"));
		slideCat.setRemarks(request.getParameter("remarks"));
		slideCatService.insert(slideCat);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "200");
		map.put("retMsg", "成功");
		map.put("retObj", "object");
		return JSON.toJSONString(map);
	}

	@RequestMapping("/editCat")
	public void editCat(@RequestParam int id, HttpServletRequest request) {
		SlideCat t = slideCatService.fetchById(id);
		request.setAttribute("obj", t);
	}

	@ResponseBody
	@RequestMapping(value = "/editCatSave", produces = "text/plain;charset=UTF-8")
	public String editCatSave(HttpServletRequest request) {
		slideCatService.update(Requester.warp(request).getParams());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "200");
		map.put("retMsg", "成功");
		map.put("retObj", "object");
		return JSON.toJSONString(map);
	}

	@RequestMapping("/delCat")
	public String delCat(@RequestParam String ids) {
		slideCatService.deleteByIds(ids);
		return "redirect:listCat";
	}

	@ResponseBody
	@RequestMapping(value = "/updateSort", produces = "text/plain;charset=UTF-8")
	public String updateSort(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String[] ids = request.getParameterValues("ids");
		String[] sorts = request.getParameterValues("sorts");
		if (ids.length != sorts.length)
			return fail("保存失败，保存的数据条数不一致！");
		boolean res = slideService.updateSort(ids, sorts);
		if (res)
			return ok();
		else
			return fail("保存失败！");
	}
}
