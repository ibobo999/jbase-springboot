/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.coder.component.sys;

import java.util.List;
import java.util.Map;

import org.jbase.GlobalConstants;

/**
 * Project: jb_coder <br/>
 * File: CbSysEntity.java <br/>
 * Class: org.jbase.coder.component.xwl.XwlDomain <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年8月3日 上午12:34:11 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public class CbSysAdd extends SysCoderBuilder {

	@Override
	protected String getTplName() {
		return "/tpl_add.tpl";
	}

	@Override
	protected Map<String, Object> getBindMap(List<Map<String, Object>> tableAttrs, String entityclassname,
			String tablename, String modulename, String tableDesc) {
		return super.getBindMap(tableAttrs, entityclassname, tablename, modulename, tableDesc);
	}

	@Override
	protected String getSourceFile(String entityclassname) {
		return "add.html";
	}

	@Override
	protected String getSourcePath(String modulename, String entityclassname) {
		return GlobalConstants.Real_Path_Root + "/code_sys/src/main/resources/templates/admin/"
				+ entityclassname;
	}
}
