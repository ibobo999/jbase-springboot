/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.coder.component.xwl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.jbase.GlobalConstants;

/**
 * Project: jb_coder <br/>
 * File: HebimCoderBuilder.java <br/>
 * Class: org.jbase.coder.component.jbase.JbaseCoderBuilder <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年8月1日 下午9:16:10 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public abstract class XwlCoderBuilder {

	protected static String SOURCE_JAVA = GlobalConstants.Real_Path_Root + "/code_source" + File.separator + "java"
			+ File.separator;

	protected static String SOURCE_TPL = GlobalConstants.Real_Path_Root + "/code_source" + File.separator + "template"
			+ File.separator;

	protected static String SOURCE_ROOT = SOURCE_JAVA + "org" + File.separator + "jbase" + File.separator;

	public static GroupTemplate gt = null;

	static {
		Configuration conf = null;
		try {
			conf = Configuration.defaultConfiguration();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conf.setStatementStart("@");
		conf.setStatementEnd(null);
		gt = new GroupTemplate(new ClasspathResourceLoader("/tpl_coder/xwl"), conf);

	}

	public static void genCode(String sourcePath, String sourceFile, String tplName, Map<String, Object> bind) {

		Template template = gt.getTemplate(tplName);
		template.binding(bind);
		String code = template.render();

		File f = new File(sourcePath);
		f.mkdirs();
		File target = new File(sourcePath, sourceFile);
		FileWriter writer = null;
		try {
			writer = new FileWriter(target);
			writer.write(code.toString());
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void genCode(String tablename, String modulename, String tableDesc, List<Map<String, Object>> tableAttrs) {
		genCode(tablename, modulename, getEntityClassName(tablename), tableDesc, tableAttrs);
	}

	public void genCode(String tablename, String modulename, String entityclassname, String tableDesc,
			List<Map<String, Object>> tableAttrs) {
		String sourcePath = getSourcePath(modulename, entityclassname);
		String sourceFile = getSourceFile(entityclassname);
		String tplName = getTplName();
		Map<String, Object> bind = getBindMap(tableAttrs, entityclassname, tablename, modulename, tableDesc);
		genCode(sourcePath, sourceFile, tplName, bind);
	}

	protected abstract String getTplName();

	protected Map<String, Object> getBindMap(List<Map<String, Object>> tableAttrs, String entityclassname,
			String tablename, String modulename, String tableDesc) {
		Map<String, Object> bind = new HashMap<String, Object>();
		bind.put("tableAttrs", tableAttrs);
		bind.put("entityclassname", entityclassname);
		bind.put("controllername", entityclassname.toLowerCase());
		bind.put("tablename", tablename.toLowerCase());
		bind.put("modulename", modulename.toLowerCase());
		bind.put("tableDesc", tableDesc);
		bind.put("loclassname", entityclassname.substring(0, 1).toLowerCase() + entityclassname.substring(1));
		
		return bind;
	}

	protected String getSourcePath(String modulename, String controllername) {
		return SOURCE_ROOT + modulename.toLowerCase();
	}

	protected String getSourceFile(String entityclassname) {
		return entityclassname + ".java";
	}

	private String getUpName(String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	private String getEntityClassName(String tablename) {
		return getUpName(tablename);
	}
}
