/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.coder.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jbase.coder.service.CoderService;
import org.jbase.common.utils.Requester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

/**
 * Project: jb_coder <br/>
 * File: CoderController.java <br/>
 * Class: org.jbase.coder.controller.CoderController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年7月13日 下午3:09:39 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("jbase_coder_controller")
@RequestMapping("/coder")
public class CoderController {

	@Autowired
	private CoderService coderService;

	@RequestMapping("/fromdb")
	public void fromdb(HttpServletRequest request) {
		List<Map<String, Object>> listTables = coderService.listTables();
		request.setAttribute("list", listTables);
	}

	@RequestMapping("/fromdblist")
	public void fromdblist(HttpServletRequest request) {
		List<Map<String, Object>> listTables = coderService.listTables();
		request.setAttribute("list", listTables);
	}

	@RequestMapping(value = "/fromdbSave", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String fromdbSave(HttpServletRequest request) {
		Map<String, Object> params = Requester.warp(request).getParams();
		coderService.genCode(params);
		return ok();
	}

	@RequestMapping(value = "/fromdblistSave", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String fromdblistSave(HttpServletRequest request) {
		String[] tbnames = request.getParameterValues("table_checkbox_id");
		coderService.genCodeByTableList(tbnames);
		return ok();
	}

	protected String ok() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "200");
		map.put("retMsg", "成功");
		map.put("retObj", "object");
		return JSON.toJSONString(map);
	}

	protected String fail(String mesg) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "500");
		map.put("retMsg", mesg);
		return JSON.toJSONString(map);
	}
}
