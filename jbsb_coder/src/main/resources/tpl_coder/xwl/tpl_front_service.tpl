package com.xwl.epp.frontend.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.xwl.epp.provider.api.domain.${entityclassname};
import com.xwl.epp.provider.api.service.I${entityclassname}Service;
import com.xwl.util.message.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Project:  epp-frontend<br/>
 * File: ${entityclassname}Service.java <br/>
 * Class: com.xwl.epp.frontend.service.${entityclassname}Service <br/>
 * Description: ${tableDesc} . <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company:  <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
\@Slf4j
\@Service
public class ${entityclassname}Service {
    \@Reference
    private com.xwl.epp.provider.api.service.I${entityclassname}Service ${loclassname}Service;


    /**
     * 获取符合条件的 ${tableDesc} 信息并实现分页
     * \@param ${loclassname}  ${tableDesc} 信息
     * \@param pageable 分页信息
     * \@return
     */
    public Message<Page<${entityclassname}>> findAll(${entityclassname} ${loclassname},Pageable pageable) {

        return this.${loclassname}Service.findAll(${loclassname}, pageable);
    }

    /**
     * 编辑 ${tableDesc} 信息
     * \@param ${loclassname}  ${tableDesc} 信息
     * \@return
     */
    public Message editSave(${entityclassname} ${loclassname}){

        return this.${loclassname}Service.editSave(${loclassname});
    }

    /**
     * 根据主键获取指定记录
     * \@param ${loclassname}  ${tableDesc} 信息
     * \@return
     */
    public Message<${entityclassname}> show(${entityclassname} ${loclassname}){

        return this.${loclassname}Service.showById(${loclassname});
    }

    /**
     * 根据主键删除指定记录
     * \@param ${loclassname}  ${tableDesc} 信息
     * \@return
     */
    public Message delete(${entityclassname} ${loclassname}){

        return this.${loclassname}Service.delete(${loclassname});
    }

    /**
     * 插入记录
     * \@param ${loclassname}  ${tableDesc} 信息
     * \@return
     */
    public Message save(${entityclassname} ${loclassname}){

        return this.${loclassname}Service.save(${loclassname});
    }

}