<link rel="stylesheet" type="text/css" href="../static/js/datatables/media/css/jquery.dataTables.min.css" th:href="\@{/js/datatables/media/css/jquery.dataTables.min.css}"/>
<link rel="stylesheet" type="text/css" href="../static/js/datatables/media/assets/css/datatables.min.css" th:href="\@{/js/datatables/media/assets/css/datatables.min.css}"/>
<link rel="stylesheet" type="text/css"  href="../static/js/datatables/extras/TableTools/media/css/TableTools.min.css" th:href="\@{/js/datatables/extras/TableTools/media/css/TableTools.min.css}"/>
<link rel="stylesheet" type="text/css" href="../static/bs_pagination/jquery.bs_pagination.min.css" th:href="\@{/bs_pagination/jquery.bs_pagination.min.css}" />
<div class="container">
    <div class="row">
        <div id="content" class="col-lg-12">
            <!-- PAGE HEADER-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header">
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="index.html">Home</a>
                            </li>
                            <li>${tableDesc}管理功能</li>

                        </ul>
                        <!-- /BREADCRUMBS -->
                        <div class="clearfix">
                            <h3 class="content-title pull-left">${tableDesc}管理</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /EXPORT TABLES -->
            <div class="row">
                <div class="col-md-12">
                    <!-- BOX -->
                    <div class="box border green">
                        <div class="box-title">
                            <h4><i class="fa fa-table"></i>${tableDesc}列表</h4>
                        </div>
                        <div class="box-body">
                            <form class="form-inline" id="query" role="form">
                                <div class="form-group width-20">
                                    <label>姓名</label>
                                    <input type="text" class="form-control" placeholder=""/>
                                </div>
                                <div class="form-group">
                                    <div class="divide-20"></div>
                                    <input type="submit" class="btn btn-success" value="查询"/>
                                </div>
                            </form>
                            <div>
                                <div class="divide-20"></div>
                                <a href="#${entityclassname}/add.html"><input class="btn btn-primary" type="button"
                                                                                value="增加"/></a>&nbsp;&nbsp;&nbsp;
                                <a href="#E${entityclassname}/edit.html"><input class="btn btn-primary" type="button"
                                                                                 value="修改"/></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio"/>在职
                                <input type="radio"/>离职
                                <input class="btn btn-primary pull-right" type="button" value="导出"/>

                                <div class="divide-20"></div>
                            </div>
                            <table id="datatable1" cellpadding="0" cellspacing="0" border="0"
                                   class="font-14 datatable table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>编号</th>
                                    @for(attr in tableAttrs){
                                    <th>${attr.comment}</th>
                                    @}
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <!-- /DATA TABLES -->
                            <div id="pagination"></div>
                            <!-- /EXPORT TABLES -->
                        </div>
                    </div>
                    <!-- /BOX -->
                </div>
            </div>

            <div class="footer-tools">
				<span class="go-top">
					<i class="fa fa-chevron-up"></i> Top
				</span>
            </div>
        </div>
        <!-- /CONTENT-->
    </div>
</div>

<script src="../static/js/datatables/media/js/jquery.dataTables.min.js" th:src="\@{/js/datatables/media/js/jquery.dataTables.min.js}" ></script>
<script src="../static/js/datatables/media/assets/js/datatables.min.js" th:src="\@{/js/datatables/media/assets/js/datatables.min.js}" ></script>
<script src="../static/js/datatables/extras/TableTools/media/js/TableTools.min.js" th:src="\@{/js/datatables/extras/TableTools/media/js/TableTools.min.js}" ></script>
<script src="../static/bs_pagination/jquery.bs_pagination.js" th:src="\@{/bs_pagination/jquery.bs_pagination.js}" ></script>
<script src="../static/bs_pagination/localization/en.min.js" th:src="\@{/bs_pagination/localization/en.min.js}" ></script>


<script type="text/javascript">

    var _${loclassname} = {

        configData: {
            start: 0,
            rows: 10
        },
        init: function () {
            _${loclassname}.loadData();
        },
        loadData: function (page, size) {

            var data = $("#query").serialize() || {};
            data.page = page || _${loclassname}.configData.page;
            data.size = size || _${loclassname}.configData.size;
            
            $.getJSON('${entityclassname}/list', data, function (result) {
                var $tbody = $('tbody').empty();
                $.each(result.data.content, function (index, item) {
                    $tbody.append('<tr>' +
                    '<td>' + (result.data.number * result.data.size + index + 1) + '</td>' +
                    
                    @for(attr in tableAttrs){
                    '<td>' + item.${attr.name} + '</td>' + 
                    @}
                    
                    '<td>' +
                    '<div class="btn-group" role="group" aria-label="">' +
                    '<a href="#${entityclassname}/edit.html?id=' + item.id + '" class="btn btn-info btn-sm" data-toggle="modal" ><span class="fa fa-edit"></span></a>' +
                    '<a  href="javascript:_${loclassname}.del${entityclassname}(' + item.id + ');" class="btn btn-danger btn-sm" ><span class="fa fa-trash-o">删除</span></a>' +
                    '</div>' +
                    '</td>' +
                    '</tr>');
                });
                $("#pagination").bs_pagination({

                    showGoToPage: true,
                    rowsPerPage: result.data.size,
                    totalPages: result.data.totalPages,
                    totalRows: result.data.totalElements,
                    onChangePage: function (event, data) {

                        _${loclassname}.loadData(data.currentPage - 1, data.rowsPerPage);
                    }
                });
            });
        },
        del${entityclassname}: function (id) {

            if (confirm("确定要删除吗？")) {
                $.getJSON("${entityclassname}/delete", {"id": id}, function (data) {

                    alert(data.msg);
                    if (data.success) {

                        _${loclassname}.init();
                    }
                });
            }
        }
    };
    $(function () {

        _${loclassname}.init();
    })
</script>
