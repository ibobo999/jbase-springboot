package com.xwl.epp.provider.impl.service;

import com.xwl.epp.provider.api.domain.${entityclassname};
import com.xwl.epp.provider.api.service.I${entityclassname}Service;
import com.xwl.epp.provider.impl.dao.I${entityclassname}Repository;
import com.xwl.util.message.Message;
import com.xwl.util.message.MessageCode;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.jpa.criteria.OrderImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.alibaba.dubbo.config.annotation.Service;

import java.util.List;

/**
 * Description: ${tableDesc}. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company:  <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author 刘战洪
 * \@version 1.0
 * \@since 1.0
 */
\@Slf4j
\@Service
public class ${entityclassname}ServiceImpl implements I${entityclassname}Service {

    \@Autowired
    private I${entityclassname}Repository ${loclassname}Repository;

    /**
     * 根据条件获取指定记录
     *
     * \@param ${loclassname} ${tableDesc}信息
     * \@return
     */
    \@Override
    public Message<List<${entityclassname}>> findAll(${entityclassname} ${loclassname}) {

        // 定义返回消息
        Message<List<${entityclassname}>> message = new Message<>();

        try {
            // 获取符合条件的记录
            List<${entityclassname}> page = this.${loclassname}Repository.findAll((root, query, cb) -> {

                //默认根据主键正序排列
                query.orderBy(new OrderImpl(root.get("id"), true));
                // 获取符合条件的记录
                return query.getRestriction();
            });
            // 设置返回数据
            message.setData(page);
            message.setCode(MessageCode.SUCCESS.getCode());
        } catch (Exception e) {

            log.error("查询结果集失败，{}", e.getMessage());
            // 捕获异常并返回消息
            message.setCode(MessageCode.FAIL.getCode());
        }

        return message;
    }
    
    /**
     * 获取指定条件记录，并分页
     *
     * \@param ${loclassname} 条件信息
     * \@param pageable             分页信息
     * \@return
     */
    \@Override
    public Message<Page<${entityclassname}>> findAll(${entityclassname} ${loclassname}, Pageable pageable) {

        // 定义返回消息
        Message<Page<${entityclassname}>> message = new Message<>();

        try {
            // 获取符合条件的记录
            Page<${entityclassname}> page = (Page<${entityclassname}>) this.${loclassname}Repository.findAll((root, query, cb) -> {

                // 默认根据主键正序排列
                query.orderBy(new OrderImpl(root.get("id"), true));
                // 获取符合条件的记录
                return query.getRestriction();
            }, pageable);
            // 设置返回数据
            message.setData(page);
            message.setCode(MessageCode.SUCCESS.getCode());
        } catch (Exception e) {

            log.error("查询分页结果集失败，{}", e.getMessage());
            // 捕获异常并返回消息
            message.setCode(MessageCode.FAIL.getCode());
        }

        return message;
    }
    
    /**
     * 删除记录根据 id
     *
     * \@param ${loclassname} 条件信息
     * \@return
     */
    \@Override
    public Message delete(${entityclassname} ${loclassname}) {

        Message message = Message.fireFail();

        if (null == ${loclassname}.getId()) {
            message.setMsg("删除失败，请重试");
        } else {
            try {
                // 通过主键查询一条记录
                if (null == this.${loclassname}Repository.findById(${loclassname}.getId())) {
                    message.setMsg("记录不存在");
                } else {
                    // 删除指定主键记录
                    this.${loclassname}Repository.delete(${loclassname});
                    message.setSuccess(true);
                    message.setMsg("删除成功");
                    message.setCode(MessageCode.SUCCESS.getCode());
                }
            } catch (Exception e) {
                log.error("删除失败，{}", e.getMessage());
                message.setMsg("删除失败，请重试");
            }
        }

        return message;
    }
    
    /**
     * 保存信息
     *
     * \@param ${loclassname} 条件信息
     * \@return
     */
    \@Override
    public Message save(${entityclassname} ${loclassname}) {

        Message message = Message.fireFail();

        if (null == ${loclassname}) {
            message.setMsg("插入内容为空");
        } else {
            try {

                // 插入一条记录
                this.${loclassname}Repository.save(${loclassname});
                message.setMsg("插入成功");
                message.setSuccess(true);
                message.setCode(MessageCode.SUCCESS.getCode());
            } catch (Exception e) {

                log.error("删除群组失败，{}", e.getMessage());
                message.setMsg("删除群组失败，请重试");
            }
        }

        return message;
    }
    
    /**
     * 修改内容根据 id
     *
     * \@param ${loclassname} 条件信息
     * \@return
     */
    \@Override
    public Message editSave(${entityclassname} ${loclassname}) {

        Message message = Message.fireFail();

        if (null == ${loclassname}.getId()) {
            message.setMsg("主键为空");
        } else {

            try {
                // 通过主键查询一条记录
                if (null == this.${loclassname}Repository.findById(${loclassname}.getId())) {
                    message.setMsg("更新记录不存在");
                } else {
                    //保存或更新
                    this.${loclassname}Repository.save(${loclassname});
                    message.setMsg("更新成功");
                    message.setSuccess(true);
                    message.setCode(MessageCode.SUCCESS.getCode());
                }
            } catch (Exception e) {

                log.error("删除群组失败，{}", e.getMessage());
                message.setMsg("删除群组失败，请重试");
            }
        }

        return message;
    }
    
    /**
     * 显示信息根据 指定条件
     *
     * \@param ${loclassname} 条件信息
     * \@return
     */
    \@Override
    public Message<${entityclassname}> show(${entityclassname} ${loclassname}) {

        Message<${entityclassname}> message = new Message<${entityclassname}>();
        try {
                // 查询指定条件首条记录
                ${loclassname} = this.${loclassname}Repository.findOne((root, query, cb) -> {

                    //默认根据主键正序排列
                    query.orderBy(new OrderImpl(root.get("id"), true));
                    // 获取符合条件的记录
                    return query.getRestriction();
                });
                if (null == ${loclassname}) {
                    message.setMsg("查询记录不存在");
                } else {
                    // 设置成功获取数据
                    message.setSuccess(true);
                    message.setMsg("查询成功");
                    message.setData(${loclassname});
                    message.setCode(MessageCode.SUCCESS.getCode());
                }
        } catch (Exception e) {
            log.error("查询失败，{}", e.getMessage());
            message.setSuccess(false);
            message.setMsg("查询失败，请重试");
        }

        return message;
    }
    
    /**
     * 显示信息根据 id
     *
     * \@param ${loclassname} 条件信息
     * \@return
     */
    \@Override
    public Message<${entityclassname}> showById(${entityclassname} ${loclassname}) {

        Message<${entityclassname}> message = new Message<${entityclassname}>();
        try {
            if (null == ${loclassname}.getId()) {
                message.setSuccess(false);
                message.setMsg("ID为空");

            } else {
                //查询
                ${loclassname} = this.${loclassname}Repository.findById(${loclassname}.getId());
                if (null == ${loclassname}) {
                    message.setMsg("查询记录不存在");
                } else {
                    // 设置成功获取数据
                    message.setSuccess(true);
                    message.setMsg("查询成功");
                    message.setData(${loclassname});
                    message.setCode(MessageCode.SUCCESS.getCode());
                }
            }
        } catch (Exception e) {
            log.error("查询失败，{}", e.getMessage());
            message.setSuccess(false);
            message.setMsg("查询失败，请重试");
        }

        return message;
    }
}