package com.xwl.epp.provider.api.domain;

import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Project:  epp-provider-api<br/>
 * File: ${entityclassname}.java <br/>
 * Class: com.xwl.epp.provider.api.domain.${entityclassname} <br/>
 * Description: ${tableDesc}. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company:  <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
\@Data
\@NoArgsConstructor
\@Entity
public class ${entityclassname} implements Serializable {

	private static final long serialVersionUID = 1L;
    
    //记录唯一标识
    \@Id
    \@GeneratedValue
    private BigInteger id;
    
	@for(attr in tableAttrs){
	@		if(!isEmpty(attr.comment)){
	//${attr.comment}
	@		}
	private ${attr.type} ${attr.name} ;
	
	@}
	
}
