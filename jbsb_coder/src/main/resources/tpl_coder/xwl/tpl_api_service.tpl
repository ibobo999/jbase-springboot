package com.xwl.epp.provider.api.service;

import com.xwl.epp.provider.api.domain.${entityclassname};

/**
 * Project:  epp-provider-api<br/>
 * File: I${entityclassname}Service.java <br/>
 * Class: com.xwl.epp.provider.api.service.I${entityclassname}Service <br/>
 * Description: ${tableDesc}. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company:  <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
public interface I${entityclassname}Service extends IBaseService<${entityclassname}> {

}