<div class="container">
    <div class="row">
        <div id="content" class="col-lg-12">
            <!-- PAGE HEADER-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header">
                        <!-- STYLER -->

                        <!-- /STYLER -->
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="index.html">Home</a>
                            </li>
                            <li>${tableDesc}管理功能</li>

                        </ul>
                        <!-- /BREADCRUMBS -->
                        <div class="clearfix">
                            <h3 class="content-title pull-left">添加${tableDesc}</h3>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /EXPORT TABLES -->

            <div class="row">
                <div class="col-md-12">
                    <!-- BOX -->
                    <div class="box border primary">
                        <div class="box-title">
                            <h4><i class="fa fa-bell"></i>添加${tableDesc}</h4>
                            <a href="javascript:history.go(-1)"><input class="btn btn-default pull-right" type="button"
                                                                       style="padding:1px 6px;" value="返回"/></a>
                        </div>
                        <div class="box-body big">
                            <form role="form" class="form-horizontal" id="form_save">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="submit" class="btn btn-primary" value="确定" id="btn_save"/>
                                        <input type="button" class="btn btn-primary" value="取消"/>
                                    </div>
                                </div>
								
								@for(attr in tableAttrs){
								@if("insertTime"!=attr.name){
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">${attr.comment}</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="${attr.name}" />
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                @}
								@}

                            </form>
                        </div>
                    </div>
                    <!-- /BOX -->
                </div>

            </div>

            <div class="footer-tools">
					<span class="go-top">
						<i class="fa fa-chevron-up"></i> Top
					</span>
            </div>
        </div>
        <!-- /CONTENT-->
    </div>
</div>

<script type="text/javascript">
    $('#form_save').validator();
    $(function () {
        $('#form_save').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                $.getJSON("${entityclassname}/save", $("#form_save").serialize(), function (data) {
                    alert(data.msg);
                });
            }
            // 不跳转停止在当前页面处理
            return false;
        });

    });
</script>
