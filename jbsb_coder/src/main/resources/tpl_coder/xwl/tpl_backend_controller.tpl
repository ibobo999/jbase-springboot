
package com.xwl.epp.backend.controller;

import com.xwl.epp.backend.service.${entityclassname}Service;
import com.xwl.epp.provider.api.domain.${entityclassname};

import com.xwl.util.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * Project:  epp-backend<br/>
 * File: ${entityclassname}Controller.java <br/>
 * Class: com.xwl.epp.backend.controller.${entityclassname}Controller <br/>
 * Description: ${tableDesc}. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company:  <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */

\@RestController
\@RequestMapping("/${entityclassname}")
public class ${entityclassname}Controller {


    \@Autowired
    private ${entityclassname}Service ${loclassname}Service;

    /**
     * 获取符合条件的 ${tableDesc} 信息并实现分页
     * \@param ${loclassname} ${tableDesc}信息
     * \@param pageable 分页信息
     * \@return
     */
    \@RequestMapping(value = "list")
    Message<Page<${entityclassname}>> findAll(${entityclassname} ${loclassname},Pageable pageable){

        return ${loclassname}Service.findAll(${loclassname}, pageable);
    }

    /**
     * 编辑 ${tableDesc} 信息
     * \@param ${loclassname} ${tableDesc} 信息
     * \@return
     */
    \@RequestMapping(value = "editSave")
    public Message<${entityclassname}> editSave(${entityclassname} ${loclassname}){

        return this.${loclassname}Service.editSave(${loclassname});
    }

    /**
     * 根据主键获取指定记录
     * \@param ${loclassname} ${tableDesc} 信息
     * \@return
     */
    \@RequestMapping(value = "show")
    public Message<${entityclassname}> show(${entityclassname} ${loclassname}){

        return this.${loclassname}Service.show(${loclassname});
    }

    /**
     * 根据主键删除指定记录
     * \@param ${loclassname} ${tableDesc} 信息
     * \@return
     */
    \@RequestMapping(value = "delete")
    Message delete(${entityclassname} ${loclassname}){

        return this.${loclassname}Service.delete(${loclassname});
    }

    /**
     * 插入记录
     * \@param ${loclassname} ${tableDesc} 信息
     * \@return
     */
    \@RequestMapping(value = "save")
    Message save(${entityclassname} ${loclassname}){

        return this.${loclassname}Service.save(${loclassname});
    }
   
}


