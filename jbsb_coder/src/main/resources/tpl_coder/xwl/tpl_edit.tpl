<div class="container">
    <div class="row">
        <div id="content" class="col-lg-12">
            <!-- PAGE HEADER-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header">
                        <!-- STYLER -->

                        <!-- /STYLER -->
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="index.html">Home</a>
                            </li>
                            <li>${tableDesc}管理功能</li>

                        </ul>
                        <!-- /BREADCRUMBS -->
                        <div class="clearfix">
                            <h3 class="content-title pull-left">编辑${tableDesc}</h3>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /EXPORT TABLES -->

            <div class="row">
                <div class="col-md-12">
                    <!-- BOX -->
                    <div class="box border primary">
                        <div class="box-title">
                            <h4><i class="fa fa-bell"></i>编辑${tableDesc}</h4>
                            <a href="#${entityclassname}/index.html"><input class="btn btn-child-head-back btn-default pull-right" type="button"
                                                                      value="返回"/></a>
                        </div>
                        <div class="box-body big">
                            <form class="form-horizontal" role="form" method="post" id="form_save">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="submit" class="btn btn-primary" value="确定" id="btn_save"/>
                                        <input type="button" class="btn btn-primary" value="取消"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">编号</label>

                                    <div class="col-sm-8">
                                        <input type="text" id="id" name="id" class="form-control" readonly="readonly"/>

                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
								
								@for(attr in tableAttrs){
								@if("insertTime"!=attr.name){
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">${attr.comment}</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="${attr.name}"  name="${attr.name}" />
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                
								@}
								@}
								
                            </form>
                        </div>
                    </div>
                    <!-- /BOX -->
                </div>

            </div>

            <div class="footer-tools">
					<span class="go-top">
						<i class="fa fa-chevron-up"></i> Top
					</span>
            </div>
        </div>
        <!-- /CONTENT-->
    </div>
</div>

<script type="text/javascript">
    $('#form_save').validator();
    var param = new UrlParam();
    $.getJSON("${entityclassname}/show", param, function (data) {

        if (data.success) {

            $.each(data.data, function (name, value) {
                if ($("#" + name)) {

                    $("#" + name).val(value);
                }
            });
        } else {

            alert(data.msg);
        }
    });
    $(function () {
        $('#form_save').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                $.getJSON("${entityclassname}/editSave", $("#form_save").serialize(), function (data) {
                    alert(data.msg);
                });
            }
            // 不跳转停止在当前页面处理
            return false;
        });

    });
</script>