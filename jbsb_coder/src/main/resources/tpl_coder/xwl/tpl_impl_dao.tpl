package com.xwl.epp.provider.impl.dao;

import com.xwl.epp.provider.api.domain.${entityclassname};
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigInteger;

/**
 * Project:  epp-provider-impl<br/>
 * File: I${entityclassname}Repository.java <br/>
 * Class: com.xwl.epp.provider.impl.dao.I${entityclassname}Repository <br/>
 * Description: ${tableDesc}. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company:  <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
public interface I${entityclassname}Repository extends PagingAndSortingRepository<${entityclassname},String>, JpaSpecificationExecutor<${entityclassname}> {

    public ${entityclassname} findById(BigInteger id);
}