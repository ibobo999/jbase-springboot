<div id="tab-system" class="HuiTab">
	<div class="tabBar cl">
		<span class="current">${tableDesc}列表</span>
		<span onclick="list_add()">添加${tableDesc}</span>
	</div>
	<br>
</div>
<div>
  <form>
	<input type="text" class="input-text" style="width:200px" placeholder="输入名称" id="_s_name" name="_s_name" />
	<button type="button" class="btn btn-success" onclick="reload_table();"><i class="Hui-iconfont">&#xe665;</i> 查找</button>
	<button type="reset" class="btn" ><i class="Hui-iconfont">&#xe68f;</i> 重置</button>
  </form>
</div>
<div class="cl pd-5 bg-1 bk-gray mt-20"> 
	<span class="l">
		<a href="javascript:;" onclick="list_del()" class="btn btn-danger size-S radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a>
	</span>
	<span class="r"><select class="select" onchange="list_changeSize(this.value)">
			<option value="10">10</option>
			<option value="25">25</option>
			<option value="50">50</option>
			<option value="100">100</option>
		</select>
	</span>
</div>
<table id="table_list" class="table table-border table-bordered table-bg table-striped table-hover">
	<thead>
		<tr class="text-c">
			<th width="25"><span><input type="checkbox" name="" value="" onclick="checkAll()"/></span></th>
			<th width="40">ID</th>
			   
	@for(attr in tableAttrs){
      <th>${attr.comment}</th>
	@}
			
			<th>操作</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<script type="text/javascript">
var _table;
function reload_table(){
	if(_table){
		_table.draw();
	}else{
		alert("没有表格");
	}
}
function list_add(){
	var params = "";
	HtmlRender('\${BASE}/${modulename}/${entityclassname}/add'+params);
}
function list_del(id){
	var select_ids = "";
	if(id){
		select_ids=id;
	}else{
	    $("input[name='table_checkbox_id']:checked").each(function(){
	    	select_ids+=$(this).val()+",";   
	    });
	    if(""==select_ids){
	    	layer.msg('请先选择要删除的数据！', {icon: 3,offset: '45px'});
	    	return false;
	    }
	}
	layer.confirm('确认删除所选记录?', {icon: 3, title:'警告'}, function(index){	    
	    layer.close(index);
	    layer.load(1, {shade: [0.3,'#fff']});
	    $.post("\${BASE}/${modulename}/${entityclassname}/delete",{ids:select_ids},function(jsondata){
			layer.closeAll('loading');
			if( jsondata.code == "001" ){
				layer.msg(jsondata.msg, {icon: 6,offset: '45px'});
				reload_table();
			}else{
				layer.msg(jsondata.msg, {icon: 5,offset: '45px'});
			alert("XX");
			}
	    },"json");
	});
}
function list_edit(id){
    HtmlRender("\${BASE}/${modulename}/${entityclassname}/edit",{id:id});
}
function list_show(id){
	$.post("\${BASE}/${modulename}/${entityclassname}/show",{id:id}, function(str){
		layer.open({
			type: 1,
			shadeClose:true,
			title :'${tableDesc}信息',
			area: ['800px', '500px'],
			content: str
		});
	});
}
function list_changeSize(size){
	if(_table){
		_table.page.len( size ).draw();
	}else{
		alert("没有表格");
	}
}
function checkAll(){
	$("#table_list").find("tr > td:first-child input:checkbox").prop("checked",$("table thead th input:checkbox").prop("checked"));
}
$(function(){
	_table = $("#table_list").DataTable({
		"lengthChange":false,
		"autoWidth": false,
		"scrollX": true,
		"ordering":false,
		"searching":false,
		"serverSide":true,
		"ajax": {
		    "url": "\${BASE}/${modulename}/${entityclassname}/list",
		    "type": "POST",
		    "data": function ( d ) {
		        d._s_name = $('#_s_name').val();
	        }
		},
        "columns": [
            { "data": "id","class":"text-c",
              "render": function(data, type, row) {
            	  return '<input type="checkbox" name="table_checkbox_id" value="' + data + '" title="' + data + '" id="checkbox" />';
               }
            },
            { "data": "id" },
	@for(attr in tableAttrs){
      { "data": "${attr.name}","defaultContent":"" },
	@}
            { "data": "id","class":"text-c",
              "render": function(data, type, row) {
            	  return '<a class="btn btn-secondary size-MINI" href="javascript:;" title="编辑" onclick="list_edit(\''+data+'\')"><i class="Hui-iconfont">&#xe6df;</i> 编辑</a>&nbsp;'
            	  		+'<a class="btn btn-danger size-MINI" href="javascript:;" title="删除" onclick="list_del(\''+data+'\')"><i class="Hui-iconfont">&#xe6e2;</i> 删除</a>&nbsp;'
            	  		+'<a class="btn btn-success size-MINI" href="javascript:;" title="查看" onclick="list_show(\''+data+'\')"><i class="Hui-iconfont">&#xe616;</i> 查看</a>';
               }
            }
        ]
	});
})
</script>