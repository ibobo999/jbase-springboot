<div id="tab-system" class="HuiTab">
	<div class="tabBar cl">
		<span onclick="HtmlRender('\${BASE}/${modulename}/${entityclassname}/index')">${tableDesc}列表</span>
		<span onclick="HtmlRender('\${BASE}/${modulename}/${entityclassname}/add')">添加${tableDesc}</span>
		<span class="current">修改${tableDesc}信息</span>
	</div>
	<br>
</div>

<div class="row cl">
  <form action="\${BASE}/${modulename}/${entityclassname}/save" id="form_save" method="post" class="form form-horizontal">
    <input type="hidden" id="obj_id" name="id" value="\${obj.id}">
    <div class="col-xs-11">
      
	@for(attr in tableAttrs){
      <div class="row cl">
        <label class="form-label col-xs-3">${attr.comment}：</label>
        <div class="formControls col-xs-6">
          <input type="text" id="${attr.name}" name="${attr.name}" value="\${obj.${attr.name}}" class="input-text">
        </div>
        <div class="col-xs-3"> </div>
      </div>
	@}
	
      <div class="row cl">
        <label class="form-label col-xs-3">备注信息：</label>
        <div class="formControls col-xs-6">
          <textarea id="remarks" name="remarks" cols="" rows="" class="textarea"  placeholder="备注信息...255个字符以内" dragonfly="true" onKeyUp="textarealength(this,255)">\${obj.remarks}</textarea>
          <p class="textarea-numberbar"><em class="textarea-length">0</em>/255</p>
        </div>
        <div class="col-xs-3"> </div>
      </div>
      <div class="row cl">
        <div class="col-xs-10 col-xs-offset-2">
          <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;保存&nbsp;&nbsp;">
        </div>
      </div>
    </div>
    
    <div class="col-xs-1">&nbsp;</div>
  </form>
</div>

<script type="text/javascript">
$(function(){
	$("#form_save").validate({
		errorPlacement: function(error, element) {
			error.appendTo(element.parent());
		},
		submitHandler : function(form) {
			layer.load(1, {shade: [0.3,'#fff']});
			$(form).ajaxSubmit({
				dataType:"json",
				success:function( jsondata ){
					layer.closeAll('loading');
					if( jsondata.code == "001" ){
						layer.msg(jsondata.msg, {icon: 6,offset: '45px'});
						HtmlRender("\${BASE}/${modulename}/${entityclassname}/index");
					}else{
						layer.msg(jsondata.msg, {icon: 5,offset: '45px'});
					}
				}
			});
		},
		rules : {
			name : {
				required : true
			}
		}, 
		messages : { 
			name : { 
				required : "名称不能为空"
			}
		}
	});
});
</script>