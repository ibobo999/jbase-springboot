<div class="pd-10">
  <form class="form form-horizontal">
    <input type="hidden" id="obj_id" name="id" value="\${obj.id}">
    <div class="col-xs-11">
    
	@for(attr in tableAttrs){
      <div class="row cl">
        <label class="form-label col-xs-3">${attr.comment}：</label>
        <div class="formControls col-xs-6">\${obj.${attr.name}}</div>
        <div class="col-xs-3"> </div>
      </div>
	@}
	
      <div class="row cl">
        <label class="form-label col-xs-3">备注信息：</label>
        <div class="formControls col-xs-6">
          <textarea id="remarks" name="remarks" cols="" rows="" class="textarea"  placeholder="备注信息...255个字符以内" dragonfly="true" onKeyUp="textarealength(this,255)">\${obj.remarks}</textarea>
          <p class="textarea-numberbar"><em class="textarea-length">0</em>/255</p>
        </div>
        <div class="col-xs-3"> </div>
      </div>
    </div>
    <div class="col-xs-1">&nbsp;</div>
  </form>
</div>
