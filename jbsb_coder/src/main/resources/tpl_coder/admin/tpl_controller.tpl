/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.${modulename}.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jbase.common.utils.message.Message;
import org.jbase.common.utils.message.MessageCode;
import org.jbase.common.utils.Pager;
import org.jbase.common.utils.Requester;
import org.jbase.${modulename}.entity.${entityclassname};
import org.jbase.${modulename}.service.${entityclassname}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Project: jbase-springboot <br/>
 * File: ${entityclassname}Controller.java <br/>
 * Class: org.jbase.${modulename}.controller.${entityclassname}Controller <br/>
 * Description: ${tableDesc}. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
\@Controller("org.jbase.${modulename}.controller.${entityclassname}Controller")
\@RequestMapping("/${modulename}/${entityclassname}")
public class ${entityclassname}Controller {

	\@Autowired
	private ${entityclassname}Service ${loclassname}Service;

	\@RequestMapping("/index")
	public void index() {
	}

	\@RequestMapping("/add")
	public void add() {
	}

	\@RequestMapping("/edit")
	public void edit(HttpServletRequest request) {
		Message<?> mess = getUnique(request);
		request.setAttribute("obj", mess.getData());
	}

	\@RequestMapping("/show")
	public void show(HttpServletRequest request) {
		Message<?> mess = getUnique(request);
		request.setAttribute("obj", mess.getData());
	}

	\@ResponseBody
	\@RequestMapping(value = "/list", produces = "text/plain;charset=UTF-8")
	public String list(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = warp.getPager();
		${loclassname}Service.query(warp.getSearchKeys(), pager);
		return Message.jsonObj(pager);
	}

	\@ResponseBody
	\@RequestMapping(value = "/save", produces = "text/plain;charset=UTF-8")
	public String save(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Message<${entityclassname}> message = ${loclassname}Service.saveOrUpdate(warp.getParams());
		return Message.json(message);
	}

	\@ResponseBody
	\@RequestMapping(value = "/fetch", produces = "text/plain;charset=UTF-8")
	public String fetch(HttpServletRequest request) {
		Message<?> mess = getUnique(request);
		return Message.json(mess);
	}

	\@ResponseBody
	\@RequestMapping(value = "/delete", produces = "text/plain;charset=UTF-8")
	public String delete(HttpServletRequest request) {
		String ids = request.getParameter("ids");
		if (StringUtils.isBlank(ids)) {
			Message<?> mess = Message.fireFail().SetMessageCode(MessageCode.ENTITY_ID_IS_EMPTY);
			return Message.json(mess);
		}
		String[] idArray = ids.split(",");
		if (idArray.length > 1)
			return Message.json(${loclassname}Service.deleteByIds(ids));
		else
			return Message.json(${loclassname}Service.deleteById(ids));
	}

	/**
	 * 描述 : 如果id为空直接返回，否则返回实体对象. <br/>
	 * <p>
	 * 
	 * \@param request
	 */
	private Message<?> getUnique(HttpServletRequest request) {
		String id = request.getParameter("id");
		if (StringUtils.isBlank(id)) {
			return Message.fireFail().SetMessageCode(MessageCode.ENTITY_ID_IS_EMPTY);
		}
		Message<${entityclassname}> unique =${loclassname}Service.unique(id);
		return unique;

	}
}
