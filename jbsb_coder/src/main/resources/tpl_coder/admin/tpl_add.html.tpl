<% layout("/inc/layout_page.html"){ %>

<form action="\${BASE}/${modulename}/${entityclassname}/save" id="form_save" method="post">
<div class="box">
  <div class="box-header">
    <h4 class="box-title"><i class="fa fa-bars"></i> 添加${tableDesc}</h4>
    <div class="box-tools">
      <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> 保存</button>
      <a href="javascript:;" onclick="layer_close()" class="btn btn-info btn-sm"><i class="fa fa-chevron-left"></i> 返回</a>
    </div>
  </div>
  <div class="box-body">
    <table class="table tform table-bordered">
	@for(attr in tableAttrs){
      <tr>
        <th><label for="${attr.name}">${attr.comment}：</label></th>
        <td><input id="${attr.name}" name="${attr.name}" type="text" class="form-control"></td>
      </tr>
	@}

      <tr>
        <th><label for="remarks">备注信息：</label></th>
        <td>
        	<textarea id="remarks" name="remarks" cols="" rows="" class="form-control"  placeholder="备注信息...255个字符以内" dragonfly="true" onKeyUp="textarealength(this,255)"></textarea>
          <p class="textarea-numberbar"><em class="textarea-length">0</em>/255</p>
        </td>
      </tr>
    </table>
  </div>
  <div class="box-footer">
    <div class="box-tools pull-right">
      <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> 保存</button>
      <a href="javascript:;" onclick="layer_close()" class="btn btn-info btn-sm"><i class="fa fa-chevron-left"></i> 返回</a>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
$(function(){
	$("#form_save").validate({
		errorPlacement: function(error, element) {
			error.appendTo(element.parent());
		},
		submitHandler : function(form) {
			layer.load(1, {shade: [0.3,'#fff']});
			$(form).ajaxSubmit({
				dataType:"json",
				success:function( jsondata ){
					layer.closeAll('loading');
					if(jsondata.success){
						parent.toastr.success('保存成功！');
						parent.reload_table();
						layer_close();
					}else{
						parent.toastr.error(jsondata.msg);
					}
				}
			});
		},
		rules : {
			loginName : {
				required : true
			}
		}, 
		messages : { 
			name : { 
				required : "名称不能为空"
			}
		}
	});
});
</script>

<% } %>