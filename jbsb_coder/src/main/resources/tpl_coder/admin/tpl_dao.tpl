/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.${modulename}.dao;

import java.util.List;
import java.util.Map;

import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import org.jbase.${modulename}.entity.${entityclassname};

/**
 * Project: jbase-springboot <br/>
 * File: ${entityclassname}Dao.java <br/>
 * Class: org.jbase.${modulename}.sys.${entityclassname}Dao <br/>
 * Description: ${tableDesc}. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
public interface ${entityclassname}Dao extends BaseMapper<${entityclassname}> {

	public void selectPager(PageQuery pageQuery);

	\@Sql("select * from ${tablename} where sid=?")
	public ${entityclassname} fetchBySid(String sid);

	\@Sql("delete from ${tablename} where sid=?")
	public void deleteBySid(String sid);
	
	public List<${entityclassname}> listBySids(Map<String, Object> params);

}

