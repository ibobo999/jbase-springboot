/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.${modulename}.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import org.jbase.common.utils.message.Message;
import org.jbase.common.utils.message.MessageCode;
import org.jbase.common.service.EntityMapperService;
import org.jbase.common.utils.Pager;
import org.jbase.${modulename}.dao.${entityclassname}Dao;
import org.jbase.${modulename}.entity.${entityclassname};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Project: jbase-springboot <br/>
 * File: ${entityclassname}Service.java <br/>
 * Class: org.jbase.${modulename}.service.${entityclassname}Service <br/>
 * Description: ${tableDesc}. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
\@Service
public class ${entityclassname}Service extends EntityMapperService<${entityclassname}> {

	\@Autowired
	private ${entityclassname}Dao ${loclassname}Dao;

	\@Override
	public BaseMapper<${entityclassname}> dao() {
		return ${loclassname}Dao;
	}

	public Pager query(Map<String, String> map, Pager pager) {

		PageQuery pageQuery = new PageQuery(pager.getPageNumber(), map, -1, pager.getPageSize());
		${loclassname}Dao.selectPager(pageQuery);

		pager.setList(pageQuery.getList());
		pager.setRecordCount((int) pageQuery.getTotalRow());
		return pager;
	}

	public ${entityclassname} fetchBySid(String sid) {
		return ${loclassname}Dao.fetchBySid(sid);
	}

	public void deleteBySid(String sid) {
		${loclassname}Dao.deleteBySid(sid);
	}
	public Message<List<${entityclassname}>> listBySids(String sids) {
		Message<List<${entityclassname}>> mess = new Message<>();
		mess.SetMessageCode(MessageCode.SUCCESS);
		mess.setSuccess(true);

		if (StringUtils.isBlank(sids)) {
			return mess;
		}

		if (sids.startsWith(",")) {
			sids = sids.substring(1);
		}

		List<String>  sqltemp =  new ArrayList<>();
		String[] split = sids.split(",");
		for (String str : split) {
			if (StringUtils.isNotBlank(str))
				sqltemp.add(str);
		}

		Map<String,Object> param = new HashMap<>();
		param.put("sids", sqltemp);
		List<${entityclassname}> listBySids = ${loclassname}Dao.listBySids(param);

		mess.setData(listBySids);

		return mess;
	}
}
