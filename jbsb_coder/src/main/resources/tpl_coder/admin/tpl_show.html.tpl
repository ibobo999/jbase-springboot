<% layout("/inc/layout_page.html"){ %>
<div class="box box-solid">
  <div class="box-body">
    <table class="table tform table-bordered">
  @for(attr in tableAttrs){
      <tr>
        <th width="30%">${attr.comment}：</th>
        <td>\${obj.${attr.name}}</td>
      </tr>
  @}
      <tr>
        <th>备注信息：</th>
        <td>\${obj.remarks}</td>
      </tr>
      
    </table>
  </div>
  <div class="box-footer">
    <div class="box-tools pull-right">
      <a href="javascript:;" onclick="layer_close()" class="btn btn-info btn-sm"><i class="fa fa-close"></i> 关闭</a>
    </div>
  </div>
</div>
<script type="text/javascript">
</script>
<% } %>
