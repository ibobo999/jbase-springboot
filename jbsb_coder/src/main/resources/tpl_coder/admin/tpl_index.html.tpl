<% layout("/inc/layout_page.html"){ %>
<div class="box box-search box-solid">
  <div class="box-header">
    <i class="fa fa-ellipsis-v"></i>
    <h3 class="box-title">查询</h3>
    <div class="pull-right box-tools">
      <span onclick="reload_table()" class="btn btn-info btn-sm"><i class="fa fa-search"></i> 查询</span>
      <span onclick="form_search_reset()" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i> 重置</span>
      <button class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <div class="box-body">
    <form id="form_search">
      <input type="text" style="width:160px" placeholder="名称" id="_s_loginName" name="_s_name" />
    </form>
  </div>
</div>
<div class="box">
  <div class="box-header">
    <h4 class="box-title"><i class="fa fa-bars"></i> ${tableDesc}列表</h4>
    <div class="box-tools">
      <span onclick="list_add()" class="btn btn-info btn-sm"><i class="fa fa-plus-square"></i> 添加${tableDesc}</span>
    </div>
  </div>
  <div class="box-body no-padding">
    <table id="table_list" class="table table-border table-bordered table-bg table-striped table-hover">
      <thead>
        <tr class="text-c">
          @for(attr in tableAttrs){
          <th>${attr.comment}</th>
          @}
          <th width="170">操作</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
var _table;
function reload_table(){
	if(_table){
		_table.draw();
	}else{
		alert("没有表格");
	}
}

function list_add(){
	var index = layer.open({
		type: 2,
		closeBtn: 0,
		title: false,
		area: ['98%', '98%'],
		content: "\${BASE}/${modulename}/${entityclassname}/add"
	});
	layer.full(index);
}

function list_del(id){
	layer.confirm('确认删除所选记录?', {icon: 3, title:'警告'}, function(index){  
	    layer.close(index);
	    layer.load(1, {shade: [0.3,'#fff']});
	    $.post("\${BASE}/${modulename}/${entityclassname}/delete",{ids:id},function(jsondata){
			layer.closeAll('loading');
			if( jsondata.code == "001" ){
				toastr.success('删除成功！');
				reload_table();
			}else{
				toastr.error(jsondata.msg);
			}
	    },"json");
	});
}
function list_edit(id){
	var index = layer.open({
		type: 2,
		closeBtn: 0,
		title: false,
		area: ['98%', '98%'],
		content: "\${BASE}/${modulename}/${entityclassname}/edit?id="+id
	});
	layer.full(index);
}
function list_show(id){
    var index = layer.open({
		type: 2,
		closeBtn: 1,
		title: "${tableDesc}",
		area: ['98%', '98%'],
		content: "\${BASE}/${modulename}/${entityclassname}/show?id="+id
	});
}
function form_search_reset() {
	$("#form_search")[0].reset();
}

$(function(){
	_table = $("#table_list").DataTable({
        language: {
            "url": "/lib/datatables/Chinese.json"
        },
	    dom: 'rtlip',
		autoWidth: false,
		scrollX: true,
		ordering:false,
		searching:false,
		serverSide:true,
		ajax: {
		    url: "\${BASE}/${modulename}/${entityclassname}/list",
		    type: "POST",
		    data: function ( d ) {
		        d._s_name = $('#_s_name').val();
	        }
		},
        columns: [
            @for(attr in tableAttrs){
            { "data": "${attr.name}","defaultContent":"" },
            @}
            { data: "id",class:"text-c", "render": function(data, type, row) {
            	return '<a class="btn btn-primary btn-xs" href="javascript:;" title="编辑" onclick="list_edit(\''+data+'\')"><i class="Hui-iconfont">&#xe6df;</i> 编辑</a>&nbsp;'
            	  		+'<a class="btn btn-success btn-xs" href="javascript:;" title="查看" onclick="list_show(\''+data+'\')"><i class="Hui-iconfont">&#xe616;</i> 查看</a>&nbsp;'
            	  		+'<a class="btn btn-danger btn-xs" href="javascript:;" title="删除" onclick="list_del(\''+data+'\')"><i class="Hui-iconfont">&#xe6e2;</i> 删除</a>';
            }}
        ]
	});
})

</script>
<% } %>