/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.${modulename}.controller;

import java.math.*;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.jbase.${modulename}.entity.${entityclassname};
import org.jbase.${modulename}.service.${entityclassname}Service;
import org.jbase.common.utils.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Project:  <br/>
 * File: ${entityclassname}Controller.java <br/>
 * Class: org.jbase.admin.${modulename}.controller.${entityclassname}Controller <br/>
 * Description: ${tableDesc}的后台管理控制器类. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
\@Controller("org.jbase.admin.${modulename}.controller.${entityclassname}Controller")
\@RequestMapping("/admin/${modulename}/${controllername}")
public class ${entityclassname}Controller extends EntityController<${entityclass}> {

	\@Autowired
	private ${entityclassname}Service ${loclassname}Service;

	\@Override
	protected EntityService<Article> service() {
		return this.${loclassname}Service;
	}

}
