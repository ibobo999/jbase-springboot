<div id="tab-system" class="HuiTab">
	<div class="tabBar cl">
		<span class="current">${tableDesc}列表</span>
		<span onclick="HtmlRender('\${BASE}/${modulename}/${controllername}/add')">添加${tableDesc}</span>
	</div>
	<br>
</div>
<div>
  <form action="\${BASE}/${modulename}/${controllername}/list" id="form_pager_search" method="post">
    <input type="hidden" name="_p_pageNumber" value="\${pager.pageNumber}">
    <input type="hidden" name="_p_pageSize" value="\${pager.pageSize}">
	<input type="text" class="input-text" style="width:150px" placeholder="名称" id="_s_name" name="_s_name" value="\${searchKeys.name!}">
	<button type="button" class="btn btn-success" onclick="list_search()"><i class="Hui-iconfont">&#xe665;</i> 查找</button>
  </form>
</div>
<form action="\${BASE}/${modulename}/${controllername}/updateSort" id="form_updateSort" method="post">
<div class="cl pd-5 bg-1 bk-gray mt-20"> 
	<span class="l">
		<a href="javascript:;" onclick="list_del()" class="btn btn-danger size-S radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a>
		<a href="javascript:;" class="btn btn-primary size-S radius btn_submit"><i class="Hui-iconfont">&#xe6e2;</i> 保存排序</a>
	</span>
	<span class="r"><select class="select" onchange="list_changeSize(this.value)">
			<option value="10" \${pager.pageSize==10?"selected":""}>10</option>
			<option value="20" \${pager.pageSize==20?"selected":""}>20</option>
			<option value="50" \${pager.pageSize==50?"selected":""}>50</option>
			<option value="100" \${pager.pageSize==100?"selected":""}>100</option>
		</select>
	</span>
</div>
<div class="dataTables_wrapper no-footer">
<table class="table table-border table-bordered table-bg table-striped table-hover">
	<thead>
	<tr class="text-c"><th width="25"><input type="checkbox" name="" value=""></th>
	
	@for(attr in tableAttrs){
      <th>${attr.comment}</th>
	@}
	<th>描述</th><th width="110">操作</th></tr>
	</thead>
	<tbody>
	<% for(obj in pager.list){ %>
		<tr class="text-c"><td><input type="checkbox" value="\${obj.id}" name="table_checkbox_id"></td>
		    @for(attr in tableAttrs){
			<td>\${obj.${attr.name}}</td>
			@}
			<td class="td-manage">
				<a class="btn btn-secondary size-MINI" href="javascript:;" title="编辑" onclick="list_edit('\${obj.id}')"><i class="Hui-iconfont">&#xe6df;</i> 编辑</a> 
				<a class="btn btn-danger size-MINI" href="javascript:;" title="删除" onclick="list_del('\${obj.id}')"><i class="Hui-iconfont">&#xe6e2;</i> 删除</a>
			</td>
		</tr>
	<% } %></tbody>
</table>
<%include("/inc/pagging.html",{'pageNumber':pager.pageNumber,'pageCount':pager.pageCount,'recordCount':pager.recordCount}){}%>
</div>
</form>
<script type="text/javascript">
function list_del(id){
	var select_ids = "";
	if(id){
		select_ids=id;
	}else{
	    $("input[name='table_checkbox_id']:checked").each(function(){
	    	select_ids+=$(this).val()+",";   
	    });
	    if(""==select_ids){
	    	layer.msg('请先选择要删除的数据！', {icon: 3,offset: '45px'});
	    	return false;
	    }
	}
	layer.confirm('确认删除所选记录?', {icon: 3, title:'警告'}, function(index){	    
	    layer.close(index);
	    HtmlRender("\${BASE}/${modulename}/${controllername}/del",{ids:select_ids});
	});
}
function list_edit(id,state){
    HtmlRender("\${BASE}/${modulename}/${controllername}/edit",{id:id});
}
function list_search(){
	var $form = $("#form_pager_search");
	var url = $form.attr('action');
	var $input  = $("<input type='hidden' name='_sbtn' value='true' />");
	$form.append($input);
    HtmlRender(url,$form.serializeArray());
}
function list_changeSize(size){
	$("#form_pager_search > [name='_p_pageSize']").val(size);
	var $form = $("#form_pager_search");
	var url = $form.attr('action');
	var $input  = $("<input type='hidden' name='_sbtn' value='true' />");
	$form.append($input);
    HtmlRender(url,$form.serializeArray());
}
$(function(){
	table_checkbox_all();
	$("#form_updateSort").Validform({
		btnSubmit:".btn_submit",
		ajaxPost:true,
		postonce:true,
		callback:function(data){
			$.Hidemsg();
			if("200"==data.retCode){
				layer.msg('保存成功！', {icon: 6,offset: '45px'});
			}else{
				layer.msg(data.retMsg, {icon: 5,offset: '45px'});
			}
		}
	});
});
</script>