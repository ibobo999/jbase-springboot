/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.${modulename}.service;

import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import org.jbase.${modulename}.entity.${entityclassname};
import org.jbase.common.service.EntityService;
import org.springframework.stereotype.Service;


/**
 * Project:  <br/>
 * File: ${entityclassname}Service.java <br/>
 * Class: org.jbase.${modulename}.service.${entityclassname}Service <br/>
 * Description: ${tableDesc}的业务操作类. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
\@Service
public class ${entityclassname}Service extends EntityService<${entityclassname}> {


	\@Override
	protected Class<${entityclassname}> clzz() {
		return ${entityclassname}.class;
	}

}