/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.${modulename}.controller;

import java.math.*;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.jbase.${modulename}.entity.${entityclassname};
import org.jbase.${modulename}.service.${entityclassname}Service;
import org.jbase.common.utils.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Project:  <br/>
 * File: ${entityclassname}Controller.java <br/>
 * Class: org.jbase.${modulename}.controller.${entityclassname}Controller <br/>
 * Description: ${tableDesc}的后台管理控制器类. <br/>
 * Copyright: Copyright (c) 2016 <br/>
 * Company: http://git.oschina.net/liuzp315/Jbase <br/>
 * Makedate: ${date(),"yyyy-MM-dd hh:mm:ss"} <br/>
 * gen by Jbase Coder <br/>
 * 
 * \@author liuzhanhong
 * \@version 1.0
 * \@since 1.0
 */
\@Controller("org.jbase.${modulename}.controller.${entityclassname}Controller")
\@RequestMapping("/${modulename}/${controllername}")
public class ${entityclassname}Controller {

	\@Autowired
	private ${entityclassname}Service ${loclassname}Service;

	\@RequestMapping("/list")
	public void list(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = ${loclassname}Service.listWithPage(warp.getSearchKeys(), warp.getPager());
		request.setAttribute("pager", pager);
	}

	\@ResponseBody
	\@RequestMapping(value = "/listData", produces = "text/plain;charset=UTF-8")
	public String listData(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = ${loclassname}Service.listWithPage(warp.getSearchKeys(), warp.getPager());
		return JSON.toJSONString(pager);
	}

	\@RequestMapping("/add")
	public void add(HttpServletRequest request) {
	}

	\@ResponseBody
	\@RequestMapping(value = "/addSave", produces = "text/plain;charset=UTF-8")
	public String addSave(HttpServletRequest request) {
		${loclassname}Service.insertHolderId(Requester.warp(request).getParams());
		return ok();
	}

	\@RequestMapping("/del")
	public String del(\@RequestParam String ids) {
		${loclassname}Service.deleteByIds(ids);
		return "redirect:list";
	}

	\@RequestMapping("/edit")
	public void edit(\@RequestParam int id, HttpServletRequest request) {
		T t = ${loclassname}Service.fetchById(id);
		request.setAttribute("obj", t);
	}

	\@ResponseBody
	\@RequestMapping(value = "/editSave", produces = "text/plain;charset=UTF-8")
	public String editSave(HttpServletRequest request) {
		${loclassname}Service.update(Requester.warp(request).getParams());
		return ok();
	}

	private String ok() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "200");
		map.put("retMsg", "成功");
		map.put("retObj", "object");
		return JSON.toJSONString(map);
	}

	private String fail(String mesg) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "500");
		map.put("retMsg", mesg);
		return JSON.toJSONString(map);
	}
}
