<div id="tab-system" class="HuiTab">
	<div class="tabBar cl">
		<span onclick="HtmlRender('\${BASE}/admin/${modulename}/${controllername}/list')">${tableDesc}列表</span>
		<span onclick="HtmlRender('\${BASE}/admin/${modulename}/${controllername}/add')">添加${tableDesc}</span>
		<span class="current">修改${tableDesc}信息</span>
	</div>
	<br>
</div>
<div class="row cl">
  <form action="\${BASE}/admin/${modulename}/${controllername}/editSave" id="form_save" method="post" class="form form-horizontal">
  <input type="hidden" name="id" value="\${obj.id}">
    <div class="col-xs-11">
      
	@for(attr in tableAttrs){
      <div class="row cl">
        <label class="form-label col-xs-3">${attr.comment}：</label>
        <div class="formControls col-xs-6">
          <input type="text" id="${attr.name}" name="${attr.name}" class="input-text" value="\${obj.${attr.name}}">
        </div>
        <div class="col-xs-3"> </div>
      </div>
	@}
      <div class="row cl">
        <div class="col-xs-10 col-xs-offset-2">
          <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;保存&nbsp;&nbsp;">
        </div>
      </div>
    </div>
    
    <div class="col-xs-1">&nbsp;</div>
  </form>
</div>

<script type="text/javascript">
$(function(){
	$("#form_save").Validform({
		tiptype:2,
		ajaxPost:true,
		postonce:true,
		callback:function(data){
			$.Hidemsg();
			if("200"==data.retCode){
				layer.msg('保存成功！', {icon: 6,offset: '45px'});
				HtmlRender("\${BASE}/admin/${modulename}/${controllername}/list");
			}else{
				layer.msg('保存失败！', {icon: 5,offset: '45px'});
			}
		}
	});
});
</script>