package org.jbase.member.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.jbase.common.utils.Pager;
import org.jbase.member.entity.Member;
import org.jbase.member.entity.UserFeedBack;
import org.jbase.member.sevice.UserFeedBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;

@Controller("user_feed_back_controller")
@RequestMapping("/userFeedBack")
public class UserFeedBackController {
	@Autowired
	private UserFeedBackService userFeedBackService;
	
	@RequestMapping("/addSummary")
	public void addSummary(HttpServletRequest request){
		String sid=UUID.randomUUID().toString();
		request.setAttribute("sid",sid);
	}
	@RequestMapping("/userFeedBack_list")
	public void userFeedBack_list(HttpServletRequest request){
		
	}
	
	@RequestMapping("/submitSummary")
	@ResponseBody
	public String submitSummary(HttpServletRequest request){
		Member member = (Member) request.getSession().getAttribute("login_member");
		UserFeedBack ufb = new UserFeedBack();
		ufb.setSummary(request.getParameter("summary"));
		ufb.setContactEmail(request.getParameter("contactEmail"));
		ufb.setContactPhone(request.getParameter("contactPhone"));
		ufb.setSid(request.getParameter("sid"));
		
		ufb.setCreateBySid(member.getSid());
		userFeedBackService.insert(ufb);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "200");
		map.put("retMsg", "成功");
		map.put("retObj", "object");
		return JSON.toJSONString(map);
	}
	
	@RequestMapping("/userFeedBack_list_pages")
	public void getSummarys(HttpServletRequest request){
		Map<String,String> map=new HashMap<String,String>();
		String pageStart=request.getParameter("pageStart");
		String pageSize=request.getParameter("pageSize");
		int pt=1;
		int ps=5;
		if(!StringUtils.isEmpty(pageStart)){
			pt=Integer.valueOf(pageStart);
		}
		if(!StringUtils.isEmpty(pageSize)){
			ps=Integer.valueOf(pageSize);
		}
		Pager pager=new Pager(pt,ps);
		pager=userFeedBackService.userFeedBack_list(map, pager);
		
		request.setAttribute("pager",pager);
	}
}
