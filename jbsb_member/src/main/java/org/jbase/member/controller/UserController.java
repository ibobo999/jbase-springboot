/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.member.controller;

import java.io.File;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.jbase.GlobalConstants;
import org.jbase.common.service.AreaService;
import org.jbase.common.service.AreasService;
import org.jbase.common.service.CityService;
import org.jbase.common.service.ProvinceService;
import org.jbase.common.utils.Code;
import org.jbase.common.utils.Encodes;
import org.jbase.common.utils.MsgWs;
import org.jbase.member.entity.Member;
import org.jbase.member.sevice.FegitpassService;
import org.jbase.member.sevice.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSON;

/**
 * Project: fw_cms <br/>
 * File: IndexController.java <br/>
 * Class: com.yxt.cms.controller.IndexController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年11月27日 下午1:41:55 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("member_user_controller")
@RequestMapping("/user")
public class UserController {
	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private AreasService areasService;

	private static final Logger L = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private MemberService memberService;
	@Autowired
	private FegitpassService fegitpassService;
	@Autowired
	private AreaService areaservice;
	@Autowired
	private CityService cityservice;
	@Autowired
	private ProvinceService provinceservice;

	@RequestMapping("/yxyz")
	public void yxyz(HttpServletRequest request) {

	}

	@RequestMapping("/index")
	public void index(HttpServletRequest request) {

	}

	@RequestMapping("/register")
	public void register(HttpServletRequest request, String loginName) {
		request.setAttribute("loginName", loginName);
		String token = UUID.randomUUID().toString();
		request.setAttribute("token", token);
		request.getSession().setAttribute("token", token);
	}

	/**
	 * 向注册用户发送手机验证码
	 * 
	 * @param request
	 */
	@RequestMapping("/sendRegisterCode")
	@ResponseBody
	public Object sendRegisterCode(HttpServletRequest request) {
		String mobile = request.getParameter("mobile");
		String code = "";
		HttpSession session = request.getSession();
		for (int i = 0; i < 6; i++) {
			Random random = new Random();// 定义随机类
			int result = random.nextInt(10);// 返回[0,10)集合中的整数，注意不包括10
			int num = result; // +1后，[0,10)集合变为[1,11)集合，满足要求
			code = code + num + "";
		}
		session.setAttribute("code", code);
		code = code + "为您的宇运动注册验证码，请您在5分钟内完成注册操作，如非本人操作，请忽略。";
		String result = MsgWs.SendWaitWorkMsg(mobile, code);
		Map<String, String> resultMap = new HashMap<String, String>();
		if ("1".equals(result)) {
			resultMap.put("retCode", 200 + "");
		}

		session.setAttribute("mobile", mobile);

		Timestamp time = new Timestamp(System.currentTimeMillis());
		session.setAttribute("time", time);

		return JSON.toJSONString(resultMap);
	}

	@RequestMapping("/checkUserHased")
	@ResponseBody
	public String checkUserHased(HttpServletRequest request) {
		String loginName = request.getParameter("loginName");
		Member me = memberService.fetchByName(loginName);
		return me == null ? "true" : "false";
	}

	@RequestMapping("/checkUserNotHased")
	@ResponseBody
	public String checkUserNotHased(HttpServletRequest request) {
		String loginName = request.getParameter("loginName");
		Member me = memberService.fetchByName(loginName);
		return me == null ? "false" : "true";
	}

	@RequestMapping("/checkValidateCode")
	@ResponseBody
	public String checkValidateCode(HttpServletRequest request) {
		String validateCode = request.getParameter("validateCode");
		String code = (String) request.getSession().getAttribute("validateCode");
		if (StringUtils.isNotBlank(validateCode) && validateCode.toUpperCase().equals(code)) {
			return "true";
		} else {
			return "false";
		}
	}

	@RequestMapping(value = "/registerDo", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public Object registerDo(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String loginName = request.getParameter("loginName");
		Member me = memberService.fetchByName(loginName);
		String regionID = request.getParameter("regionID");
		String region = request.getParameter("region");
		String type = request.getParameter("type");
		Map<String, String> mapresult = new HashMap<String, String>();

		if (me == null) {
			if ("1".equals(type)) {
				Member member = new Member();
				member.setLoginName(loginName);
				member.setLoginPass(Encodes.md5(request.getParameter("loginPass")));
				member.setUserEmail(request.getParameter("userEmail"));
				member.setRegionID(regionID);
				member.setActive(region);
				memberService.insert(member);
				L.debug("用户注册成功：" + loginName);
				request.getSession().setAttribute("login_member", memberService.fetchByName(loginName));
				mapresult.put("retCode", "200");
				mapresult.put("retMsg", "注册成功！");
				mapresult.put("retUrl", "/index");
			} else {
				String mobile = (String) session.getAttribute("mobile");
				Timestamp time = (Timestamp) session.getAttribute("time");
				String code = (String) session.getAttribute("code");
				Timestamp time1 = new Timestamp(System.currentTimeMillis());

				if (!code.equals(request.getParameter("code"))) {
					mapresult.put("retCode", "100");
					mapresult.put("retMsg", "短信验证码填写错误！");

					mapresult.put("retUrl", "/user/register");
					return JSON.toJSONString(mapresult);
				}
				if ((time1.getTime() - time.getTime()) > 300000) {
					mapresult.put("retCode", "100");
					mapresult.put("retMsg", "短信验证码失效！");
					mapresult.put("retUrl", "/user/register");
					return JSON.toJSONString(mapresult);
				} else {
					Member member = new Member();
					member.setLoginName(mobile);
					member.setLoginPass(Encodes.md5(request.getParameter("loginPass")));
					member.setUserEmail(" ");
					member.setRegionID(regionID);
					member.setActive(region);
					member.setMobile(mobile);
					memberService.insert(member);
					mapresult.put("retCode", "200");
					mapresult.put("retMsg", "注册成功！");
					mapresult.put("retUrl", "/index");
					session.setAttribute("login_member", memberService.fetchByName(loginName));
				}
			}
		} else {
			mapresult.put("retCode", "100");
			mapresult.put("retMsg", "用户名重复！");
			mapresult.put("retUrl", "/user/register?loginName=" + loginName);
			return JSON.toJSONString(mapresult);
		}
		return JSON.toJSONString(mapresult);
	}

	@RequestMapping(value = "/LOGIN", method = RequestMethod.GET)
	public String login(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String loginName = request.getParameter("textfield");
		String loginPass = request.getParameter("textfield2");
		if (StringUtils.isBlank(loginPass) || StringUtils.isBlank(loginName)) {
			redirectAttributes.addFlashAttribute("error_message", "登录名、密码都必填！");
			return "redirect:/index";
		}

		Member member = memberService.fetchByName(loginName);
		if (null == member) {
			redirectAttributes.addFlashAttribute("error_message", "用户名不存在！");
			return "redirect:/index";
		}
		String md5 = Encodes.md5(loginPass);
		if (!(md5.equals(member.getLoginPass()))) {
			redirectAttributes.addFlashAttribute("error_message", "登录密码错误！");
			return "redirect:/index";
		}

		request.getSession().setAttribute("login_member", memberService.fetchByName(loginName));
		return "redirect:/index";

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public void loginDo(HttpServletRequest request) {
		String header = request.getHeader("referer");
		String header_url = header.substring(header.lastIndexOf("/"), header.length());
		String parameter = request.getParameter("referer");
		if (!"/login/register".contains(header_url)) {
			request.setAttribute("referer", header);
		} else {
			if (null != parameter) {
				request.setAttribute("referer", parameter);
			} else {
				request.setAttribute("referer", request.getSession().getAttribute("referer"));
			}
		}
	}

	@RequestMapping(value = "/loginmin", method = RequestMethod.GET)
	public void loginDomin(HttpServletRequest request) {
	}

	@RequestMapping(value = "/dologin", method = RequestMethod.POST)
	public String loginDo(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		L.debug("loginDo");
		String loginName = request.getParameter("loginName");
		String loginPass = request.getParameter("loginPass");

		String referer = request.getParameter("referer");

		if (StringUtils.isBlank(loginPass) || StringUtils.isBlank(loginName)) {
			redirectAttributes.addFlashAttribute("error_message", "登录名、密码都必填！");
			request.getSession().setAttribute("referer", referer);
			return "redirect:/user/login";
		}

		Member member = memberService.fetchByName(loginName);
		if (null == member) {
			redirectAttributes.addFlashAttribute("error_message", "用户名不存在！");
			request.getSession().setAttribute("referer", referer);
			return "forward:/user/login";
		}
		String md5 = Encodes.md5(loginPass);
		if (!(md5.equals(member.getLoginPass()))) {
			redirectAttributes.addFlashAttribute("error_message", "登录密码错误！");
			request.getSession().setAttribute("referer", referer);
			return "redirect:/user/login";
		}

		request.getSession().setAttribute("login_member", memberService.fetchByName(loginName));
		return "redirect:" + referer;
	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("login_member", null);
		session.removeAttribute("login_member");
		// session.invalidate();
		String referer = request.getHeader("referer");
		return "redirect:" + referer;
	}

	@RequestMapping("/fegitpass")
	public void fegitpass(HttpServletRequest request) {
		request.setAttribute("msg", "");
	}

	@RequestMapping("/checkUserMobile")
	@ResponseBody
	public String checkUserMobile(HttpServletRequest request) {
		String loginName = request.getParameter("mobile");
		Member me = memberService.fetchByMobile(loginName);
		return me == null ? "false" : "true";
	}

	/**
	 * 手机找回密码
	 * 
	 * @param request
	 */
	@RequestMapping("/sendzhRegisterCode")
	@ResponseBody
	public Object sendzhRegisterCode(HttpServletRequest request) {
		String mobile = request.getParameter("mobile");

		String code = "";
		HttpSession session = request.getSession();
		for (int i = 0; i < 6; i++) {
			Random random = new Random();// 定义随机类
			int result = random.nextInt(10);// 返回[0,10)集合中的整数，注意不包括10
			int num = result; // +1后，[0,10)集合变为[1,11)集合，满足要求
			code = code + num + "";
		}
		session.setAttribute("code", code);
		code = "您在宇运动请求的验证码是 " + code;
		String resultMsg = MsgWs.SendWaitWorkMsg(mobile, code);

		Member m = memberService.fetchByMobile(mobile);
		Map<String, String> result = new HashMap<String, String>();
		if (m != null && m.getUserEmail() != null) {

			Map<String, Object> mapParams = new HashMap<String, Object>();
			mapParams.put("userCode", session.getAttribute("code"));
			mapParams.put("sid", m.getSid());
			mapParams.put("limitTime", System.currentTimeMillis());
			memberService.updateBySid(m.getSid(), mapParams);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", m.getId());
			map.put("userCode", code);
			Date date = new Date();
			long invalidTime = date.getTime() + 5 * 60 * 1000;
			map.put("invalidTime", invalidTime);

			result.put("retCode", "200");
		} else if (m == null) {
			result.put("retCode", "500");
		} else {
			result.put("retCode", "400");
		}
		return JSON.toJSONString(result);
	}

	/**
	 * 手机找回密码
	 * 
	 * @param request
	 */
	@RequestMapping("/sjyz")
	public void sjyz(HttpServletRequest request) {

	}

	@RequestMapping(value = "/updatepass", method = RequestMethod.POST)
	@ResponseBody
	public Object updatepass(HttpServletRequest request) {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

		String username = request.getParameter("loginName");
		String email = request.getParameter("email");
		Member m = memberService.fetchByName(username);
		Map<String, String> result = new HashMap<String, String>();
		if (m != null && m.getUserEmail() != null && m.getUserEmail().equals(email)) {
			simpleMailMessage.setFrom("djz236@163.com");
			simpleMailMessage.setTo(email);
			simpleMailMessage.setSubject("乒娱网密码重置");
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String str = sdf.format(date);
			String uuid = Code.suiji.toString();
			String md5 = Encodes.md5(uuid + str);
			simpleMailMessage.setText("请点击下面的链接，按流程进行密码重设:  http://www.ourpyw.com/user/setpass_sj?loginName="
					+ m.getLoginName() + "&usercode=" + md5);

			/*
			 * simpleMailMessage.setText(
			 * "请点击下面的链接，按流程进行密码重设:  http://localhost:8080/yxt_ourpyw/user/setpass_sj?loginName=" + m.getLoginName()+
			 * "&usercode=" + md5);
			 */ mailSender.send(simpleMailMessage);
			Map<String, Object> mapParams = new HashMap<String, Object>();
			mapParams.put("userCode", md5);
			mapParams.put("sid", m.getSid());
			mapParams.put("limitTime", System.currentTimeMillis());
			memberService.updateBySid(m.getSid(), mapParams);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", m.getId());
			map.put("userCode", Encodes.md5(Code.suiji + str));

			long invalidTime = date.getTime() + 15 * 60 * 1000;
			map.put("invalidTime", invalidTime);

			// fegitpassService.insertHolderId(map);

			result.put("success", "200");
		} else if (m == null) {
			result.put("success", "500");
		} else {
			result.put("success", "400");
		}
		return JSON.toJSONString(result);// "redirect:/index";
	}

	@RequestMapping("/setpass_sj")
	public String setpass_sj(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("loginName", request.getParameter("loginName"));
		map.put("userEmail", request.getParameter("userEmail"));
		map.put("userCode", request.getParameter("usercode"));
		request.setAttribute("column_id", "");
		request.setAttribute("columnId", "");

		request.setAttribute("loginName", request.getParameter("loginName"));
		request.setAttribute("userCode", request.getParameter("usercode"));
		/*
		 * Fegitpass fe = fegitpassService.fetchByCol(map); int feid = fe.getId(); request.setAttribute("feid", feid);
		 */
		Member me = memberService.fetchByName(request.getParameter("loginName"));
		request.setAttribute("me", me);
		Date date = new Date();
		String str = me.getLimitTime();
		if ((System.currentTimeMillis() - Long.valueOf(str)) > 300000) {
			return "/user/setpass_pass";
		}
		// if (date.getTime() < fe.getInvalidTime() && fe.getState() == 1) {
		return "/user/setpass_sj";
		/*
		 * } else { return null; }
		 */
	}

	@RequestMapping("/setpass_sjyz")
	public String setpass_sjyz(HttpServletRequest request) {
		/*
		 * Map<String, Object> map = new HashMap<String, Object>(); map.put("loginName",
		 * request.getParameter("loginName")); map.put("userEmail", request.getParameter("userEmail"));
		 * map.put("userCode", request.getParameter("usercode")); request.setAttribute("column_id", "");
		 * request.setAttribute("columnId", "");
		 * 
		 * request.setAttribute("loginName", request.getParameter("loginName")); request.setAttribute("userCode",
		 * request.getParameter("usercode")); Fegitpass fe = fegitpassService.fetchByCol(map); int feid = fe.getId();
		 * request.setAttribute("feid", feid); Member me = memberService.fetchByName(request.getParameter("loginName"));
		 * request.setAttribute("me", me); Date date = new Date(); String str=me.getLimitTime();
		 * if((System.currentTimeMillis()-Long.valueOf(str))>300000){ return "/user/setpass_pass"; }
		 */
		// if (date.getTime() < fe.getInvalidTime() && fe.getState() == 1) {

		request.setAttribute("loginName", request.getParameter("loginName"));
		request.setAttribute("userCode", request.getParameter("code"));
		return "/user/setpass_sj";
		/*
		 * } else { return null; }
		 */
	}

	@RequestMapping("/dosetpass")
	public String dosetpass(HttpServletRequest request) {

		Map<String, Object> map = new HashMap<String, Object>();
		String password = request.getParameter("loginPass");
		String usercode = request.getParameter("userCode");
		map.put("loginPass", Encodes.md5(password));
		map.put("userCode", usercode);
		memberService.updatePasswordByUserCode(map);
		/*
		 * Map<String, Object> map1 = new HashMap<String, Object>(); map1.put("state", 0); map1.put("id",
		 * request.getParameter("feid")); fegitpassService.update(map1);
		 */
		return "redirect:/index";
	}

	@RequestMapping("/updateuser")
	public void updateuser(HttpServletRequest request) {
		Member m = (Member) request.getSession().getAttribute("login_member");
		int id = m.getId();
		Member me = memberService.fetchById(id);
		request.setAttribute("member", me);
		int updateuser = 1;
		request.setAttribute("updateuser", updateuser);
	}

	@RequestMapping("/updatetouxiang")
	public void updatetouxiang(HttpServletRequest request) {
		Member m = (Member) request.getSession().getAttribute("login_member");
		int id = m.getId();
		Member me = memberService.fetchById(id);
		request.setAttribute("member", me);
		int updateuser = 1;
		request.setAttribute("updateuser", updateuser);
	}

	@RequestMapping("/doupdatetouxiang")
	public String doupdatetouxiang(HttpServletRequest request) {
		Member m = (Member) request.getSession().getAttribute("login_member");
		int id = m.getId();
		String touxiang = request.getParameter("logo");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("oauthHeadImg", touxiang);
		memberService.update(map);
		return "redirect:/user/updatetouxiang";

	}

	@RequestMapping("/doupdateuser")
	public String doupdateuser(HttpServletRequest request) throws ParseException {
		Member m = (Member) request.getSession().getAttribute("login_member");
		int id = m.getId();
		String userNicename = request.getParameter("userNicename").toString();
		String userEmail = request.getParameter("userEmail");
		String phone = request.getParameter("phone");
		String signature = request.getParameter("signature");
		String sex = request.getParameter("sex");
		int Technicalplay = Integer.parseInt(request.getParameter("Technicalplay"));
		String Technicalplays = request.getParameter("Technicalplays");
		int equipment1 = Integer.parseInt(request.getParameter("equipment1"));
		String equipment1s = request.getParameter("equipment1s");
		int equipment2 = Integer.parseInt(request.getParameter("equipment2"));
		String equipment2s = request.getParameter("equipment2s");
		int equipment3 = Integer.parseInt(request.getParameter("equipment3"));
		String equipment3s = request.getParameter("equipment3s");
		int sex1 = Integer.parseInt(sex);
		String birthday = request.getParameter("birthday");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date birthday1 = df.parse(birthday);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("userNicename", userNicename);
		map.put("userEmail", userEmail);
		map.put("phone", phone);
		map.put("signature", signature);
		map.put("sex", sex1);
		map.put("birthday", birthday1);
		map.put("Technicalplay", Technicalplay);
		map.put("Technicalplays", Technicalplays);
		map.put("equipment1", equipment1);
		map.put("equipment1s", equipment1s);
		map.put("equipment2", equipment2);
		map.put("equipment2s", equipment2s);
		map.put("equipment3", equipment3);
		map.put("equipment3s", equipment3s);
		memberService.update1(map);
		return "redirect:/user/updateuser";
	}

	@RequestMapping("/updatepassword")
	public void updatepassword(HttpServletRequest request) {
		request.setAttribute("err", request.getParameter("err"));
		Member m = (Member) request.getSession().getAttribute("login_member");
		int id = m.getId();
		Member me = memberService.fetchById(id);
		request.setAttribute("member", me);
		request.setAttribute("updatepassword", 1);
	}

	@RequestMapping("/doupdatepassword")
	public String doupdatepassword(HttpServletRequest request) {
		Member m = (Member) request.getSession().getAttribute("login_member");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		if (Encodes.md5(password).equals(m.getLoginPass())) {
			if (password1.equals(password2)) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("loginPass", Encodes.md5(password1));
				map.put("id", m.getId());
				memberService.update1(map);
				return "redirect:/user/updateuser";

			} else {
				String err = "新两次密码输入不一致";
				request.setAttribute("err", err);
				return "redirect:/user/updatepassword";
			}
		} else {
			String err = "旧密码输入错误，请重新输入！";
			request.setAttribute("err", err);
			return "redirect:/user/updatepassword";

		}
	}

	/**
	 * 跳转到个人信息首页
	 * 
	 * @param request
	 */
	@RequestMapping("/userinfo")
	public void userinfo(HttpServletRequest request) {
		Member m = (Member) request.getSession().getAttribute("login_member");
		int id = m.getId();
		Member me = memberService.fetchById(id);
		request.setAttribute("member", me);
		int updateuser = 1;
		request.setAttribute("updateuser", updateuser);
	}

	/**
	 * 注册协议
	 * 
	 * @param request
	 */
	@RequestMapping("/register_xieyi")
	public void register(HttpServletRequest request) {

	}

	/**
	 * 获取用户头像集合
	 * 
	 * @param request
	 */
	@RequestMapping("/getUserHeadImg")
	public void getUserHeadImg(HttpServletRequest request) {
		String path = GlobalConstants.image_upload_path + "/logos/member";
		File file = new File(path);
		File[] tempList = file.listFiles();
		List<String> mlist = new ArrayList<String>();// 男
		List<String> wlist = new ArrayList<String>();// 女
		List<String> zlist = new ArrayList<String>();// 中性
		for (int i = 0; i < tempList.length; i++) {
			if (tempList[i].isFile()) {
				String string = tempList[i].toString();
				String[] strArr = string.split("upload");
				if (strArr[1].contains("_m_")) {
					mlist.add("upload" + strArr[1]);
				}
				if (strArr[1].contains("_w_")) {
					wlist.add("upload" + strArr[1]);
				}
				if (strArr[1].contains("_z_")) {
					zlist.add("upload" + strArr[1]);
				}
			}
			if (tempList[i].isDirectory()) {
			}
		}
		request.setAttribute("mlist", mlist);
		request.setAttribute("wlist", wlist);
		request.setAttribute("zlist", zlist);
		request.setAttribute("id", request.getParameter("id"));
	}

	/**
	 * 设置用户头像
	 * 
	 * @param request
	 */
	@RequestMapping("/setlogo")
	@ResponseBody
	public String setlogo(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", request.getParameter("userid"));
		String parameter = request.getParameter("logoUrl").replace("upload\\", "");
		map.put("oauthHeadImg", parameter);
		int i = memberService.update(map);
		Map<String, String> result = new HashMap<String, String>();
		if (i > 0) {
			result.put("success", "200");
		}
		return JSON.toJSONString(result);
	}
}
