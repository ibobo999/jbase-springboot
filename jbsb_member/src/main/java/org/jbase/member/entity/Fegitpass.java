package org.jbase.member.entity;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

@Table(name = "cms_fegitpass")
public class Fegitpass extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4316110420931907709L;
	private long invalidTime;
	private String loginName;
	private String userEmail;
	private String userCode;

	public long getInvalidTime() {
		return invalidTime;
	}

	public void setInvalidTime(long invalidTime) {
		this.invalidTime = invalidTime;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
}
