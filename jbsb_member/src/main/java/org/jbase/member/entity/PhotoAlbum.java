package org.jbase.member.entity;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

@Table(name = "user_photo_album")
public class PhotoAlbum extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8035445933048743285L;
	private int clubid;
	private int gameid;
	private String albumurl;

	public String getAlbumurl() {
		return albumurl;
	}

	public void setAlbumurl(String albumurl) {
		this.albumurl = albumurl;
	}

	public int getClubid() {
		return clubid;
	}

	public int getGameid() {
		return gameid;
	}

	public void setGameid(int gameid) {
		this.gameid = gameid;
	}

	public void setClubid(int clubid) {
		this.clubid = clubid;
	}

	private int createId;

	public int getCreateId() {
		return createId;
	}

	public void setCreateId(int createId) {
		this.createId = createId;
	}

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
