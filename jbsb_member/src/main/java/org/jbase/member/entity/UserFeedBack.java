package org.jbase.member.entity;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * 用户反馈信息
 * 
 * @author Administrator
 *
 */
@Table(name = "user_feedback")
public class UserFeedBack extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4870774708757291753L;
	/**
	 * 反馈信息内容
	 */
	private String summary;
	/**
	 * 电话联系方式
	 */
	private String contactPhone;
	/**
	 * 邮箱联系方式
	 */
	private String contactEmail;

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

}
