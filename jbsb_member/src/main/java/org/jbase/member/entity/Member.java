/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.member.entity;

import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Project: fw_common <br/>
 * File: UserMember.java <br/>
 * Class: com.yxt.user.entity.UserMember <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年1月7日 下午4:57:26 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "user_member")
public class Member extends BaseEntity {

	private static final long serialVersionUID = 3305509745144601944L;

	/**
	 * 重置密码的时间限制
	 */
	private String limitTime;
	/**
	 * 找回密码时的标示
	 */
	private String userCode;

	public String getLimitTime() {
		return limitTime;
	}

	public void setLimitTime(String limitTime) {
		this.limitTime = limitTime;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	/**
	 * @return the companyId
	 */
	public int getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId
	 *            the companyId to set
	 */
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the companySid
	 */
	public String getCompanySid() {
		return companySid;
	}

	/**
	 * @param companySid
	 *            the companySid to set
	 */
	public void setCompanySid(String companySid) {
		this.companySid = companySid;
	}

	/**
	 * @return the officeId
	 */
	public int getOfficeId() {
		return officeId;
	}

	/**
	 * @param officeId
	 *            the officeId to set
	 */
	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}

	/**
	 * @return the officeSid
	 */
	public String getOfficeSid() {
		return officeSid;
	}

	/**
	 * @param officeSid
	 *            the officeSid to set
	 */
	public void setOfficeSid(String officeSid) {
		this.officeSid = officeSid;
	}

	/**
	 * @return the empNo
	 */
	public String getEmpNo() {
		return empNo;
	}

	/**
	 * @param empNo
	 *            the empNo to set
	 */
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	/**
	 * @return the realName
	 */
	public String getRealName() {
		return realName;
	}

	/**
	 * @param realName
	 *            the realName to set
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @param loginName
	 *            the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @return the loginPass
	 */
	public String getLoginPass() {
		return loginPass;
	}

	/**
	 * @param loginPass
	 *            the loginPass to set
	 */
	public void setLoginPass(String loginPass) {
		this.loginPass = loginPass;
	}

	/**
	 * @return the userNicename
	 */
	public String getUserNicename() {
		return userNicename;
	}

	/**
	 * @param userNicename
	 *            the userNicename to set
	 */
	public void setUserNicename(String userNicename) {
		this.userNicename = userNicename;
	}

	/**
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * @param userEmail
	 *            the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/**
	 * @return the userUrl
	 */
	public String getUserUrl() {
		return userUrl;
	}

	/**
	 * @param userUrl
	 *            the userUrl to set
	 */
	public void setUserUrl(String userUrl) {
		this.userUrl = userUrl;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the userType
	 */
	public int getUserType() {
		return userType;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(int userType) {
		this.userType = userType;
	}

	/**
	 * @return the photo
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * @param photo
	 *            the photo to set
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**
	 * @return the sex
	 */
	public int getSex() {
		return sex;
	}

	/**
	 * @param sex
	 *            the sex to set
	 */
	public void setSex(int sex) {
		this.sex = sex;
	}

	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday
	 *            the birthday to set
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * @return the signature
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * @param signature
	 *            the signature to set
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * @return the lastLoginIp
	 */
	public String getLastLoginIp() {
		return lastLoginIp;
	}

	/**
	 * @param lastLoginIp
	 *            the lastLoginIp to set
	 */
	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	/**
	 * @return the lastLoginTime
	 */
	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	/**
	 * @param lastLoginTime
	 *            the lastLoginTime to set
	 */
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	/**
	 * @return the loginTimes
	 */
	public int getLoginTimes() {
		return loginTimes;
	}

	/**
	 * @param loginTimes
	 *            the loginTimes to set
	 */
	public void setLoginTimes(int loginTimes) {
		this.loginTimes = loginTimes;
	}

	/**
	 * @return the userActivationKey
	 */
	public String getUserActivationKey() {
		return userActivationKey;
	}

	/**
	 * @param userActivationKey
	 *            the userActivationKey to set
	 */
	public void setUserActivationKey(String userActivationKey) {
		this.userActivationKey = userActivationKey;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score
	 *            the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the userTypeId
	 */
	public int getUserTypeId() {
		return userTypeId;
	}

	/**
	 * @param userTypeId
	 *            the userTypeId to set
	 */
	public void setUserTypeId(int userTypeId) {
		this.userTypeId = userTypeId;
	}

	/**
	 * @return the registerTypeId
	 */
	public int getRegisterTypeId() {
		return registerTypeId;
	}

	/**
	 * @param registerTypeId
	 *            the registerTypeId to set
	 */
	public void setRegisterTypeId(int registerTypeId) {
		this.registerTypeId = registerTypeId;
	}

	/**
	 * @return the levelTypeId
	 */
	public int getLevelTypeId() {
		return levelTypeId;
	}

	/**
	 * @param levelTypeId
	 *            the levelTypeId to set
	 */
	public void setLevelTypeId(int levelTypeId) {
		this.levelTypeId = levelTypeId;
	}

	/**
	 * @return the scoreTypeId
	 */
	public int getScoreTypeId() {
		return scoreTypeId;
	}

	/**
	 * @param scoreTypeId
	 *            the scoreTypeId to set
	 */
	public void setScoreTypeId(int scoreTypeId) {
		this.scoreTypeId = scoreTypeId;
	}

	/**
	 * @return the oauthFromKey
	 */
	public String getOauthFromKey() {
		return oauthFromKey;
	}

	/**
	 * @param oauthFromKey
	 *            the oauthFromKey to set
	 */
	public void setOauthFromKey(String oauthFromKey) {
		this.oauthFromKey = oauthFromKey;
	}

	/**
	 * @return the oauthName
	 */
	public String getOauthName() {
		return oauthName;
	}

	/**
	 * @param oauthName
	 *            the oauthName to set
	 */
	public void setOauthName(String oauthName) {
		this.oauthName = oauthName;
	}

	/**
	 * @return the oauthHeadImg
	 */
	public String getOauthHeadImg() {
		return oauthHeadImg;
	}

	/**
	 * @param oauthHeadImg
	 *            the oauthHeadImg to set
	 */
	public void setOauthHeadImg(String oauthHeadImg) {
		this.oauthHeadImg = oauthHeadImg;
	}

	/**
	 * @return the oauthBindTime
	 */
	public Date getOauthBindTime() {
		return oauthBindTime;
	}

	/**
	 * @param oauthBindTime
	 *            the oauthBindTime to set
	 */
	public void setOauthBindTime(Date oauthBindTime) {
		this.oauthBindTime = oauthBindTime;
	}

	/**
	 * @return the oauthStatus
	 */
	public int getOauthStatus() {
		return oauthStatus;
	}

	/**
	 * @param oauthStatus
	 *            the oauthStatus to set
	 */
	public void setOauthStatus(int oauthStatus) {
		this.oauthStatus = oauthStatus;
	}

	/**
	 * @return the oauthAccessToken
	 */
	public String getOauthAccessToken() {
		return oauthAccessToken;
	}

	/**
	 * @param oauthAccessToken
	 *            the oauthAccessToken to set
	 */
	public void setOauthAccessToken(String oauthAccessToken) {
		this.oauthAccessToken = oauthAccessToken;
	}

	/**
	 * @return the oauthExpiresDate
	 */
	public long getOauthExpiresDate() {
		return oauthExpiresDate;
	}

	/**
	 * @param oauthExpiresDate
	 *            the oauthExpiresDate to set
	 */
	public void setOauthExpiresDate(long oauthExpiresDate) {
		this.oauthExpiresDate = oauthExpiresDate;
	}

	/**
	 * @return the oauthOpenid
	 */
	public String getOauthOpenid() {
		return oauthOpenid;
	}

	/**
	 * @param oauthOpenid
	 *            the oauthOpenid to set
	 */
	public void setOauthOpenid(String oauthOpenid) {
		this.oauthOpenid = oauthOpenid;
	}

	private int companyId;
	private String companySid;;
	private int officeId;
	private String officeSid;
	private String empNo;
	private String realName;
	private String loginName;
	private String loginPass;
	private String userNicename;
	private String userEmail;
	private String userUrl;
	private String phone;
	private String mobile;
	private int userType;
	private String photo;
	private int sex = 0;
	private Date birthday;
	private String signature;
	private String lastLoginIp;
	private Date lastLoginTime;
	private int loginTimes;
	private String userActivationKey;
	private int score;
	private int level;
	private int userTypeId;
	private int registerTypeId;
	private int levelTypeId;
	private int scoreTypeId;
	private String oauthFromKey;
	private String oauthName;
	private String oauthHeadImg;
	private Date oauthBindTime;
	private int oauthStatus;
	private String oauthAccessToken;
	private long oauthExpiresDate;
	private String oauthOpenid;
	private String active;
	private String regionID;

	public String getRegionID() {
		return regionID;
	}

	public void setRegionID(String regionID) {
		this.regionID = regionID;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

}
