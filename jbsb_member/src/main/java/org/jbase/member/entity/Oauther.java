/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.member.entity;

import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Project: fw_common <br/>
 * File: UserMember.java <br/>
 * Class: com.yxt.user.entity.UserMember <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年1月7日 下午4:57:26 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "user_oauther")
public class Oauther extends BaseEntity {

	private static final long serialVersionUID = -537777858427304034L;

	/**
	 * @return the memberId
	 */
	public int getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId
	 *            the memberId to set
	 */
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	/**
	 * @return the fromKey
	 */
	public String getFromKey() {
		return fromKey;
	}

	/**
	 * @param fromKey
	 *            the fromKey to set
	 */
	public void setFromKey(String fromKey) {
		this.fromKey = fromKey;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the headImg
	 */
	public String getHeadImg() {
		return headImg;
	}

	/**
	 * @param headImg
	 *            the headImg to set
	 */
	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	/**
	 * @return the bindTime
	 */
	public Date getBindTime() {
		return bindTime;
	}

	/**
	 * @param bindTime
	 *            the bindTime to set
	 */
	public void setBindTime(Date bindTime) {
		this.bindTime = bindTime;
	}

	/**
	 * @return the firstLoginTime
	 */
	public Date getFirstLoginTime() {
		return firstLoginTime;
	}

	/**
	 * @param firstLoginTime
	 *            the firstLoginTime to set
	 */
	public void setFirstLoginTime(Date firstLoginTime) {
		this.firstLoginTime = firstLoginTime;
	}

	/**
	 * @return the lastLoginTime
	 */
	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	/**
	 * @param lastLoginTime
	 *            the lastLoginTime to set
	 */
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	/**
	 * @return the lastLoginIp
	 */
	public String getLastLoginIp() {
		return lastLoginIp;
	}

	/**
	 * @param lastLoginIp
	 *            the lastLoginIp to set
	 */
	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	/**
	 * @return the loginTimes
	 */
	public int getLoginTimes() {
		return loginTimes;
	}

	/**
	 * @param loginTimes
	 *            the loginTimes to set
	 */
	public void setLoginTimes(int loginTimes) {
		this.loginTimes = loginTimes;
	}

	/**
	 * @return the accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * @param accessToken
	 *            the accessToken to set
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the expiresDate
	 */
	public long getExpiresDate() {
		return expiresDate;
	}

	/**
	 * @param expiresDate
	 *            the expiresDate to set
	 */
	public void setExpiresDate(long expiresDate) {
		this.expiresDate = expiresDate;
	}

	/**
	 * @return the openid
	 */
	public String getOpenid() {
		return openid;
	}

	/**
	 * @param openid
	 *            the openid to set
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	private int memberId;
	private String fromKey;
	private String name;
	private String headImg;
	private Date bindTime;
	private Date firstLoginTime;
	private Date lastLoginTime;
	private String lastLoginIp;
	private int loginTimes = 0;
	private String accessToken;
	private long expiresDate;
	private String openid;

}
