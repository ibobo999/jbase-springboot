package org.jbase.member.entity;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

@Table(name = "user_photos")
public class Photos extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1211856536335613654L;
	private int createId;
	private String albumId;
	private String keywords;
	private String imageUrl;
	private int gameid;

	public int getGameid() {
		return gameid;
	}

	public void setGameid(int gameid) {
		this.gameid = gameid;
	}

	public int getCreateId() {
		return createId;
	}

	public void setCreateId(int createId) {
		this.createId = createId;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getAlbumId() {
		return albumId;
	}

	public void setAlbumId(String albumId) {
		this.albumId = albumId;
	}

}
