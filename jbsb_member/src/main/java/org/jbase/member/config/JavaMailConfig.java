/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.member.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * Project: jbsb_member <br/>
 * File: JavaMailConfig.java <br/>
 * Class: org.jbase.member.config.JavaMailConfig <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年8月22日 下午4:14:10 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Configuration
public class JavaMailConfig {
	@Bean(name = "javaMailSender")
	public JavaMailSender getJavaMailSender() {
		JavaMailSenderImpl jms = new JavaMailSenderImpl();
		jms.setHost("host");
		jms.setPort(465);
		jms.setProtocol("smtps");
		jms.setUsername("username");
		jms.setPassword("password");
		jms.setDefaultEncoding("UTF8");

		Properties pros = new Properties();
		pros.setProperty("mail.smtps.auth", "true");
		pros.setProperty("mail.smtp.ssl.enable", "true");
		pros.setProperty("mail.transport.protocol", "smtps");
		jms.setJavaMailProperties(pros);

		return jms;
	}
}
