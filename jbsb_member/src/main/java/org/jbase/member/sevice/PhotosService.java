package org.jbase.member.sevice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.common.service.EntityService;
import org.jbase.common.utils.Pager;
import org.jbase.member.entity.Photos;
import org.springframework.stereotype.Service;

@Service
public class PhotosService extends EntityService<Photos> {

	@Override
	protected Class<Photos> clzz() {
		// TODO Auto-generated method stub
		return Photos.class;
	}

	public Pager findAllPhotos(int start, int size) {
		// TODO Auto-generated method stub
		Pager pager = new Pager(start, size);
		Map<String, Object> map = new HashMap<String, Object>();
		Integer recordCount = sql.intValue("photos.pageByALL_count", map);
		pager.setRecordCount(recordCount);
		List<Photos> list = sql.select("photos.pageByAll_page", Photos.class, map, pager.getStart(),
				pager.getPageSize());
		pager.setList(list);
		return pager;
	}

	public void deletebyalId(int id) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("albumId", id);
		int executeUpdate = sql.update("photos.deletebyalId", map);
	}

	public List<Photos> findByColpage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		int page = (Integer) map.get("page");
		int start = 0 + (page - 1) * 20;
		map.put("start", start);
		List<Photos> list = sql.select("photos.findByColpage", Photos.class, map);
		return list;
	}

}
