package org.jbase.member.sevice;

import org.jbase.common.service.EntityService;
import org.jbase.member.entity.PhotoAlbum;
import org.springframework.stereotype.Service;

@Service
public class PhotoAlbumService extends EntityService<PhotoAlbum> {

	@Override
	protected Class<PhotoAlbum> clzz() {
		// TODO Auto-generated method stub
		return PhotoAlbum.class;
	}

}
