package org.jbase.member.sevice;

import java.util.List;
import java.util.Map;

import org.jbase.common.service.EntityService;
import org.jbase.common.utils.Pager;
import org.jbase.member.entity.UserFeedBack;
import org.springframework.stereotype.Service;

@Service
public class UserFeedBackService extends EntityService<UserFeedBack> {

	@Override
	protected Class<UserFeedBack> clzz() {
		return UserFeedBack.class;
	}

	public Pager userFeedBack_list(Map<String, String> map, Pager pager) {

		int size = sql.intValue("userFeedBack.getsummarysCount", null);
		List<UserFeedBack> list = sql.select("userFeedBack.getsummarys", UserFeedBack.class, null, pager.getStart(),
				pager.getPageSize());

		pager.setRecordCount(size);
		pager.setList(list);
		return pager;
	}

}
