/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.member.sevice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.common.service.EntityService;
import org.jbase.member.entity.Member;
import org.springframework.stereotype.Service;

/**
 * Project: fw_common <br/>
 * File: MemberService.java <br/>
 * Class: com.yxt.user.service.MemberService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年1月7日 下午5:32:17 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class MemberService extends EntityService<Member> {

	@Override
	protected Class<Member> clzz() {
		return Member.class;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param username
	 * @return
	 */
	public Member fetchByName(String username) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("loginName", username);
		return fetchByCol(map);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param username
	 * @return
	 */
	public Member fetchByMobile(String mobile) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mobile", mobile);
		return fetchByCol(map);
	}

	public int update1(Map<String, Object> map) {
		StringBuilder sb = new StringBuilder("update ");
		sb.append(sql.getNc().getTableName(clzz()));
		sb.append(" set ");
		for (String key : map.keySet()) {
			if ("id".equals(key) || "sid".equals(key))
				continue;
			sb.append(key + "=#" + key + "#,");
		}
		sb.deleteCharAt(sb.lastIndexOf(",")).append(" where id = #id#");
		return sql.executeUpdate(sb.toString(), map);
	}

	public List<Member> findByenroll(int matchid) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("matchId", matchid);
		List<Member> enlist = sql.select("game_enroll.findgame_enrollList", Member.class, map);
		return enlist;
	}

	public Member findByMatchid(int matchid) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("matchId", matchid);
		Member member = sql.selectSingle("member.SearchClub", map, Member.class);
		return member;
	}

	public int updatePasswordByUserCode(Map<String, Object> map) {
		int i = sql.update("member.updateUserPassword", map);
		return i;
	}

}
