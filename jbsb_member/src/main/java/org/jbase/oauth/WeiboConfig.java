package org.jbase.oauth;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class WeiboConfig {
	private WeiboConfig() {
	}

	private static Properties props = new Properties();

	static {
		try {
			props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("oauth_weibo.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getValue(String key) {
		String property = props.getProperty(key);
		if (property != null)
			property = property.trim();
		else
			return "";
		return property;
	}

	public static void updateProperties(String key, String value) {
		props.setProperty(key, value);
	}
}
