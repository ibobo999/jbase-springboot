/*    */ package com.qq.connect.javabeans.weibo;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Music
/*    */   implements Serializable
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/* 13 */   private String author = "";
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getAuthor()
/*    */   {
/* 20 */     return this.author;
/*    */   }
/*    */   
/*    */   public String toString()
/*    */   {
/* 25 */     return "Music{author='" + this.author + '\'' + ", url='" + this.url + '\'' + ", title='" + this.title + '\'' + '}';
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getUrl()
/*    */   {
/* 37 */     return this.url;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 45 */   public String getTitle() { return this.title; }
/*    */   
/* 47 */   private String url = "";
/* 48 */   private String title = "";
/*    */   
/*    */   public Music(String author, String url, String title)
/*    */   {
/* 52 */     this.author = author;
/* 53 */     this.url = url;
/* 54 */     this.title = title;
/*    */   }
/*    */ }


/* Location:              /Users/popo/works/projects/javaee/20151010/qqdemo/WebContent/WEB-INF/lib/Sdk4J.jar!/com/qq/connect/javabeans/weibo/Music.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */