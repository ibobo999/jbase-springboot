/*    */ package com.qq.connect.api;
/*    */ import java.util.regex.Matcher;
/*    */ import java.util.regex.Pattern;

import org.jbase.oauth.QqConfig;

/*    */ 
/*    */ import com.qq.connect.QQConnect;
/*    */ import com.qq.connect.QQConnectException;
/*    */ import com.qq.connect.utils.http.PostParameter;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class OpenID
/*    */   extends QQConnect
/*    */ {
/*    */   private static final long serialVersionUID = 6913005509508673584L;
/*    */   
/*    */   public OpenID(String token)
/*    */   {
/* 26 */     this.client.setToken(token);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   private String getUserOpenID(String accessToken)
/*    */     throws QQConnectException
/*    */   {
/* 43 */     String openid = "";
/* 44 */     String jsonp = this.client.get(QqConfig.getValue("getOpenIDURL"), new PostParameter[] { new PostParameter("access_token", accessToken) }).asString();
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 49 */     Matcher m = Pattern.compile("\"openid\"\\s*:\\s*\"(\\w+)\"").matcher(jsonp);
/*    */     
/* 51 */     if (m.find()) {
/* 52 */       openid = m.group(1);
/*    */     } else {
/* 54 */       throw new QQConnectException("server error!");
/*    */     }
/* 56 */     return openid;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getUserOpenID()
/*    */     throws QQConnectException
/*    */   {
/* 70 */     String accessToken = this.client.getToken();
/* 71 */     return getUserOpenID(accessToken);
/*    */   }
/*    */ }


/* Location:              /Users/popo/works/projects/javaee/20151010/qqdemo/WebContent/WEB-INF/lib/Sdk4J.jar!/com/qq/connect/api/OpenID.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */