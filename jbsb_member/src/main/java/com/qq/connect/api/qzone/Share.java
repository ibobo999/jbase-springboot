/*     */ package com.qq.connect.api.qzone;
/*     */ import java.util.ArrayList;

import org.jbase.oauth.QqConfig;

/*     */ 
/*     */ import com.qq.connect.QQConnect;
/*     */ import com.qq.connect.QQConnectException;
/*     */ import com.qq.connect.javabeans.GeneralResultBean;
/*     */ import com.qq.connect.utils.http.PostParameter;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Share
/*     */   extends QQConnect
/*     */ {
/*     */   private static final long serialVersionUID = -3088533004308446275L;
/*     */   
/*     */   public Share(String token, String openID)
/*     */   {
/*  31 */     super(token, openID);
/*     */   }
/*     */   
/*     */ 
/*     */   private GeneralResultBean addShare(PostParameter[] parameters)
/*     */     throws QQConnectException
/*     */   {
/*  38 */     return new GeneralResultBean(this.client.post(QqConfig.getValue("addShareURL"), parameters).asJSONObject());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public GeneralResultBean addShare(String title, String url, String site, String fromUrl, String... parameters)
/*     */     throws QQConnectException
/*     */   {
/*  77 */     ArrayList<PostParameter> postParameterArray = new ArrayList();
/*     */     
/*  79 */     postParameterArray.add(new PostParameter("title", title));
/*  80 */     postParameterArray.add(new PostParameter("url", url));
/*  81 */     postParameterArray.add(new PostParameter("site", site));
/*  82 */     postParameterArray.add(new PostParameter("fromurl", fromUrl));
/*     */     
/*     */ 
/*     */ 
/*  86 */     for (String parameter : parameters) {
/*  87 */       if (parameter.indexOf("comment") == 0) {
/*  88 */         postParameterArray.add(new PostParameter("comment", parameter.substring(8)));
/*     */       }
/*  90 */       else if (parameter.indexOf("summary") == 0) {
/*  91 */         postParameterArray.add(new PostParameter("summary", parameter.substring(8)));
/*     */       }
/*  93 */       else if (parameter.indexOf("images") == 0) {
/*  94 */         postParameterArray.add(new PostParameter("images", parameter.substring(7)));
/*     */       }
/*  96 */       else if (parameter.indexOf("type") == 0) {
/*  97 */         postParameterArray.add(new PostParameter("type", parameter.substring(5)));
/*     */       }
/*  99 */       else if (parameter.indexOf("playurl") == 0) {
/* 100 */         postParameterArray.add(new PostParameter("playurl", parameter.substring(8)));
/*     */       }
/* 102 */       else if (parameter.indexOf("nswb") == 0) {
/* 103 */         postParameterArray.add(new PostParameter("nswb", parameter.substring(5)));
/*     */       }
/*     */       else {
/* 106 */         throw new QQConnectException("you pass one illegal parameter");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 111 */     postParameterArray.add(new PostParameter("format", "json"));
/* 112 */     postParameterArray.add(new PostParameter("access_token", this.client.getToken()));
/* 113 */     postParameterArray.add(new PostParameter("oauth_consumer_key", QqConfig.getValue("app_ID")));
/*     */     
/* 115 */     postParameterArray.add(new PostParameter("openid", this.client.getOpenID()));
/*     */     
/*     */ 
/* 118 */     return addShare((PostParameter[])postParameterArray.toArray(new PostParameter[1]));
/*     */   }
/*     */ }


/* Location:              /Users/popo/works/projects/javaee/20151010/qqdemo/WebContent/WEB-INF/lib/Sdk4J.jar!/com/qq/connect/api/qzone/Share.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */