/*    */ package com.qq.connect.api.qzone;
/*    */ import java.util.ArrayList;

import org.jbase.oauth.QqConfig;

/*    */ 
/*    */ import com.qq.connect.QQConnect;
/*    */ import com.qq.connect.QQConnectException;
/*    */ import com.qq.connect.javabeans.GeneralResultBean;
/*    */ import com.qq.connect.utils.http.PostParameter;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Blog
/*    */   extends QQConnect
/*    */ {
/*    */   private static final long serialVersionUID = -6962921164439096289L;
/*    */   
/*    */   public Blog(String token, String openID)
/*    */   {
/* 29 */     super(token, openID);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   private GeneralResultBean addBlog(PostParameter[] parameters)
/*    */     throws QQConnectException
/*    */   {
/* 45 */     return new GeneralResultBean(this.client.post(QqConfig.getValue("addBlogURL"), parameters).asJSONObject());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public GeneralResultBean addBlog(String title, String content)
/*    */     throws QQConnectException
/*    */   {
/* 71 */     ArrayList<PostParameter> postParameterArray = new ArrayList();
/*    */     
/*    */ 
/*    */ 
/* 75 */     postParameterArray.add(new PostParameter("title", title));
/* 76 */     postParameterArray.add(new PostParameter("content", content));
/* 77 */     postParameterArray.add(new PostParameter("format", "json"));
/* 78 */     postParameterArray.add(new PostParameter("access_token", this.client.getToken()));
/* 79 */     postParameterArray.add(new PostParameter("oauth_consumer_key", QqConfig.getValue("app_ID")));
/* 80 */     postParameterArray.add(new PostParameter("openid", this.client.getOpenID()));
/*    */     
/*    */ 
/* 83 */     return addBlog((PostParameter[])postParameterArray.toArray(new PostParameter[1]));
/*    */   }
/*    */ }


/* Location:              /Users/popo/works/projects/javaee/20151010/qqdemo/WebContent/WEB-INF/lib/Sdk4J.jar!/com/qq/connect/api/qzone/Blog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */