/*    */ package com.qq.connect.api.qzone;
import org.jbase.oauth.QqConfig;

/*    */ 
/*    */ import com.qq.connect.QQConnect;
/*    */ import com.qq.connect.QQConnectException;
/*    */ import com.qq.connect.javabeans.qzone.PageFansBean;
/*    */ import com.qq.connect.utils.http.PostParameter;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class PageFans
/*    */   extends QQConnect
/*    */ {
/*    */   private static final long serialVersionUID = -3088533004308446275L;
/*    */   
/*    */   public PageFans(String token, String openID)
/*    */   {
/* 26 */     super(token, openID);
/*    */   }
/*    */   
/*    */   private PageFansBean checkPageFans(PostParameter[] parameters) throws QQConnectException {
/* 30 */     return new PageFansBean(this.client.get(QqConfig.getValue("checkPageFansURL"), parameters).asJSONObject());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public PageFansBean checkPageFans(String pageID)
/*    */     throws QQConnectException
/*    */   {
/* 50 */     return checkPageFans(new PostParameter[] { new PostParameter("page_id", pageID), new PostParameter("format", "json"), new PostParameter("access_token", this.client.getToken()), new PostParameter("oauth_consumer_key", QqConfig.getValue("app_ID")), new PostParameter("openid", this.client.getOpenID()) });
/*    */   }
/*    */ }


/* Location:              /Users/popo/works/projects/javaee/20151010/qqdemo/WebContent/WEB-INF/lib/Sdk4J.jar!/com/qq/connect/api/qzone/PageFans.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */