/*    */ package com.qq.connect.api.weibo;
import org.jbase.oauth.QqConfig;

/*    */ 
/*    */ import com.qq.connect.QQConnect;
/*    */ import com.qq.connect.QQConnectException;
/*    */ import com.qq.connect.javabeans.weibo.UserInfoBean;
/*    */ import com.qq.connect.utils.http.PostParameter;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class OtherUserInfo
/*    */   extends QQConnect
/*    */ {
/*    */   private static final long serialVersionUID = -6124397423510235640L;
/*    */   
/*    */   public OtherUserInfo(String token, String openID)
/*    */   {
/* 28 */     super(token, openID);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   private UserInfoBean getUserInfo(String openid, String parameter, int flag)
/*    */     throws QQConnectException
/*    */   {
/* 42 */     PostParameter[] parameters = null;
/* 43 */     if (flag == 1) {
/* 44 */       parameters = new PostParameter[] { new PostParameter("openid", openid), new PostParameter("name", parameter), new PostParameter("oauth_consumer_key", QqConfig.getValue("app_ID")), new PostParameter("access_token", this.client.getToken()), new PostParameter("format", "json") };
/*    */ 
/*    */     }
/*    */     else
/*    */     {
/*    */ 
/* 50 */       parameters = new PostParameter[] { new PostParameter("openid", openid), new PostParameter("fopenid", parameter), new PostParameter("oauth_consumer_key", QqConfig.getValue("app_ID")), new PostParameter("access_token", this.client.getToken()), new PostParameter("format", "json") };
/*    */     }
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 56 */     return new UserInfoBean(this.client.get(QqConfig.getValue("getWeiboOtherUserInfoURL"), parameters).asJSONObject());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public UserInfoBean getUserInfoByName(String name)
/*    */     throws QQConnectException
/*    */   {
/* 73 */     return getUserInfo(this.client.getOpenID(), name, 1);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public UserInfoBean getUserInfoByOpenID(String fopenid)
/*    */     throws QQConnectException
/*    */   {
/* 86 */     return getUserInfo(this.client.getOpenID(), fopenid, 2);
/*    */   }
/*    */ }


/* Location:              /Users/popo/works/projects/javaee/20151010/qqdemo/WebContent/WEB-INF/lib/Sdk4J.jar!/com/qq/connect/api/weibo/OtherUserInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */