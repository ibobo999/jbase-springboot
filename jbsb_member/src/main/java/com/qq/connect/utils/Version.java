/*    */ package com.qq.connect.utils;

import org.jbase.oauth.QqConfig;

/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Version
/*    */ {
/* 10 */   private static String VERSION = "2.0.0.0";
/*    */   private static final String TITLE = "qq_connect_sdk";
/*    */   
/*    */   static {
/*    */     try {
/* 15 */       VERSION = QqConfig.getValue("version");
/*    */     } catch (Exception e) {
/* 17 */       VERSION = "2.0.0.0";
/*    */     }
/* 19 */     if (VERSION.equals("")) {
/* 20 */       VERSION = "2.0.0.0";
/*    */     }
/*    */   }
/*    */   
/*    */   public static String getVersion() {
/* 25 */     return VERSION;
/*    */   }
/*    */   
/*    */   public String toString() {
/* 29 */     return "qq_connect_sdk " + VERSION;
/*    */   }
/*    */ }


/* Location:              /Users/popo/works/projects/javaee/20151010/qqdemo/WebContent/WEB-INF/lib/Sdk4J.jar!/com/qq/connect/utils/Version.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */