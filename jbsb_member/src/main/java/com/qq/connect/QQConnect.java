package com.qq.connect;

import java.io.Serializable;

import com.qq.connect.utils.http.HttpClient;

public class QQConnect implements Serializable {
	private static final long serialVersionUID = 2403532632395197292L;
	protected HttpClient client = new HttpClient();

	protected QQConnect() {
	}

	private String token;
	private String openID;

	protected QQConnect(String token, String openID) {
		this.token = token;
		this.openID = openID;
		this.client.setToken(token);
		this.client.setOpenID(openID);
	}

	protected void setToken(String token) {
		this.token = token;
		this.client.setToken(token);
	}

	protected void setOpenID(String openID) {
		this.openID = openID;
		this.client.setOpenID(openID);
	}

	public String getToken() {
		return this.token;
	}

	public String getOpenId() {
		return this.openID;
	}
}
