package org.jbase.bbs.entity;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

@Table(name = "bbs_topic")
public class Topic extends BaseEntity {

	private static final long serialVersionUID = -148861696638816094L;

	private int userId;
	private int moduleId;
	private int postCount;
	private int replyCount;
	private int pv = 0;
	private String content;
	private int emotion;
	private int isNice = 0;
	private int isUp = 0;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the moduleId
	 */
	public int getModuleId() {
		return moduleId;
	}

	/**
	 * @param moduleId
	 *            the moduleId to set
	 */
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @return the postCount
	 */
	public int getPostCount() {
		return postCount;
	}

	/**
	 * @param postCount
	 *            the postCount to set
	 */
	public void setPostCount(int postCount) {
		this.postCount = postCount;
	}

	/**
	 * @return the replyCount
	 */
	public int getReplyCount() {
		return replyCount;
	}

	/**
	 * @param replyCount
	 *            the replyCount to set
	 */
	public void setReplyCount(int replyCount) {
		this.replyCount = replyCount;
	}

	/**
	 * @return the pv
	 */
	public int getPv() {
		return pv;
	}

	/**
	 * @param pv
	 *            the pv to set
	 */
	public void setPv(int pv) {
		this.pv = pv;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the emotion
	 */
	public int getEmotion() {
		return emotion;
	}

	/**
	 * @param emotion
	 *            the emotion to set
	 */
	public void setEmotion(int emotion) {
		this.emotion = emotion;
	}

	/**
	 * @return the isNice
	 */
	public int getIsNice() {
		return isNice;
	}

	/**
	 * @param isNice
	 *            the isNice to set
	 */
	public void setIsNice(int isNice) {
		this.isNice = isNice;
	}

	/**
	 * @return the isUp
	 */
	public int getIsUp() {
		return isUp;
	}

	/**
	 * @param isUp
	 *            the isUp to set
	 */
	public void setIsUp(int isUp) {
		this.isUp = isUp;
	}

}