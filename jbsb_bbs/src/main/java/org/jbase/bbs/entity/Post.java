package org.jbase.bbs.entity;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.domain.BaseEntity;

/**
 * Created with IntelliJ IDEA. Author: iver Date: 13-3-26
 */
@Table(name = "bbs_post")
public class Post extends BaseEntity {

	private static final long serialVersionUID = -1943054632691110158L;

	private int topicId;
	private int userId;
	private String content;
	private int hasReply;

	/**
	 * @return the topicId
	 */
	public int getTopicId() {
		return topicId;
	}

	/**
	 * @param topicId
	 *            the topicId to set
	 */
	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the hasReply
	 */
	public int getHasReply() {
		return hasReply;
	}

	/**
	 * @param hasReply
	 *            the hasReply to set
	 */
	public void setHasReply(int hasReply) {
		this.hasReply = hasReply;
	}

}
