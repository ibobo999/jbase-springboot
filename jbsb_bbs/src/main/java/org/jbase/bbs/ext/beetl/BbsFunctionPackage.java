/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.bbs.ext.beetl;

import javax.servlet.http.HttpServletRequest;

import org.jbase.ext.beetl.JbaseFunctionPackage;
import org.jbase.member.entity.Member;
import org.springframework.stereotype.Service;

/**
 * 
 * Project: yxt_bbs <br/>
 * File: BbsFunctionPackage.java <br/>
 * Class: org.jbase.bbs.ext.beetl.BbsFunctionPackage <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年4月20日 下午4:01:05 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service("bbsfn")
public class BbsFunctionPackage implements JbaseFunctionPackage {

	public Member currentUser(HttpServletRequest request) {
		Member member = new Member(); //(Member) request.getSession().getAttribute("login_member");
		member.setLoginName("liu");
		return member;
	}
}
