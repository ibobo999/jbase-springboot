package org.jbase.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.common.domain.Area;
import org.springframework.stereotype.Service;

@Service
public class AreaService extends BaseService {
	public List<Area> AllArea(Map<String, Object> map) {
		List<Area> area = null;

		area = sql.select("Area.AllArea", Area.class, map);

		return area;
	}

	public List<Area> AllArea1(Map<String, Object> map) {
		List<Area> area = null;

		area = sql.select("Area.AllArea1", Area.class, null);

		return area;
	}

	public Area OneArea(String areaID) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("areaID", areaID);
		Area area = sql.selectSingle("Area.OneArea", map, Area.class);
		return area;

	}

}
