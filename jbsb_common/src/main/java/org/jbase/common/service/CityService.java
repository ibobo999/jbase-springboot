package org.jbase.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.common.domain.City;
import org.springframework.stereotype.Service;

@Service
public class CityService extends EntityService<City> {
	@Override
	protected Class<City> clzz() {
		// TODO Auto-generated method stub
		return City.class;
	}

	public List<City> AllCity(Map<String, Object> map) {
		List<City> city = sql.select("City.AllCity", City.class, map);

		return city;
	}

	public List<City> AllCity1() {
		List<City> city = sql.select("City.AllCity1", City.class, null);

		return city;
	}

	public City OneCity(String cityID) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cityID", cityID);
		City city = sql.selectSingle("City.OneCity", map, City.class);
		return city;
	}

}
