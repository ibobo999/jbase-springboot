/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.common.domain.Areas;
import org.springframework.stereotype.Service;

/**
 * Project: fw_common <br/>
 * File: AreasService.java <br/>
 * Class: com.yxt.common.service.AreasService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年2月17日 下午3:17:35 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class AreasService extends BaseService {
	public List<Areas> listProvince() {
		String sqlTmp = " select * from dict_areas where parentId=0 ";
		return sql.execute(sqlTmp, Areas.class, null);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param provinceId
	 * @return
	 */
	public Areas fetchByAreaId(Integer areaId) {
		Map<String, Object> paras = new HashMap<String, Object>();
		paras.put("areaId", areaId);
		return sql.selectSingle("areas.fetchByAreaId", paras, Areas.class);
	}
	public List<Areas> fetchByParentId(Integer parentId) {
		Map<String, Object> paras = new HashMap<String, Object>();
		paras.put("parentId", parentId);
		return sql.select("areas.fetchByParentId",  Areas.class, paras);
	}
}
