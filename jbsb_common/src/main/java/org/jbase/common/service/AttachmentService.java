/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.service;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.db.KeyHolder;
import org.jbase.GlobalConstants;
import org.jbase.common.domain.Attachment;
import org.jbase.common.utils.Pager;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Project: fw_common <br/>
 * File: AttachmentService.java <br/>
 * Class: com.yxt.common.service.AttachmentService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 上午12:26:06 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class AttachmentService extends BaseService {

	private static final DateFormat df = new SimpleDateFormat("yyyyMMdd");

	public Attachment createFile(MultipartFile file) {
		Attachment attach = new Attachment();

		String dt = df.format(new Date());
		String fileName = file.getOriginalFilename();
		String suffix = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
		String sname = System.currentTimeMillis() + (Math.random() + "").replace(".", "") + suffix;
		String lpath = GlobalConstants.image_upload_path + "/" + dt;
		File pfile = new File(lpath);
		if (!pfile.exists())
			pfile.mkdirs();
		attach.setMimeType(file.getContentType());
		attach.setSize(file.getSize());
		attach.setUrl(dt + "/" + sname);
		attach.setPath(lpath + "/" + sname);
		attach.setAftName(sname);
		attach.setPreName(fileName);
		attach.setSuffix(suffix);
		// sql.insert(attach);
		KeyHolder holder = new KeyHolder();
		sql.insert(Attachment.class, attach, holder);
		attach.setId(holder.getInt());
		return attach;
	}

	public Attachment createFile(MultipartFile file, String id) {
		Attachment attach = new Attachment();

		String dt = df.format(new Date());
		String fileName = file.getOriginalFilename();
		String suffix = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
		String sname = System.currentTimeMillis() + (Math.random() + "").replace(".", "") + suffix;
		String lpath = GlobalConstants.image_upload_path + "/" + dt;
		File pfile = new File(lpath);
		if (!pfile.exists())
			pfile.mkdirs();
		attach.setMimeType(file.getContentType());
		attach.setSize(file.getSize());
		attach.setUrl(dt + "/" + sname);
		attach.setPath(lpath + "/" + sname);
		attach.setAftName(sname);
		attach.setPreName(fileName);
		attach.setLinkSid(id);
		attach.setSuffix(suffix);
		sql.insert(attach);
		return attach;
	}

	public Pager queryPhotos(String sid) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sid", sid);
		List<Attachment> list = sql.select("attach.fetchBySId", Attachment.class, map);
		Pager pager = new Pager();
		pager.setList(list);
		return pager;
	}

	/**
	 * 根据id删除图片
	 * 
	 * @param id
	 * @return
	 */
	public int deleteFile(String id) {
		return sql.deleteById(Attachment.class, id);
	}

	public String queryPhotosForGyLogo(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sid", id);
		List<Attachment> list = sql.select("attach.fetchBySId", Attachment.class, map, 0, 1);
		if (list.size() > 0) {
			return list.get(0).getUrl();
		}
		return "";
	}

	public List<Attachment> listAttachs(String ids) {
		String sqlTemplate = "select * from sys_attachment where id in (" + ids + ")";
		List<Attachment> list = sql.execute(sqlTemplate, Attachment.class, null);
		return list;
	}

}
