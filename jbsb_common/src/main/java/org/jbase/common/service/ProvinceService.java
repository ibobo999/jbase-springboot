package org.jbase.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.common.domain.Province;
import org.springframework.stereotype.Service;

@Service
public class ProvinceService extends BaseService {
	public List<Province> AllProvince() {
		List<Province> province = sql.select("Province.AllProvince", Province.class, null);
		return province;
	}

	public List<Province> SearchProvince(Map<String, Object> map) {
		List<Province> province = sql.select("Province.SearchProvince", Province.class, map);
		return province;
	}

	public Province OneProvince(String provinceID) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("provinceID", provinceID);
		Province province = sql.selectSingle("Province.OneProvince", map, Province.class);
		return province;

	}
}
