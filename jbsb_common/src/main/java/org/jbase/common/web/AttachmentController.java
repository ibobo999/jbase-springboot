/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.web;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jbase.common.domain.Attachment;
import org.jbase.common.service.AttachmentService;
import org.jbase.common.utils.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.alibaba.fastjson.JSON;

/**
 * Project: fw_common <br/>
 * File: AttachmentController.java <br/>
 * Class: com.yxt.common.controller.AttachmentController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月25日 上午11:46:45 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller
@RequestMapping("/attachment")
public class AttachmentController {

	@Autowired
	private AttachmentService attachmentService;

	@ResponseBody
	@RequestMapping("/uploadImage")
	public String uploadImage(HttpServletRequest request) {
		MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multiRequest.getFile("file");
		Map<String, String> map = new HashMap<String, String>();
		map.put("state", "SUCCESS");
		if (file != null) {
			Attachment att = attachmentService.createFile(file);
			try {
				file.transferTo(new File(att.getPath()));
				map.put("url", att.getUrl());
				map.put("attachId", att.getId() + "");
				map.put("attachSid", att.getSid());
			} catch (IllegalStateException e) {
				e.printStackTrace();
				map.put("state", "fail");
				map.put("error_info", e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				map.put("state", "fail");
				map.put("error_info", e.getMessage());
			}
		}

		return JSON.toJSONString(map);
	}

	@RequestMapping("/photos")
	public String photos(HttpServletRequest request) {
		String id = request.getParameter("id");
		String sid = request.getParameter("sid");
		/* Pager pager=attachmentService.queryPhotos(id); */
		Pager pager = attachmentService.queryPhotos(sid);
		request.setAttribute("pager", pager);
		return "/gymnasium/gymnasium_photos_page";
	}

	@RequestMapping("/summary_photos")
	public String summary_photos(HttpServletRequest request) {
		String id = request.getParameter("id");
		String sid = request.getParameter("sid");
		/* Pager pager=attachmentService.queryPhotos(id); */
		Pager pager = attachmentService.queryPhotos(sid);
		request.setAttribute("pager", pager);
		return "/userFeedBack/userFeedBack_photos_page";
	}

	@ResponseBody
	@RequestMapping("/deletePhto")
	public String deletePhto(HttpServletRequest request) {
		String id = request.getParameter("idvalue");
		int att = attachmentService.deleteFile(id);
		Map<String, String> map = new HashMap<String, String>();
		map.put("state", "400");
		if (att > 0) {
			map.put("state", "200");
		}
		return JSON.toJSONString(map);
	}

	@ResponseBody
	@RequestMapping("/uploadImages")
	public String uploadImages(HttpServletRequest request) {
		String id = request.getParameter("idvalue");
		// String id1=request.getAttribute("id");
		// 解析器解析request的上下文
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
				request.getSession().getServletContext());
		// 先判断request中是否包涵multipart类型的数据，
		if (multipartResolver.isMultipart(request)) {
			// 再将request中的数据转化成multipart类型的数据
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			Iterator<?> iter = multiRequest.getFileNames();
			while (iter.hasNext()) {
				MultipartFile file = multiRequest.getFile((String) iter.next());
				if (file != null) {
					Attachment att = attachmentService.createFile(file, id);
					// 写文件到本地
					try {
						file.transferTo(new File(att.getPath()));
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("state", "SUCCESS");
		return JSON.toJSONString(map);
	}

	/**
	 * 球馆的馆主详细页面的图片查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/photosForGl")
	public String photosForGl(HttpServletRequest request) {
		String id = request.getParameter("id");
		Pager pager = attachmentService.queryPhotos(id);
		request.setAttribute("pager", pager);
		return "/gymnasium/gymnasium_photos_page_forGl";
	}

	/**
	 * 球馆游客的详细页面的图片查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("photosForDetail")
	public String photosForDetail(HttpServletRequest request) {
		String sid = request.getParameter("sid");
		Pager pager = attachmentService.queryPhotos(sid);
		request.setAttribute("pager", pager);
		return "/gymnasium/gymnasium_photos_page_ykdetail";
	}
}
