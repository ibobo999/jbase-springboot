/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.web;

import javax.servlet.http.HttpServletRequest;

import org.jbase.common.domain.BaseEntity;
import org.jbase.common.service.EntityMapperService;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Project: fw_admin <br/>
 * File: EntityController.java <br/>
 * Class: com.yxt.admin.controller.EntityController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月19日 上午4:04:22 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public abstract class EntityMapperController<T extends BaseEntity> {

	abstract protected EntityMapperService<T> service();

	// private static final Logger L = LoggerFactory.getLogger(EntityController.class);

	@RequestMapping("/index")
	public void index(HttpServletRequest request) {
	}

	@RequestMapping("/add")
	public void add(HttpServletRequest request) {
	}

	@RequestMapping("/edit")
	public void edit(HttpServletRequest request) {
	}

	@RequestMapping("/show")
	public void show(HttpServletRequest request) {
	}
//
//	@RequestMapping("/list")
//	public void list(HttpServletRequest request) {
//		Requester warp = Requester.warp(request);
//		Pager pager = warp.getPager();
//		List<T> all = service().all(pager.getOffset(), pager.getPageSize());
//		pager.setList(all);
//		request.setAttribute("pager", pager);
//	}
//
//	@ResponseBody
//	@RequestMapping(value = "/listData", produces = "text/plain;charset=UTF-8")
//	public String listData(HttpServletRequest request) {
//		Requester warp = Requester.warp(request);
//		Pager pager = warp.getPager();
//		pager.setList(service().all(pager.getOffset(), pager.getPageSize()));
//		return JSON.toJSONString(pager);
//	}
//
//	@ResponseBody
//	@RequestMapping(value = "/addSave", produces = "text/plain;charset=UTF-8")
//	public String addSave(HttpServletRequest request, @RequestParam T entity) {
//		service().insert(entity);
//		return ok();
//	}
//
//	@RequestMapping("/del")
//	public String del(@RequestParam String ids) {
//		service().deleteById(ids);
//		return "redirect:list";
//	}
//
//	@RequestMapping("/edit")
//	public void edit(@RequestParam int id, HttpServletRequest request) {
//		T t = service().unique(id);
//		request.setAttribute("obj", t);
//	}
//
//	@ResponseBody
//	@RequestMapping(value = "/editSave", produces = "text/plain;charset=UTF-8")
//	public String editSave(HttpServletRequest request, @RequestParam T entity) {
//		service().updateById(entity);
//		return ok();
//	}
//
//	protected String ok() {
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("retCode", "200");
//		map.put("retMsg", "成功");
//		map.put("retObj", "object");
//		return JSON.toJSONString(map);
//	}
//
//	protected String fail(String mesg) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("retCode", "500");
//		map.put("retMsg", mesg);
//		return JSON.toJSONString(map);
//	}

}
