/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jbase.common.domain.BaseEntity;
import org.jbase.common.service.EntityService;
import org.jbase.common.utils.Pager;
import org.jbase.common.utils.Requester;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

/**
 * Project: fw_admin <br/>
 * File: EntityController.java <br/>
 * Class: com.yxt.admin.controller.EntityController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月19日 上午4:04:22 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public abstract class EntityController<T extends BaseEntity> {

	abstract protected EntityService<T> service();

	// private static final Logger L = LoggerFactory.getLogger(EntityController.class);

	@RequestMapping("/list")
	public void list(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = service().listWithPage(warp.getSearchKeys(), warp.getPager());
		request.setAttribute("pager", pager);
	}

	@ResponseBody
	@RequestMapping(value = "/listData", produces = "text/plain;charset=UTF-8")
	public String listData(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = service().listWithPage(warp.getSearchKeys(), warp.getPager());
		return JSON.toJSONString(pager);
	}

	@RequestMapping("/add")
	public void add(HttpServletRequest request) {
	}

	@ResponseBody
	@RequestMapping(value = "/addSave", produces = "text/plain;charset=UTF-8")
	public String addSave(HttpServletRequest request) {
		service().insertHolderId(Requester.warp(request).getParams());
		return ok();
	}

	@RequestMapping("/del")
	public String del(@RequestParam String ids) {
		service().deleteByIds(ids);
		return "redirect:list";
	}

	@RequestMapping("/edit")
	public void edit(@RequestParam int id, HttpServletRequest request) {
		T t = service().fetchById(id);
		request.setAttribute("obj", t);
	}

	@ResponseBody
	@RequestMapping(value = "/editSave", produces = "text/plain;charset=UTF-8")
	public String editSave(HttpServletRequest request) {
		service().update(Requester.warp(request).getParams());
		return ok();
	}

	protected String ok() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "200");
		map.put("retMsg", "成功");
		map.put("retObj", "object");
		return JSON.toJSONString(map);
	}
	protected String fail(String mesg) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "500");
		map.put("retMsg", mesg);
		return JSON.toJSONString(map);
	}

}
