package org.jbase.common.domain;

import java.io.Serializable;

import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.Table;

@Table(name = "dict_province")
public class Province extends TailBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 538300720560602050L;
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProvinceID() {
		return provinceID;
	}

	public void setProvinceID(String provinceID) {
		this.provinceID = provinceID;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	private String provinceID;
	private String province;

}
