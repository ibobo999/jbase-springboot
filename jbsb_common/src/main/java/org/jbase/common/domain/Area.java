package org.jbase.common.domain;

import java.io.Serializable;

import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.Table;

@Table(name = "dict_area")
public class Area extends TailBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2143058592882020363L;
	/**
	 * 
	 */
	private int id;
	private String areaID;
	private String area;
	private String cityID;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAreaID() {
		return areaID;
	}
	public void setAreaID(String areaID) {
		this.areaID = areaID;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getCityID() {
		return cityID;
	}
	public void setCityID(String cityID) {
		this.cityID = cityID;
	}
}
