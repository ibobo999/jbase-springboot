package org.jbase.common.domain;

import org.beetl.sql.core.annotatoin.Table;

@Table(name = "dict_city")
public class City extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -819121362840538220L;
	private int id;
	private String cityID;
	private String city;
	private String provinceID;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCityID() {
		return cityID;
	}

	public void setCityID(String cityID) {
		this.cityID = cityID;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvinceID() {
		return provinceID;
	}

	public void setProvinceID(String provinceID) {
		this.provinceID = provinceID;
	}

}
