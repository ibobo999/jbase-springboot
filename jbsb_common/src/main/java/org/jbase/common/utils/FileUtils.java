/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

/**
 * Project: jbsb_common <br/>
 * File: FileUtils.java <br/>
 * Class: org.jbase.common.utils.FileUtils <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年9月29日 上午11:48:33 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public abstract class FileUtils {

	private static ResourceLoader resourceLoader = new DefaultResourceLoader();
	private static final Logger L = LoggerFactory.getLogger(FileUtils.class);

	public static String read(String path, String filename, String encode) {
		File file = new File(path, filename);
		return read(file, encode);
	}

	public static String read(File file, String encode) {
		String str = "";
		try {
			FileInputStream in2 = new FileInputStream(file);
			str = read(in2, encode);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return str;
	}

	public static String read(InputStream inputStream, String encode) {
		StringBuffer sb = new StringBuffer();
		try {
			InputStreamReader in = new InputStreamReader(inputStream, encode);
			BufferedReader br = new BufferedReader(in);
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				line = br.readLine();
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static String read(InputStream inputStream) {
		String read = read(inputStream, "UTF8");
		L.debug("读取资源结束：{}", read);
		return read;
	}

	public static String readByClassLoaderResourceStream(String resurce) {

		Resource resource = resourceLoader.getResource(resurce);
		InputStream sras = null;
		try {
			sras = resource.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		L.debug("读取资源：{}", resurce);
		return read(sras);
	}
}
