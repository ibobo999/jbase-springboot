package org.jbase.common.utils.message;

/**
 * 封装消费者调用接口返回信息状态码<br/>
 * 不够用请自己添加，注意命名规范
 * Created by ADon on 2016/7/22.
 */
public enum MessageCode {
    //成功状态码
    SUCCESS("001", "成功"),
    FAIL("002", "失败"),
    ENTITY_ID_IS_EMPTY("002001", "传入的ID为空"),
    UserLoginInfo_VeCode_NOT_EXIST("010","用户不存在"),
    UserLoginInfo_Password_NOT_EQUAL("011","密码错误"),
    UserLoginInfo_VeCode_IS_Login("012","登录成功"),
    UserLoginInfo_VeCode_NOT_Login("013","登录失败"),
    UserData_VeCode_NOT_EXIST("010","用户数据不存在"),
    UserData_VeCode_NOT_ERROR("011","发生错误"),
    UserData_Level_NOT_AUTH("010","尚未进行实名认证"),
    UserData_Pwd_NOT_LEGAL("010","密码只能为6位数字"),
    UserAccount_VeCode_NOT_ERROR("010","发生错误"),
    Manager_Delete_Error1("020","对不起，剩余管理员不能少于1位"),
    Manager_Delete_Error2("021","对不起，不能删除自己"),
    Group_Delete_Error("021","对不起，不能删除自己的角色");
    /*
        命名规范说明：
        实体名_属性_是否_错误信息
        TASKINFO_ID_IS_EMPTY("010", "任务ID为空")
        TASKINFO_ID_NOT_EXIST("011", "任务ID不存在")
        TASKINFO_ID_IS_EXIST（"012", "任务ID已存在"）
    */

    private String code;
    private String msg;

    MessageCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

}
