
package org.jbase.common.utils;
import javax.xml.namespace.QName;

import org.apache.axis.client.Service;
import org.apache.axis.client.Call;
import org.apache.axis.encoding.XMLType;


/**
 * 发送短信调用接口
 * @author lyf
 *
 */
public class MsgWs {
	
	public static String SendWaitWorkMsg(String mobile,String pwd) {
	  try {

			  String urlname = "http://sdk1.mb345.com/ws/LinkWS.asmx" ;
//			  String urlname = "http://120.132.132.133/ws/LinkWS.asmx" ;
              String soapActionURI = "http://tempuri.org/BatchSend";


              Service s = new  Service();

              Call call = (Call) s.createCall();

              call.setTimeout(new Integer(5000));
              call.setUseSOAPAction(true);
              call.setSOAPActionURI(soapActionURI);

              call.setOperationName(new QName("http://tempuri.org/","BatchSend" ));//wsdl中接口名称

              call.setTargetEndpointAddress(urlname);
              call.addParameter(new QName("http://tempuri.org/","CorpID" ), XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
              call.addParameter(new QName("http://tempuri.org/","Pwd" ), XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN); 
              call.addParameter(new QName("http://tempuri.org/","Mobile" ), XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN); 
              call.addParameter(new QName("http://tempuri.org/","Content" ), XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN); 
              call.addParameter(new QName("http://tempuri.org/","Cell" ), XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN); 
              call.addParameter(new QName("http://tempuri.org/","SendTime" ), XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN); 
              /*QName qn = new QName("http://tempuri.org/","BatchSend");  
              call.setReturnType(qn);*/
              String[] fn01 = {"YXT004911","yyundong@yuxintong",mobile,pwd,"","" };

              String val = (String)call.invoke(fn01);

             // System.out .println( "getSecurityToken(correct):"  + val);
              return val;

    } catch (Exception e) {

          //java.io.InterruptedIOException: Read timed out

        //  System.out.println(e.getMessage());
          return e.getMessage();

    }
		/*URL url = null;
		String CorpID="YXT004911";//账户名
		String Pwd="yyundong@yuxintong";//密码
		url = new URL("http://sdk2.028lk.com:9880/sdk2/BatchSend2.aspx?CorpID="+CorpID+"&Pwd="+Pwd+"&Mobile="+"15097329653"+"&Content="+"[宇运动]"+"&Cell=&SendTime="+"");
		BufferedReader in = null;
		int inputLine = 0;
		try {
			System.out.println("开始发送短信手机号码为 ："+"15097329653");
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			inputLine = new Integer(in.readLine()).intValue();
		} catch (Exception e) {
			System.out.println("网络异常,发送短信失败！");
			inputLine=-2;
		}
		System.out.println("结束发送短信返回值：  "+inputLine);*/
//		return inputLine;
	}
/*public static void main(String[] args) {
	SendWaitWorkMsg("15097329653,18033761989","123456,为您的宇运动注册验证码，请您在5分钟内完成注册操作，如非本人操作，请忽略。");
	SendWaitWorkMsg("15097329653,18033761989","您在宇运动请求的验证码是 123456。");
}*/
}
