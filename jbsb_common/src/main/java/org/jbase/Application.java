/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Project: Jbase-Common <br/>
 * File: Application.java <br/>
 * Class: org.jbase.Application <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年8月4日 下午3:12:44 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@SpringBootApplication
@ServletComponentScan
public class Application {

	private static final Logger L = LoggerFactory.getLogger(Application.class);

	private static ApplicationContext applicationContext = null;

	public static void main(String[] args) {
		applicationContext = SpringApplication.run(Application.class, args);

		if (L.isDebugEnabled()) {
			L.debug("开始启动：Let's inspect the beans provided by Spring Boot:");
			String[] beanNames = applicationContext.getBeanDefinitionNames();
			Arrays.sort(beanNames);
			for (String beanName : beanNames) {
				L.debug(beanName);
			}
		}
	}

	public static Object getBean(String name) {
		return applicationContext.getBean(name);
	}

	public static ApplicationContext ctx() {
		return applicationContext;
	}

	@Controller
	class IndexController {
		@RequestMapping(value = { "/", "/index" })
		public String index() {
			return "index";
		}
	}
}
